### Architecture definitions & requirements

* Module has structure like
```
src
    course
        repository
        dao
            migrations
            entity
        service
        controller
        cli -> command line interface commands
        command -> CQRS (command & queries) here without sub-folding
        course.module.ts
```

* Use flat modules

Good
```
src
    auth
    courses
    consultations
    messages
```

Bad
```
src
    auth
        google
        common
    courses
                
```

* Definition modules for external APIs

naming rule: `${protocol}-${name}`

for example if we integrate with sendpulse via http and need module for it
name should be `http-sendpulse`

if we try to integrate with internal service `exchange` via grpc use `grpc-exchange`
