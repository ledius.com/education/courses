import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class Mails {
  public constructor(
    private readonly http: HttpService,
    private readonly smtpHost: string = 'localhost',
  ) {}

  public async hasSentTo(email: string): Promise<boolean> {
    const url = new URL(`http://${this.smtpHost}:8025/api/v2/search`);
    url.searchParams.set('kind', 'to');
    url.searchParams.set('query', email);
    const { data } = await lastValueFrom(
      this.http.get<{ total: number }>(url.toString()),
    );
    return data.total >= 1;
  }

  public async clear(): Promise<void> {
    const url = new URL(`http://${this.smtpHost}:8025/api/v1/messages`);
    await lastValueFrom(this.http.delete(url.toString()));
  }
}
