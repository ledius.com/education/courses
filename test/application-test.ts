import {
  INestApplication,
  INestApplicationContext,
  NotImplementedException,
  Type,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '@/app.module';
import * as request from 'supertest';
import { Connection, EntityTarget } from 'typeorm';
import { Mails } from './mails';
import { FixtureModule } from '@/fixture/fixture-module';
import { FixtureLoader } from '@/fixture/fixture-loader';
import { FixtureInterface } from '@/fixture/fixture.interface';
import { OAuthAccessTokenModel } from '@/auth/model/access/token/oauth-access-token-model';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { Email } from '@/auth/entity/email/email';
import { AccessTokenServiceInterface } from '@/auth/service/access/token/access-token-service-interface';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HttpModule, HttpService } from '@nestjs/axios';
import { EventEmitter2 } from '@nestjs/event-emitter';
import supertest = require('supertest');

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
BigInt.prototype.toJSON = function () {
  return this.toString();
};

/**
 * @class
 */
export class ApplicationTest implements INestApplicationContext {
  private constructor(private readonly app: INestApplication) {}

  public flushLogs(): void {
    this.app.flushLogs();
  }

  public static async initialize(
    module: Type = AppModule,
  ): Promise<ApplicationTest> {
    const moduleFixture = await Test.createTestingModule({
      imports: [module, HttpModule.register({}), ConfigModule.forRoot({})],
      providers: [
        {
          provide: Mails,
          useFactory: (http: HttpService, config: ConfigService): Mails => {
            return new Mails(http, config.get('SMTP_HOST'));
          },
          inject: [HttpService, ConfigService],
        },
      ],
    }).compile();
    const app = moduleFixture.createNestApplication();
    const self = new ApplicationTest(app);
    await self.app.init();
    await self.mails().clear();
    return self;
  }

  public request(): supertest.SuperTest<supertest.Test> {
    return request(this.app.getHttpServer());
  }

  public mails(): Mails {
    return this.app.get(Mails);
  }

  public select<T>(
    module: import('@nestjs/common').DynamicModule | Type<T>,
  ): INestApplicationContext {
    return this.app.select(module);
  }
  public get<TInput = any, TResult = TInput>(
    typeOrToken:
      | string
      | symbol
      | Type<TInput>
      | import('@nestjs/common').Abstract<TInput>,
    options?: { strict: boolean },
  ): TResult {
    return this.app.get(typeOrToken, options);
  }
  public resolve<TInput = any, TResult = TInput>(
    typeOrToken:
      | string
      | symbol
      | Type<TInput>
      | import('@nestjs/common').Abstract<TInput>,
    contextId?: { id: number },
    options?: { strict: boolean },
  ): Promise<TResult> {
    return this.app.resolve(typeOrToken, contextId, options);
  }
  public registerRequestByContextId<T = any>(
    request: T,
    contextId: { id: number },
  ): void {
    this.app.registerRequestByContextId(request, contextId);
  }
  public async close(): Promise<void> {
    await this.app.get(EventEmitter2).removeAllListeners();
    await this.app.close();
  }
  public useLogger(
    logger:
      | false
      | import('@nestjs/common').LoggerService
      | import('@nestjs/common').LogLevel[],
  ): void {
    this.app.useLogger(logger);
  }
  public enableShutdownHooks(
    signals?: import('@nestjs/common').ShutdownSignal[] | string[],
  ): this {
    this.app.enableShutdownHooks(signals);
    return this;
  }
  public init(): Promise<this> {
    throw new NotImplementedException();
  }

  public async loadFixture(
    fixture: FixtureInterface<unknown> | { new (): FixtureInterface<unknown> },
  ): Promise<void> {
    const loader = this.select(FixtureModule).get(FixtureLoader);
    await loader.load(typeof fixture === 'function' ? new fixture() : fixture);
  }

  public async loginByEmail(
    email: string,
    fixture?: FixtureInterface<unknown> | { new (): FixtureInterface<unknown> },
  ): Promise<OAuthAccessTokenModel> {
    if (fixture) {
      await this.loadFixture(fixture);
    }
    const user = await this.get(UserRepositoryInterface).getByEmail(
      new Email(email),
    );
    return await this.get(AccessTokenServiceInterface).releaseFor(user);
  }

  public async clear(entity: EntityTarget<unknown>): Promise<void> {
    await this.get(Connection).getRepository(entity).clear();
  }
}
