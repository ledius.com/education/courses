import { ApplicationTest } from '../../../application-test';
import { InitCommand } from '@/tinkoff/handler/init/init-command';
import { InitResultResponse } from '@/http/tinkoff/controller/init-result-response';
import * as nock from 'nock';
import { InitResult } from '@/tinkoff/handler/init/init-result';
import { OrderType } from '@/tinkoff/model/order-type';
import { AuthUserFixture } from '../../auth/v1/info/fixture/auth-user-fixture';

describe('Tinkoff init', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
  });

  describe('success', () => {
    beforeEach(() => {
      nock('https://securepay.tinkoff.ru')
        .post('/v2/Init')
        .reply(201, {
          Amount: 350000,
          OrderId: '10101',
          TerminalKey: 'testijdaww891',
          PaymentId: 123,
          ErrorCode: 0,
          Success: true,
          PaymentURL: 'https://payment.com/test/success',
        } as InitResult);
    });

    it('should be success init pay', async () => {
      const accessToken = await app.loginByEmail(
        'test@gmail.com',
        AuthUserFixture,
      );

      const command = new InitCommand();
      command.OrderId = '10101';
      command.CustomerKey = 'hello.world';
      command.Amount = 350000;
      command.DATA = {
        orderType: OrderType.COURSE,
      };
      const response = await app
        .request()
        .post('/api/v1/tinkoff/payment/init')
        .set({ authorization: accessToken.toString() })
        .send(command);
      const body: InitResultResponse = response.body;

      expect(response.status).toBe(201);
      expect(body.paymentUrl).toBe('https://payment.com/test/success');
    });
  });
});
