import { ApplicationTest } from '../../../../../application-test';
import { LoginEmailRequestCommand } from '@/auth/handler/login/email/request/login-email-request-command';
import { LoginEmailFixture } from './login-email-fixture';

describe('Login email request', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new LoginEmailFixture());
  });

  it('should be sent login code', async () => {
    const command = new LoginEmailRequestCommand();
    command.email = 'test@gmail.com';
    const response = await app
      .request()
      .post('/api/v1/auth/email/request')
      .send(command);

    expect(response.status).toBe(201);
    expect(await app.mails().hasSentTo('test@gmail.com')).toBeTruthy();
  });
});
