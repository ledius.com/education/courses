import { ApplicationTest } from '../../../../../application-test';
import { LoginEmailConfirmCommand } from '@/auth/handler/login/email/confirm/login-email-confirm-command';
import { AuthModule } from '@/auth/auth.module';
import { LoginRequestsInterface } from '@/auth/service/login/requests/login-requests-interface';
import { UserJoinFixture } from '../../join/fixture/user-join-fixture';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { Email } from '@/auth/entity/email/email';
import { Token } from '@/auth/entity/token';

describe('LoginEmailConfirm', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new UserJoinFixture());
  });

  it('should be confirm', async () => {
    const loginRequests = app.select(AuthModule).get(LoginRequestsInterface);
    const users = app.select(AuthModule).get(UserRepositoryInterface);
    const user = await users.getByEmail(new Email('test@gmail.com'));
    await loginRequests.add(user, Token.create('1234', new Date('2030')));
    const command = new LoginEmailConfirmCommand();
    command.email = 'test@gmail.com';
    command.code = '1234';
    const response = await app
      .request()
      .post('/api/v1/auth/email/confirm')
      .send(command);
    const body = response.body;

    expect(response.status).toBe(200);
    expect(body.accessToken).toBeDefined();
    expect(body.refreshToken).toBeDefined();
    expect(body.expiresIn).toBeDefined();
    expect(await loginRequests.has(user, command.code)).toBeFalsy();
  });
});
