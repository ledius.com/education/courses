import { AuthModule } from '@/auth/auth.module';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { Id } from '@/auth/entity/id/id';
import { JoinResponse } from '@/http/auth/v1/join/response/join-response';
import { Connection } from 'typeorm';
import { User } from '@/auth/entity/user';
import { ApplicationTest } from '../../../../application-test';
import { DomainExceptionResponse } from '@/http/common/response/domain-exception-response';
import { UserJoinFixture } from './fixture/user-join-fixture';

describe('Join Request', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.get(Connection).getRepository(User).clear();
  });

  describe('success join', () => {
    it('should be join', async () => {
      const payload = {
        email: 'test@gmail.com',
        phone: '79609889797',
      };

      const res = await app.request().post('/api/v1/auth/join').send(payload);
      const response: JoinResponse = res.body;

      expect(res.status).toBe(201);
      expect(response.id).toBeDefined();
      expect(response.email).toBe(payload.email);
      expect(response.phone).toBe(payload.phone);

      const joinedUser = await app
        .select(AuthModule)
        .get(UserRepositoryInterface)
        .getById(new Id(response.id));

      expect(joinedUser.getId().getValue()).toBe(response.id);
      expect(joinedUser.getEmail().getValue()).toBe(payload.email);
      expect(joinedUser.getPhone()?.getValue()).toBe(payload.phone);
    });
  });

  describe('failure join', () => {
    beforeEach(async () => {
      await app.loadFixture(new UserJoinFixture());
    });

    it('should be not joined because email already used', async () => {
      const payload = {
        email: 'test@gmail.com',
        phone: '79619889797',
      };

      const res = await app
        .request()
        .post('/api/v1/auth/join')
        .set('accept-language', 'en')
        .send(payload);
      const response: DomainExceptionResponse = res.body;

      expect(res.status).toBe(409);
      expect(response.message).toBe('Email already used');
    });

    it('should be not joined because phone already used', async () => {
      const payload = {
        email: 'test1@gmail.com',
        phone: '79609889797',
      };

      const res = await app
        .request()
        .post('/api/v1/auth/join')
        .set('accept-language', 'en')
        .send(payload);
      const response: DomainExceptionResponse = res.body;

      expect(res.status).toBe(409);
      expect(response.message).toBe('Phone already used');
    });
  });
});
