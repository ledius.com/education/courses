import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { FixtureInterface } from '@/fixture/fixture.interface';
import { EntityTarget } from 'typeorm';

export class UserJoinFixture implements FixtureInterface<User> {
  public getEntity(): EntityTarget<User> {
    return User;
  }

  public async getFixtures(): Promise<User[]> {
    return [
      User.join(
        Id.next(),
        new Email('test@gmail.com'),
        new Phone('79609889797'),
      ),
    ];
  }
}
