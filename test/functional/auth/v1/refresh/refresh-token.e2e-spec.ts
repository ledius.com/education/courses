import { ApplicationTest } from '../../../../application-test';
import { TokenRefreshCommand } from '@/auth/handler/refresh/token-refresh-command';
import { RefreshResponse } from '@/http/auth/v1/refresh/refresh-response';
import { OAuthAccessTokenModel } from '@/auth/model/access/token/oauth-access-token-model';
import { RefreshFixture } from './refresh-fixture';

describe('Refresh Token', () => {
  let app: ApplicationTest;
  let token: OAuthAccessTokenModel;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new RefreshFixture());
    token = await app.loginByEmail('test@gmail.com');
  });

  it('should be refresh token', async () => {
    const command = new TokenRefreshCommand();
    command.refresh = token.getRefresh();
    const response = await app
      .request()
      .post('/api/v1/auth/refresh')
      .send(command);

    const body: RefreshResponse = response.body;
    expect(response.status).toBe(201);
    expect(body.type).toBe('bearer');
    expect(body.accessToken).toBeDefined();
    expect(body.refreshToken).toBeDefined();
    expect(body.expiresIn).toBeDefined();
  });
});
