import { ApplicationTest } from '../../../../application-test';
import { AuthUserFixture } from './fixture/auth-user-fixture';
import { AuthInfoResponse } from '@/http/auth/v1/info/controller/auth-info-response';

describe('Auth User Info', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new AuthUserFixture());
  });

  it('should be returns 401 unauthorized if token not passed', async () => {
    const response = await app.request().get('/api/v1/auth/info');
    expect(response.status).toBe(401);
    expect(response.body.message).toBe('Unauthorized');
  });

  it('should be return success response', async () => {
    const accessToken = await app.loginByEmail('test@gmail.com');
    const response = await app
      .request()
      .get('/api/v1/auth/info')
      .set({ authorization: accessToken.toString() });
    const body = response.body as AuthInfoResponse;

    expect(response.status).toBe(200);
    expect(body.id).toBe('6bebd3c4-fae8-4755-ac97-93d28ed40d72');
    expect(body.email).toBe('test@gmail.com');
    expect(body.phone).toBe('79609889797');
  });
});
