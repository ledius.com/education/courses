import { FixtureInterface } from '@/fixture/fixture.interface';
import { User } from '@/auth/entity/user';
import { EntityTarget } from 'typeorm';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { Role } from '@/permission/role.enum';

export class AuthUserFixture implements FixtureInterface<User> {
  public getEntity(): EntityTarget<User> {
    return User;
  }

  public async getFixtures(): Promise<User[]> {
    return [
      User.join(
        new Id('6bebd3c4-fae8-4755-ac97-93d28ed40d72'),
        new Email('test@gmail.com'),
        new Phone('79609889797'),
      ).addRole(Role.ROOT),
    ];
  }
}
