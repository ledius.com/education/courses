import { ApplicationTest } from '../../../../application-test';
import { GetByIdLessonFixture } from './fixture/get-by-id-lesson-fixture';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';

describe('get by id lesson', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new GetByIdLessonFixture());
  });

  it('should be return lesson by id', async () => {
    const response = await app
      .request()
      .get('/api/v1/lesson/9963118b-1c8b-45a7-a479-6406d047c4cb');

    const body: LessonResponse = response.body;

    expect(response.status).toBe(200);
    expect(body.id).toBe('9963118b-1c8b-45a7-a479-6406d047c4cb');
    expect(body.content.name).toBe('Test');
    expect(body.content.icon).toBe('test.svg');
    expect(body.content.text).toBe('hello world');
    expect(body.courseId).toBe('e09d655a-b5fa-4e14-86f7-64a33a0bd4ec');
  });
});
