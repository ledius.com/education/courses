import { FixtureInterface } from '@/fixture/fixture.interface';
import { Lesson } from '@/lesson/entity/lesson';
import { EntityTarget } from 'typeorm';
import { CourseId } from '@/lesson/entity/course/course-id';
import { Content } from '@/lesson/entity/content/content';
import { Name } from '@/lesson/entity/content/name/name';
import { Icon } from '@/lesson/entity/content/icon/icon';
import { Text } from '@/lesson/entity/content/text/text';
import { Id } from '@/lesson/entity/id/id';

export class GetByIdLessonFixture implements FixtureInterface<Lesson> {
  public getEntity(): EntityTarget<Lesson> {
    return Lesson;
  }

  public async getFixtures(): Promise<Lesson[]> {
    return [
      Lesson.create(
        new Id('9963118b-1c8b-45a7-a479-6406d047c4cb'),
        new CourseId('e09d655a-b5fa-4e14-86f7-64a33a0bd4ec'),
        new Content(
          new Name('Test'),
          new Icon('test.svg'),
          new Text('hello world'),
        ),
      ),
    ];
  }
}
