import { Lesson } from '@/lesson/entity/lesson';
import { EntityTarget } from 'typeorm';
import { Id } from '@/lesson/entity/id/id';
import { CourseId } from '@/lesson/entity/course/course-id';
import { Content } from '@/lesson/entity/content/content';
import { Name } from '@/lesson/entity/content/name/name';
import { Icon } from '@/lesson/entity/content/icon/icon';
import { FixtureInterface } from '@/fixture/fixture.interface';

export class ListLessonFixture implements FixtureInterface<Lesson> {
  public getEntity(): EntityTarget<Lesson> {
    return Lesson;
  }

  public async getFixtures(): Promise<Lesson[]> {
    return [
      Lesson.create(
        new Id('06a864d8-853f-4162-92cf-37bab45afe5b'),
        new CourseId('b52888d9-a4c8-4b9f-9d2d-d7afc810784c'),
        new Content(new Name('Default name1'), new Icon('default.icon1')),
      ),
      Lesson.create(
        new Id('9a7d3e7c-b28a-44fb-a0fe-a87a531feece'),
        new CourseId('b52888d9-a4c8-4b9f-9d2d-d7afc810784c'),
        new Content(new Name('Default name2'), new Icon('default.icon2')),
      ),
      Lesson.create(
        new Id('2b2b091d-5f59-4b3b-9f99-a805d8ea7e7c'),
        new CourseId('6d724db9-ccf5-444e-8e7c-fc4ac261ad34'),
        new Content(new Name('Default name3'), new Icon('default.icon3')),
      ),
    ];
  }
}
