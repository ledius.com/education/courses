import { ApplicationTest } from '../../../../application-test';
import { ListLessonFixture } from './fixture/list-lesson-fixture';
import { ListLessonResponse } from '@/http/lesson/controller/list/list-lesson-response';

describe('List Lesson', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new ListLessonFixture());
  });

  it('should be return all entities', async () => {
    const response = await app.request().get('/api/v1/lessons');

    const body: ListLessonResponse = response.body;

    expect(response.status).toBe(200);
    expect(body.items).toHaveLength(3);
    expect(body.items[0]?.id).toBe('06a864d8-853f-4162-92cf-37bab45afe5b');
    expect(body.items[0]?.content.name).toBe('Default name1');
    expect(body.items[0]?.content.icon).toBe('default.icon1');
    expect(body.items[0]?.courseId).toBe(
      'b52888d9-a4c8-4b9f-9d2d-d7afc810784c',
    );
    expect(body.items[0]?.isDraft).toBeTruthy();
    expect(body.items[0]?.isPublished).toBeFalsy();

    expect(body.items[1]?.id).toBe('9a7d3e7c-b28a-44fb-a0fe-a87a531feece');
    expect(body.items[1]?.content.name).toBe('Default name2');
    expect(body.items[1]?.content.icon).toBe('default.icon2');
    expect(body.items[1]?.courseId).toBe(
      'b52888d9-a4c8-4b9f-9d2d-d7afc810784c',
    );
    expect(body.items[1]?.isDraft).toBeTruthy();
    expect(body.items[1]?.isPublished).toBeFalsy();

    expect(body.items[2]?.id).toBe('2b2b091d-5f59-4b3b-9f99-a805d8ea7e7c');
    expect(body.items[2]?.content.name).toBe('Default name3');
    expect(body.items[2]?.content.icon).toBe('default.icon3');
    expect(body.items[2]?.courseId).toBe(
      '6d724db9-ccf5-444e-8e7c-fc4ac261ad34',
    );
    expect(body.items[2]?.isDraft).toBeTruthy();
    expect(body.items[2]?.isPublished).toBeFalsy();
  });

  it('should be return by course id', async () => {
    const response = await app
      .request()
      .get('/api/v1/lessons?courseId=b52888d9-a4c8-4b9f-9d2d-d7afc810784c');

    const body: ListLessonResponse = response.body;

    expect(response.status).toBe(200);
    expect(body.items).toHaveLength(2);
    expect(body.items[0]?.id).toBe('06a864d8-853f-4162-92cf-37bab45afe5b');
    expect(body.items[0]?.content.name).toBe('Default name1');
    expect(body.items[0]?.content.icon).toBe('default.icon1');
    expect(body.items[0]?.courseId).toBe(
      'b52888d9-a4c8-4b9f-9d2d-d7afc810784c',
    );
    expect(body.items[0]?.isDraft).toBeTruthy();
    expect(body.items[0]?.isPublished).toBeFalsy();

    expect(body.items[1]?.id).toBe('9a7d3e7c-b28a-44fb-a0fe-a87a531feece');
    expect(body.items[1]?.content.name).toBe('Default name2');
    expect(body.items[1]?.content.icon).toBe('default.icon2');
    expect(body.items[1]?.courseId).toBe(
      'b52888d9-a4c8-4b9f-9d2d-d7afc810784c',
    );
    expect(body.items[1]?.isDraft).toBeTruthy();
    expect(body.items[1]?.isPublished).toBeFalsy();
  });
});
