import { ApplicationTest } from '../../../../application-test';
import { CreateLessonCommand } from '@/lesson/handler/create/create-lesson-command';
import { v4 } from 'uuid';
import { LessonRepositoryInterface } from '@/lesson/repository/lesson-repository-interface.interface';
import { Id } from '@/lesson/entity/id/id';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';
import { AuthUserFixture } from '../../../auth/v1/info/fixture/auth-user-fixture';

describe('Create Lesson', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
  });

  it('should be create lesson', async () => {
    const accessToken = await app.loginByEmail(
      'test@gmail.com',
      AuthUserFixture,
    );

    const command = new CreateLessonCommand();
    command.courseId = v4();
    command.icon = 'icon';
    command.name = 'name';

    const response = await app
      .request()
      .post('/api/v1/lesson')
      .set({ authorization: accessToken.toString() })
      .send(command);

    const body: LessonResponse = response.body;

    expect(response.status).toBe(201);
    expect(body.id).toBeDefined();
    expect(body.content.name).toBe(command.name);
    expect(body.content.icon).toBe(command.icon);
    expect(body.courseId).toBe(command.courseId);
    expect(body.isDraft).toBeTruthy();
    expect(body.isPublished).toBeFalsy();

    const lesson = await app
      .get(LessonRepositoryInterface)
      .getById(new Id(body.id));
    expect(lesson).toBeDefined();
    expect(lesson.getCourseId().getValue()).toBe(body.courseId);
    expect(lesson.getContent().getName().getValue()).toBe(body.content.name);
    expect(lesson.getContent().getIcon().getValue()).toBe(body.content.icon);
  });
});
