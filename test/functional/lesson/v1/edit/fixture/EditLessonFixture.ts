import { Lesson } from '@/lesson/entity/lesson';
import { EntityTarget } from 'typeorm';
import { Id } from '@/lesson/entity/id/id';
import { CourseId } from '@/lesson/entity/course/course-id';
import { Content } from '@/lesson/entity/content/content';
import { Name } from '@/lesson/entity/content/name/name';
import { Icon } from '@/lesson/entity/content/icon/icon';
import { FixtureInterface } from '@/fixture/fixture.interface';

export class EditLessonFixture implements FixtureInterface<Lesson> {
  public getEntity(): EntityTarget<Lesson> {
    return Lesson;
  }

  public async getFixtures(): Promise<Lesson[]> {
    return [
      Lesson.create(
        new Id('06a864d8-853f-4162-92cf-37bab45afe5b'),
        new CourseId('b52888d9-a4c8-4b9f-9d2d-d7afc810784c'),
        new Content(new Name('Default name'), new Icon('default.icon')),
      ),
    ];
  }
}
