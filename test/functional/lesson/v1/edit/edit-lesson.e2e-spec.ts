import { ApplicationTest } from '../../../../application-test';
import { v4 } from 'uuid';
import { LessonRepositoryInterface } from '@/lesson/repository/lesson-repository-interface.interface';
import { Id } from '@/lesson/entity/id/id';
import { EditLessonFixture } from './fixture/EditLessonFixture';
import { EditLessonCommand } from '@/lesson/handler/edit/edit-lesson-command';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';
import { AuthUserFixture } from '../../../auth/v1/info/fixture/auth-user-fixture';

describe('Edit Lesson', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new EditLessonFixture());
  });

  it('should be create lesson', async () => {
    const accessToken = await app.loginByEmail(
      'test@gmail.com',
      AuthUserFixture,
    );
    const command = new EditLessonCommand();
    command.courseId = v4();
    command.icon = 'updated.icon';
    command.name = 'New name';
    command.id = '06a864d8-853f-4162-92cf-37bab45afe5b';
    command.text = 'Hello text';

    const response = await app
      .request()
      .put('/api/v1/lesson')
      .set({ authorization: accessToken.toString() })
      .send(command);

    const body: LessonResponse = response.body;

    expect(response.status).toBe(200);
    expect(body.id).toBeDefined();
    expect(body.id).toBe(command.id);
    expect(body.content.name).toBe(command.name);
    expect(body.content.icon).toBe(command.icon);
    expect(body.courseId).toBe(command.courseId);
    expect(body.isDraft).toBeTruthy();
    expect(body.isPublished).toBeFalsy();

    const lesson = await app
      .get(LessonRepositoryInterface)
      .getById(new Id(body.id));
    expect(lesson).toBeDefined();
    expect(lesson.getCourseId().getValue()).toBe(body.courseId);
    expect(lesson.getContent().getName().getValue()).toBe(body.content.name);
    expect(lesson.getContent().getIcon().getValue()).toBe(body.content.icon);
  });
});
