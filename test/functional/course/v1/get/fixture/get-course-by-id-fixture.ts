import { FixtureInterface } from '@/fixture/fixture.interface';
import { Course } from '@/course/dao/entity/course';
import { EntityTarget } from 'typeorm';
import { Id } from '@/course/dao/entity/id/id';
import { Content } from '@/course/dao/entity/content/content';
import { Name } from '@/course/dao/entity/content/name/name';
import { Icon } from '@/course/dao/entity/content/icon/icon';
import { Price } from '@/course/dao/entity/price/price';

export class GetCourseByIdFixture implements FixtureInterface<Course> {
  public getEntity(): EntityTarget<Course> {
    return Course;
  }

  public async getFixtures(): Promise<Course[]> {
    return [
      Course.create(
        new Id('f2789086-7cb3-43c4-b2e4-222fd7ae38ac'),
        new Content(new Name('Test'), new Icon('icon.svg')),
        new Price(1000),
      ),
    ];
  }
}
