import { ApplicationTest } from '../../../../application-test';
import { GetCourseByIdFixture } from './fixture/get-course-by-id-fixture';
import { CourseResponse } from '@/course/controller/course-response';

describe('Get Course by Id', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new GetCourseByIdFixture());
  });

  it('should be return course by id', async () => {
    const response = await app
      .request()
      .get('/api/v1/courses/f2789086-7cb3-43c4-b2e4-222fd7ae38ac');
    const body: CourseResponse = response.body;

    expect(response.status).toBe(200);
    expect(body.id).toBe('f2789086-7cb3-43c4-b2e4-222fd7ae38ac');
    expect(body.isPublished).toBeFalsy();
    expect(body.price).toBe(1000);
    expect(body.content.name).toBe('Test');
    expect(body.content.icon).toBe('icon.svg');
  });
});
