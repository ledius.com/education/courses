import { ApplicationTest } from '../../../../application-test';
import { CourseListFixture } from './fixture/course-list-fixture';
import { Course } from '@/course/dao/entity/course';

describe('List Course', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new CourseListFixture());
  });

  afterEach(async () => {
    await app.clear(Course);
  });

  it('should be return list', async () => {
    const { status, body } = await app.request().get('/api/v1/courses');

    expect(status).toBe(200);
    expect(body.items).toHaveLength(3);
  });
});
