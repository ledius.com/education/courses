import { Course } from '@/course/dao/entity/course';
import { EntityTarget } from 'typeorm';
import { CourseBuilder } from '@/course/builder/course-builder';
import { FixtureInterface } from '@/fixture/fixture.interface';

export class CourseListFixture implements FixtureInterface<Course> {
  public getEntity(): EntityTarget<Course> {
    return Course;
  }

  public async getFixtures(): Promise<Course[]> {
    return [
      new CourseBuilder().build(),
      new CourseBuilder().build(),
      new CourseBuilder().build(),
    ];
  }
}
