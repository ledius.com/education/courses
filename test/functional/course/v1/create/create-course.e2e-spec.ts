import { ApplicationTest } from '../../../../application-test';
import { CreateCourseCommand } from '@/course/handler/create/create-course-command';
import { CourseModule } from '@/course/course-module';
import { CourseRepositoryInterface } from '@/course/repository/course-repository.interface';
import { Id } from '@/course/dao/entity/id/id';
import { Course } from '@/course/dao/entity/course';
import { AuthUserFixture } from '../../../auth/v1/info/fixture/auth-user-fixture';

describe('Create Course', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.clear(Course);
  });

  afterEach(async () => {
    await app.clear(Course);
  });

  it('should be create course', async () => {
    const accessToken = await app.loginByEmail(
      'test@gmail.com',
      AuthUserFixture,
    );
    const command = new CreateCourseCommand();
    command.price = 100;
    command.icon = 'test';
    command.name = 'same';

    const { body, status } = await app
      .request()
      .post('/api/v1/courses')
      .set({ authorization: accessToken.toString() })
      .send(command);

    expect(status).toBe(201);
    expect(body.id).toBeDefined();
    expect(body.content.icon).toBe(command.icon);
    expect(body.content.name).toBe(command.name);
    expect(body.published).toBeFalsy();

    const course = await app
      .select(CourseModule)
      .get(CourseRepositoryInterface)
      .getById(new Id(body.id));
    expect(course).toBeDefined();
  });
});
