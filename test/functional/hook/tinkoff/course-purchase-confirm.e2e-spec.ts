import { ApplicationTest } from '../../../application-test';
import { CourseFixture } from './fixture/course-fixture';
import { CoursePurchaseConfirmFixture } from './fixture/course-purchase-confirm-fixture';
import { NotificationCommand } from '@/hook/tinkoff/dispatcher/notification-command';
import { CoursePurchaseModule } from '@/purchase/course/course-purchase-module';
import { CoursePurchaseRepositoryInterface } from '@/purchase/course/repository/course-purchase-repository.interface';
import { Id } from '@/purchase/course/entity/id/id';
import { Status } from '@/tinkoff/model/status';
import { OrderType } from '@/tinkoff/model/order-type';

describe('Course Purchase Confirm', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
  });

  describe('success pay confirm', () => {
    beforeEach(async () => {
      await app.loadFixture(new CourseFixture());
      await app.loadFixture(new CoursePurchaseConfirmFixture());
    });

    it('should be change status of purchase', async () => {
      const command = new NotificationCommand();
      command.Status = Status.CONFIRMED;
      command.OrderId = '243b603a-0340-4422-b140-44f62a657edf';
      command.Amount = 10000;
      command.Data = { orderType: OrderType.COURSE };
      command.Success = true;
      const response = await app
        .request()
        .post('/api/v1/webhook/tinkoff/notification')
        .send(command);

      expect(response.status).toBe(200);
      expect(response.text).toBe('OK');

      const purchase = await app
        .select(CoursePurchaseModule)
        .get(CoursePurchaseRepositoryInterface)
        .getById(new Id('243b603a-0340-4422-b140-44f62a657edf'));

      expect(purchase.isPaid()).toBeTruthy();
      expect(purchase.isNew()).toBeFalsy();
    });
  });
});
