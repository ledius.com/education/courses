import { FixtureInterface } from '@/fixture/fixture.interface';
import { Course } from '@/course/dao/entity/course';
import { EntityTarget } from 'typeorm';
import { Content } from '@/course/dao/entity/content/content';
import { Name } from '@/course/dao/entity/content/name/name';
import { Icon } from '@/course/dao/entity/content/icon/icon';
import { Price } from '@/course/dao/entity/price/price';
import { Id } from '@/course/dao/entity/id/id';

export class CourseFixture implements FixtureInterface<Course> {
  public getEntity(): EntityTarget<Course> {
    return Course;
  }

  public async getFixtures(): Promise<Course[]> {
    return [
      Course.create(
        new Id('2cea0503-1a82-4aa7-b3f8-84c3c8b255bd'),
        new Content(new Name('doesnt matter'), new Icon('no')),
        new Price(100),
      ),
    ];
  }
}
