import { FixtureInterface } from '@/fixture/fixture.interface';
import { CoursePurchase } from '@/purchase/course/entity/course-purchase';
import { EntityTarget } from 'typeorm';
import { Id } from '@/purchase/course/entity/id/id';
import { CourseId } from '@/purchase/course/entity/course/id/course-id';
import { UserId } from '@/purchase/course/entity/user/id/user-id';

export class CoursePurchaseConfirmFixture
  implements FixtureInterface<CoursePurchase>
{
  public getEntity(): EntityTarget<CoursePurchase> {
    return CoursePurchase;
  }

  public async getFixtures(): Promise<CoursePurchase[]> {
    return [
      CoursePurchase.purchase(
        new Id('243b603a-0340-4422-b140-44f62a657edf'),
        new CourseId('2cea0503-1a82-4aa7-b3f8-84c3c8b255bd'),
        new UserId('fd995946-5cb7-4912-80ba-9165b9fd92cd'),
      ),
    ];
  }
}
