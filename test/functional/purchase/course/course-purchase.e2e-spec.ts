import { ApplicationTest } from '../../../application-test';
import { CoursePurchaseResponse } from '@/http/purchase/course/controller/response/course-purchase-response';
import { CoursePurchaseCommand } from '@/purchase/course/handler/purchase/course-purchase-command';
import { v4 } from 'uuid';
import { CoursePurchaseRepositoryInterface } from '@/purchase/course/repository/course-purchase-repository.interface';
import { Id } from '@/purchase/course/entity/id/id';
import { AuthUserFixture } from '../../auth/v1/info/fixture/auth-user-fixture';

describe('CoursePurchase', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
  });

  it('should be purchase course', async () => {
    const accessToken = await app.loginByEmail(
      'test@gmail.com',
      AuthUserFixture,
    );
    const command = new CoursePurchaseCommand();
    command.userId = v4();
    command.courseId = v4();

    const response = await app
      .request()
      .post('/api/v1/purchase/course')
      .set({ authorization: accessToken.toString() })
      .send(command);
    const body: CoursePurchaseResponse = response.body;

    expect(response.status).toBe(201);
    expect(body.id).toBeDefined();
    expect(body.userId).toBe(command.userId);
    expect(body.courseId).toBe(command.courseId);

    const found = await app
      .get(CoursePurchaseRepositoryInterface)
      .getById(new Id(body.id));

    expect(found.getId().getValue()).toBe(body.id);
    expect(found.getUserId().getValue()).toBe(body.userId);
    expect(found.getCourseId().getValue()).toBe(body.courseId);
  });
});
