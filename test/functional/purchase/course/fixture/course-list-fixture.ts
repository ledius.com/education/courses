import { CoursePurchase } from '@/purchase/course/entity/course-purchase';
import { EntityTarget } from 'typeorm';
import { Id } from '@/purchase/course/entity/id/id';
import { CourseId } from '@/purchase/course/entity/course/id/course-id';
import { UserId } from '@/purchase/course/entity/user/id/user-id';
import { FixtureInterface } from '@/fixture/fixture.interface';

export class CourseListFixture implements FixtureInterface<CoursePurchase> {
  public getEntity(): EntityTarget<CoursePurchase> {
    return CoursePurchase;
  }

  public async getFixtures(): Promise<CoursePurchase[]> {
    return [
      CoursePurchase.purchase(
        new Id('26edba14-ea9e-4f0b-883b-a537ec25b209'),
        new CourseId('69271bc2-eecc-4a70-864c-3b17a97da920'),
        new UserId('16d84ccc-a097-4c49-b680-d0aab9c99f1a'),
      ),
      CoursePurchase.purchase(
        new Id('d0a5e202-2912-47bf-b2e2-3b84645ac597'),
        new CourseId('f70a9c28-b6db-4178-9ed8-f5e6af32d8ec'),
        new UserId('16d84ccc-a097-4c49-b680-d0aab9c99f1a'),
      ).pay(),
      CoursePurchase.purchase(
        new Id('af75a6c5-f901-481a-b06b-bdfb337228eb'),
        new CourseId('f70a9c28-b6db-4178-9ed8-f5e6af32d8ec'),
        new UserId('d8d920cf-c730-4ee7-bf33-712667c85689'),
      ),
    ];
  }
}
