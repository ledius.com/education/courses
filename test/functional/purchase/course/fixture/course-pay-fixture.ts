import { FixtureInterface } from '@/fixture/fixture.interface';
import { EntityTarget } from 'typeorm';
import { Course } from '@/course/dao/entity/course';
import { Content } from '@/course/dao/entity/content/content';
import { Name } from '@/course/dao/entity/content/name/name';
import { Icon } from '@/course/dao/entity/content/icon/icon';
import { Price } from '@/course/dao/entity/price/price';
import { Id } from '@/course/dao/entity/id/id';

export class CoursePayFixture implements FixtureInterface<Course> {
  public getEntity(): EntityTarget<Course> {
    return Course;
  }

  public async getFixtures(): Promise<Course[]> {
    return [
      Course.create(
        new Id('4d96a21d-d5f4-43b2-928e-39bc2241a0e0'),
        new Content(new Name('unknown'), new Icon('undefined')),
        new Price(100),
      ),
    ];
  }
}
