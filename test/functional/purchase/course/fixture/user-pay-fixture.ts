import { FixtureInterface } from '@/fixture/fixture.interface';
import { User } from '@/auth/entity/user';
import { EntityTarget } from 'typeorm';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';

export class UserPayFixture implements FixtureInterface<User> {
  public getEntity(): EntityTarget<User> {
    return User;
  }

  public async getFixtures(): Promise<User[]> {
    return [
      User.join(
        new Id('a2abd63d-b3fa-419b-a185-9e27ec90623e'),
        new Email('test@gmail.com'),
        new Phone('79609889797'),
      ),
    ];
  }
}
