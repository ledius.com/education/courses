import { ApplicationTest } from '../../../application-test';
import { CoursePurchasePayCommand } from '@/purchase/course/handler/pay/course-purchase-pay-command';
import { CoursePurchasePayResponse } from '@/http/purchase/course/controller/pay/course-purchase-pay-response';
import nock = require('nock');
import { InitResult } from '@/tinkoff/handler/init/init-result';
import { CoursePayFixture } from './fixture/course-pay-fixture';
import { UserPayFixture } from './fixture/user-pay-fixture';
import { AuthUserFixture } from '../../auth/v1/info/fixture/auth-user-fixture';

describe('Course Purchase Pay', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new AuthUserFixture());
  });

  describe('success', () => {
    beforeEach(async () => {
      await app.loadFixture(new CoursePayFixture());
      await app.loadFixture(new UserPayFixture());
      nock('https://securepay.tinkoff.ru')
        .post('/v2/Init')
        .reply(201, {
          Amount: 10000,
          OrderId: '10101',
          TerminalKey: 'testijdaww891',
          PaymentId: 123,
          ErrorCode: 0,
          Success: true,
          PaymentURL: 'https://payment.com/test/success',
        } as InitResult);
    });

    it('should be return pay result', async () => {
      const accessToken = await app.loginByEmail('test@gmail.com');

      const command = new CoursePurchasePayCommand();
      command.userId = 'a2abd63d-b3fa-419b-a185-9e27ec90623e';
      command.courseId = '4d96a21d-d5f4-43b2-928e-39bc2241a0e0';
      const response = await app
        .request()
        .post('/api/v1/purchase/course/pay')
        .set({ authorization: accessToken.toString() })
        .send(command);
      const body: CoursePurchasePayResponse = response.body;

      expect(response.status).toBe(201);
      expect(body.id).toBeDefined();
      expect(body.userId).toBe(command.userId);
      expect(body.courseId).toBe(command.courseId);
      expect(body.paymentUrl).toBe('https://payment.com/test/success');
    });
  });
});
