import { ApplicationTest } from '../../../application-test';
import { CourseListFixture } from './fixture/course-list-fixture';
import { CoursePurchaseListResponse } from '@/http/purchase/course/controller/list/course-purchase-list-response';
import { CoursePurchaseListQuery } from '@/purchase/course/handler/list/course-purchase-list-query';
import { AuthUserFixture } from '../../auth/v1/info/fixture/auth-user-fixture';

describe('CoursePurchaseList', () => {
  let app: ApplicationTest;

  beforeEach(async () => {
    app = await ApplicationTest.initialize();
    await app.loadFixture(new CourseListFixture());
  });

  it('should be return purchased course by user', async () => {
    const accessToken = await app.loginByEmail(
      'test@gmail.com',
      AuthUserFixture,
    );

    const query = new CoursePurchaseListQuery();
    query.userId = '16d84ccc-a097-4c49-b680-d0aab9c99f1a';
    const response = await app
      .request()
      .get('/api/v1/purchases/course')
      .set({ authorization: accessToken.toString() })
      .query(query);
    const body: CoursePurchaseListResponse = response.body;

    expect(response.status).toBe(200);
    expect(body.items).toHaveLength(2);
    expect(body.items[0]?.courseId).toBe(
      '69271bc2-eecc-4a70-864c-3b17a97da920',
    );
    expect(body.items[0]?.isPaid).toBeFalsy();
    expect(body.items[1]?.courseId).toBe(
      'f70a9c28-b6db-4178-9ed8-f5e6af32d8ec',
    );
    expect(body.items[1]?.isPaid).toBeTruthy();
  });
});
