#!/usr/bin/node

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { execSync } = require('child_process');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { readFileSync } = require('fs');

console.log(execSync('npm run test:cov').toString());

// eslint-disable-next-line @typescript-eslint/no-var-requires
const parseString = require('xml2js').parseString;
parseString(
  readFileSync('./coverage/clover.xml', { encoding: 'utf8' }),
  function (err, result) {
    const obj = JSON.parse(JSON.stringify(result));
    const [statements, coveredStatements] = [
      obj.coverage.project.reduce(
        (total, item) => total + Number(item.metrics[0].$.statements),
        0,
      ),
      obj.coverage.project.reduce(
        (total, item) => total + Number(item.metrics[0].$.coveredstatements),
        0,
      ),
    ];

    const percentStatementsCoverage = Math.round(
      (coveredStatements / statements) * 100,
    );
    const min = Number(process.argv[2] || 95);

    if (percentStatementsCoverage < min) {
      console.error(
        `Test coverage is reach min limit ${percentStatementsCoverage}/${min}`,
      );
      process.exit(1);
    }

    console.log(`Test coverage passed ${percentStatementsCoverage}/${min}`);
  },
);
