// eslint-disable-next-line @typescript-eslint/no-var-requires
const { SnakeNamingStrategy } = require('typeorm-naming-strategies');

module.exports = {
  name: 'default',
  type: 'postgres',
  host: 'postgres',
  username: 'postgres',
  password: '1234',
  port: '5432',
  database: 'hodl-invest',
  logging: true,
  namingStrategy: new SnakeNamingStrategy(),
  entities: ['dist/**/entity/**.js'],
  migrations: ['dist/migrations/*.js', 'dist/*/dao/migrations/*.js'],
  cli: {
    migrationsDir: 'src/migrations',
  },
};
