/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
  CourseRepositoryInterface,
  FindOptions,
} from './course-repository.interface';
import { Course } from '../dao/entity/course';
import { Id } from '../dao/entity/id/id';
import { FindConditions, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DomainException } from 'node-exceptions';

export class PostgresCourseRepository implements CourseRepositoryInterface {
  public constructor(
    @InjectRepository(Course) private readonly repository: Repository<Course>,
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async add(course: Course): Promise<Course> {
    return await this.repository.save(course);
  }

  public async find(options: FindOptions = {}): Promise<Course[]> {
    let where: FindConditions<Course> = {};
    if (options.tag) {
      where = {
        ...where,
        tag: {
          id: options.tag,
        },
      };
    }
    return await this.repository.find({ where });
  }

  public async getById(id: Id): Promise<Course> {
    const found = await this.repository.findOne(id.getValue());
    if (!found) {
      throw new DomainException('Course not found');
    }
    return found;
  }

  public async findById(id: Id): Promise<Course | undefined> {
    return await this.repository.findOne(id.getValue());
  }

  public async update(course: Course): Promise<void> {
    await this.repository.save(course);
  }
}
