/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { GetCourseByIdHandler } from './get-course-by-id-handler';
import { CourseBuilder } from '../../builder/course-builder';
import { mock } from 'jest-mock-extended';
import { CourseRepositoryInterface } from '../../repository/course-repository.interface';
import { GetCourseByIdQuery } from './get-course-by-id-query';

describe('GetCourseByIdHandler', () => {
  it('should be defined', async () => {
    const expectedCourse = new CourseBuilder().build();
    const repository = mock<CourseRepositoryInterface>();
    repository.getById.mockReturnValue(Promise.resolve(expectedCourse));
    const handler = new GetCourseByIdHandler(repository);
    const course = await handler.execute(
      GetCourseByIdQuery.of(expectedCourse.getId().getValue()),
    );

    expect(course).toEqual(expectedCourse);
    expect(repository.getById).toBeCalledWith(expectedCourse.getId());
  });
});
