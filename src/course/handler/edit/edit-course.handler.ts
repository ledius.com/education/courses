import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EditCourseCommand } from '@/course/handler/edit/edit-course.command';
import { Course } from '@/course/dao/entity/course';
import { CourseRepositoryInterface } from '@/course/repository/course-repository.interface';
import { Id } from '@/course/dao/entity/id/id';
import { Content } from '@/course/dao/entity/content/content';
import { Name } from '@/course/dao/entity/content/name/name';
import { Icon } from '@/course/dao/entity/content/icon/icon';
import { Price } from '@/course/dao/entity/price/price';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';

@CommandHandler(EditCourseCommand)
export class EditCourseHandler
  implements ICommandHandler<EditCourseCommand, Course>
{
  public constructor(
    private readonly courseRepository: CourseRepositoryInterface,
    private readonly tagPgRepository: TagPgRepository,
  ) {}

  public async execute(command: EditCourseCommand): Promise<Course> {
    const course = await this.courseRepository.getById(new Id(command.id));

    const updatedCourse = await course
      .changeContent(
        new Content(new Name(command.name), new Icon(command.icon)),
      )
      .changePrice(new Price(command.price))
      .setChips(command.chips)
      .applyTagById(command.tag, this.tagPgRepository);

    await this.courseRepository.update(updatedCourse);

    return updatedCourse;
  }
}
