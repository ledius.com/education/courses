import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { Chip } from '@/course/handler/common/chip';
import { IsOptional, IsUUID, ValidateNested } from 'class-validator';

export class EditCourseCommand implements ICommand {
  @ApiProperty()
  @IsUUID('4')
  public id!: string;

  @ApiProperty({
    name: 'name',
    type: 'string',
  })
  public name!: string;

  @ApiProperty({
    name: 'price',
    type: 'integer',
    minimum: 0,
  })
  public price!: number;

  @ApiProperty({
    name: 'icon',
    type: 'string',
  })
  public icon!: string;

  @ApiProperty({
    type: Chip,
  })
  @ValidateNested({
    each: true,
  })
  public chips: Chip[] = [];

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID('4')
  public tag?: string;
}
