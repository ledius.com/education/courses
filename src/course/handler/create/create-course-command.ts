/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ApiProperty } from '@nestjs/swagger';
import { Chip } from '@/course/handler/common/chip';
import { IsOptional, IsUUID, ValidateNested } from 'class-validator';

export class CreateCourseCommand {
  @ApiProperty({
    name: 'name',
    type: 'string',
  })
  public name!: string;

  @ApiProperty({
    name: 'price',
    type: 'integer',
    minimum: 0,
  })
  public price!: number;

  @ApiProperty({
    name: 'icon',
    type: 'string',
  })
  public icon!: string;

  @ApiProperty({
    type: Chip,
  })
  @ValidateNested({
    each: true,
  })
  public chips: Chip[] = [];

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID('4')
  public tag?: string;
}
