/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CreateCourseHandler } from './create-course-handler';
import { Course } from '../../dao/entity/course';
import { CreateCourseCommand } from './create-course-command';
import { CourseRepositoryInterface } from '../../repository/course-repository.interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';

describe('CreateCourseHandler', () => {
  let handler: CreateCourseHandler;
  let repository: MockProxy<CourseRepositoryInterface> &
    CourseRepositoryInterface;
  let tagRepository: MockProxy<TagPgRepository> & TagPgRepository;

  beforeEach(() => {
    repository = mock<CourseRepositoryInterface>();
    tagRepository = mock<TagPgRepository>();
    handler = new CreateCourseHandler(repository, tagRepository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  describe('create success', () => {
    let command: CreateCourseCommand;

    beforeEach(() => {
      command = new CreateCourseCommand();
      command.name = 'Invest course';
      command.icon = 'test.png';
      command.price = 100;
    });

    it('should be create a course', async () => {
      const course: Course = await handler.execute(command);
      expect(course.getId().getValue()).toBeDefined();
      expect(course.getContent().getName().getValue()).toBe(command.name);
      expect(course.getContent().getIcon().getValue()).toBe(command.icon);
      expect(course.getPrice().getValue()).toBe(command.price);
    });

    it('should be save into repository', async () => {
      const course: Course = await handler.execute(command);

      expect(repository.add).toBeCalledWith(course);
    });
  });
});
