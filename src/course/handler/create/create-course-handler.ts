/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CreateCourseCommand } from './create-course-command';
import { Course } from '../../dao/entity/course';
import { Id } from '../../dao/entity/id/id';
import { Content } from '../../dao/entity/content/content';
import { Name } from '../../dao/entity/content/name/name';
import { Icon } from '../../dao/entity/content/icon/icon';
import { Price } from '../../dao/entity/price/price';
import { CourseRepositoryInterface } from '../../repository/course-repository.interface';
import { Injectable } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';

@Injectable()
@CommandHandler(CreateCourseCommand)
export class CreateCourseHandler
  implements ICommandHandler<CreateCourseCommand, Course>
{
  public constructor(
    private readonly repository: CourseRepositoryInterface,
    private readonly tagRepository: TagPgRepository,
  ) {}

  public async execute(command: CreateCourseCommand): Promise<Course> {
    const content = new Content(new Name(command.name), new Icon(command.icon));
    let course = Course.create(
      Id.next(),
      content,
      new Price(command.price),
      command.chips,
    );

    if (command.tag) {
      const tag = await this.tagRepository.getById(command.tag);
      course = course.setTag(tag);
    }

    await this.repository.add(course);
    return course;
  }
}
