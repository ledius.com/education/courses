/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ListCourseHandler } from './list-course-handler';
import { Course } from '../../dao/entity/course';
import { CourseRepositoryInterface } from '../../repository/course-repository.interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { CourseBuilder } from '../../builder/course-builder';

describe('ListCourseHandler', () => {
  let handler: ListCourseHandler;
  let repository: MockProxy<CourseRepositoryInterface> &
    CourseRepositoryInterface;

  beforeEach(() => {
    repository = mock<CourseRepositoryInterface>();
    handler = new ListCourseHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be returns list of courses from repository', async () => {
    const expectedCourses = [
      new CourseBuilder().build(),
      new CourseBuilder().build(),
      new CourseBuilder().build(),
    ];
    repository.find.mockReturnValue(Promise.resolve(expectedCourses));
    const courses: Course[] = await handler.execute({});

    expect(courses).toEqual(expectedCourses);
  });
});
