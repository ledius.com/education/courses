import { ChipInterface } from '@/course/dao/entity/chip.interface';
import { ApiProperty } from '@nestjs/swagger';

export class Chip implements ChipInterface {
  @ApiProperty({
    nullable: true,
    type: 'string',
  })
  public readonly icon!: string | null;

  @ApiProperty({})
  public readonly text!: string;
}
