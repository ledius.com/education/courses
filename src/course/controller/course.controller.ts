import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiProperty,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { CourseListResponse } from '@/course/controller/course-list-response';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ListCourseQuery } from '@/course/handler/list/list-course-query';
import { CourseResponse } from '@/course/controller/course-response';
import { AuthGuard } from '@/jwt/guard/auth-guard';
import { CreateCourseCommand } from '@/course/handler/create/create-course-command';
import { GetCourseByIdQuery } from '@/course/handler/get/get-course-by-id-query';
import { RoleGuard } from '@/permission/guard/role.guard';
import { UseRoles } from '@/permission/decorators/use-roles';
import { Role } from '@/permission/role.enum';
import { EditCourseCommand } from '@/course/handler/edit/edit-course.command';

@Controller()
@ApiTags('Courses')
export class CourseController {
  public constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  @Post()
  @ApiCreatedResponse({
    type: CourseResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard, RoleGuard)
  @UseRoles(Role.ADMIN, Role.ROOT)
  public async create(
    @Body() command: CreateCourseCommand,
  ): Promise<CourseResponse> {
    const course = await this.commandBus.execute(command);
    return new CourseResponse(course);
  }

  @Get('/:id')
  @ApiProperty({ type: CourseResponse })
  public async get(@Param('id') id: string): Promise<CourseResponse> {
    const course = await this.queryBus.execute(GetCourseByIdQuery.of(id));
    return new CourseResponse(course);
  }

  @Get('')
  @ApiOkResponse({
    type: CourseListResponse,
  })
  @ApiQuery({
    name: 'tag',
    type: 'string',
    required: false,
  })
  public async list(
    @Query() query: ListCourseQuery,
  ): Promise<CourseListResponse> {
    const courses = await this.queryBus.execute(query);
    return new CourseListResponse(courses);
  }

  @Put()
  @ApiOkResponse({
    type: CourseResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard, RoleGuard)
  @UseRoles(Role.ROOT, Role.ADMIN)
  public async update(
    @Body() command: EditCourseCommand,
  ): Promise<CourseResponse> {
    return new CourseResponse(await this.commandBus.execute(command));
  }
}
