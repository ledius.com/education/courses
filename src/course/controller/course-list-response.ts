/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Course } from '@/course/dao/entity/course';
import { CourseResponse } from '@/course/controller/course-response';
import { ApiProperty } from '@nestjs/swagger';

export class CourseListResponse {
  @ApiProperty({
    type: CourseResponse,
    isArray: true,
  })
  public readonly items: CourseResponse[];

  public constructor(courses: Course[]) {
    this.items = courses.map((course) => new CourseResponse(course));
  }
}
