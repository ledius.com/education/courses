/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CourseListResponse } from './course-list-response';
import { CourseBuilder } from '@/course/builder/course-builder';

describe('CourseListResponse', () => {
  it('should be defined', () => {
    const courses = [
      new CourseBuilder().build(),
      new CourseBuilder().build(),
      new CourseBuilder().build(),
    ];
    const response = new CourseListResponse(courses);

    expect(response.items).toHaveLength(3);
    expect(response.items[0]?.id).toBe(courses[0]?.getId().getValue());
    expect(response.items[0]?.content.name).toBe(
      courses[0]?.getContent().getName().getValue(),
    );
    expect(response.items[0]?.content.icon).toBe(
      courses[0]?.getContent().getIcon().getValue(),
    );
    expect(response.items[0]?.price).toBe(courses[0]?.getPrice().getValue());
    expect(response.items[0]?.isPublished).toBe(courses[0]?.isPublished());

    expect(response.items[1]?.id).toBe(courses[1]?.getId().getValue());
    expect(response.items[1]?.content.name).toBe(
      courses[1]?.getContent().getName().getValue(),
    );
    expect(response.items[1]?.content.icon).toBe(
      courses[1]?.getContent().getIcon().getValue(),
    );
    expect(response.items[1]?.price).toBe(courses[1]?.getPrice().getValue());
    expect(response.items[1]?.isPublished).toBe(courses[1]?.isPublished());

    expect(response.items[2]?.id).toBe(courses[2]?.getId().getValue());
    expect(response.items[2]?.content.name).toBe(
      courses[2]?.getContent().getName().getValue(),
    );
    expect(response.items[2]?.content.icon).toBe(
      courses[2]?.getContent().getIcon().getValue(),
    );
    expect(response.items[2]?.price).toBe(courses[2]?.getPrice().getValue());
    expect(response.items[2]?.isPublished).toBe(courses[2]?.isPublished());
  });
});
