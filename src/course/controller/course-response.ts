/**
 *  Ledius LLC
 *  Copyright (C) 11 Oct 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ApiProperty } from '@nestjs/swagger';
import { Course } from '@/course/dao/entity/course';
import { Type } from 'class-transformer';
import { Chip } from '@/course/handler/common/chip';
import { TagResponse } from '@/tag/controller/tag.response';

class CourseContentResponse {
  @ApiProperty({
    type: 'string',
  })
  public name!: string;

  @ApiProperty({
    type: 'string',
  })
  public icon!: string;

  public constructor(course: Course) {
    this.name = course.getContent().getName().getValue();
    this.icon = course.getContent().getIcon().getValue();
  }
}

export class Prices {
  @ApiProperty({
    type: 'string',
  })
  @Type(() => String)
  public lds: bigint;

  @ApiProperty()
  public rub: number;

  public constructor(lds: bigint, rub: number) {
    this.lds = lds;
    this.rub = rub;
  }
}

export class CourseResponse {
  @ApiProperty({
    type: 'string',
  })
  public id: string;
  @ApiProperty({
    type: CourseContentResponse,
  })
  public content: CourseContentResponse;
  @ApiProperty({
    type: 'string',
  })
  public price: number;
  @ApiProperty({
    type: 'string',
  })
  public isPublished: boolean;

  @ApiProperty({
    type: Prices,
  })
  public readonly prices: Prices;

  @ApiProperty({
    type: Chip,
    isArray: true,
  })
  public readonly chips: Chip[];

  @ApiProperty({
    type: TagResponse,
    nullable: true,
  })
  public readonly tag: TagResponse | null;

  public constructor(course: Course) {
    this.id = course.getId().getValue();
    this.content = new CourseContentResponse(course);
    this.price = course.getPrice().getValue();
    this.isPublished = course.isPublished();
    this.prices = new Prices(
      course.getPrice().inLds(),
      course.getPrice().getValue(),
    );
    this.chips = course.chips;
    this.tag = course.tag ? new TagResponse(course.tag) : null;
  }
}
