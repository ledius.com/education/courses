/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { CreateCourseHandler } from './handler/create/create-course-handler';
import { PostgresCourseRepository } from './repository/postgres-course-repository';
import { CourseRepositoryInterface } from './repository/course-repository.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from './dao/entity/course';
import { ListCourseHandler } from './handler/list/list-course-handler';
import { GetCourseByIdHandler } from './handler/get/get-course-by-id-handler';
import { CqrsModule } from '@nestjs/cqrs';
import { CourseController } from '@/course/controller/course.controller';
import { TagModule } from '@/tag/tag.module';
import { EditCourseHandler } from '@/course/handler/edit/edit-course.handler';

@Module({
  imports: [TypeOrmModule.forFeature([Course]), CqrsModule, TagModule],
  controllers: [CourseController],
  providers: [
    CreateCourseHandler,
    ListCourseHandler,
    PostgresCourseRepository,
    GetCourseByIdHandler,
    EditCourseHandler,

    {
      provide: CourseRepositoryInterface,
      useExisting: PostgresCourseRepository,
    },
  ],
  exports: [
    CreateCourseHandler,
    ListCourseHandler,
    GetCourseByIdHandler,
    CourseRepositoryInterface,
    EditCourseHandler,
  ],
})
export class CourseModule {}
