/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Id } from './id/id';
import { Price } from './price/price';
import { Content } from './content/content';
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { IdTransformer } from './id/id-transformer';
import { PriceTransformer } from './price/price-transformer';
import { ChipInterface } from '@/course/dao/entity/chip.interface';
import { Tag } from '@/tag/dao/entity/tag';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';

@Entity('courses')
export class Course {
  public static readonly PUBLISHED: boolean = true;
  public static readonly DRAFT: boolean = false;

  @PrimaryColumn({
    type: 'uuid',
    transformer: new IdTransformer(),
  })
  private readonly id: Id;

  @Column(() => Content)
  private readonly content: Content;

  @Column({
    type: 'integer',
    unsigned: true,
    transformer: new PriceTransformer(),
  })
  private readonly price: Price;

  @Column({
    type: 'boolean',
    default: false,
  })
  private readonly published: boolean;

  @Column('jsonb', {
    default: '[]',
  })
  public readonly chips: ChipInterface[];

  @ManyToOne(() => Tag, {
    onDelete: 'SET NULL',
    eager: true,
  })
  public readonly tag?: Tag;

  private constructor(
    id: Id,
    content: Content,
    price: Price,
    published = Course.DRAFT,
    chips: ChipInterface[] = [],
    tag?: Tag,
  ) {
    this.id = id;
    this.content = content;
    this.price = price;
    this.published = published;
    this.chips = chips;
    this.tag = tag;
  }

  public static create(
    id: Id,
    content: Content,
    price: Price,
    chips: ChipInterface[] = [],
    tag?: Tag,
  ): Course {
    return new Course(id, content, price, Course.DRAFT, chips, tag);
  }

  public getId(): Id {
    return this.id;
  }

  public getPrice(): Price {
    return this.price;
  }

  public getContent(): Content {
    return this.content;
  }

  public changePrice(price: Price): Course {
    return new Course(this.id, this.content, price);
  }

  public changeContent(content: Content): Course {
    return new Course(this.id, content, this.price);
  }

  public isPublished(): boolean {
    return this.published;
  }

  public publish(): Course {
    return new Course(this.id, this.content, this.price, Course.PUBLISHED);
  }

  public unpublish(): Course {
    return new Course(this.id, this.content, this.price, Course.DRAFT);
  }

  public setChips(chips: ChipInterface[]): Course {
    return new Course(this.id, this.content, this.price, this.published, chips);
  }

  public setTag(tag: Tag): Course {
    return new Course(
      this.id,
      this.content,
      this.price,
      this.published,
      this.chips,
      tag,
    );
  }

  public async applyTagById(
    id: string | undefined,
    repository: TagPgRepository,
  ): Promise<Course> {
    if (!id) {
      return this;
    }
    const tag = await repository.getById(id);
    return new Course(
      this.id,
      this.content,
      this.price,
      this.published,
      this.chips,
      tag,
    );
  }
}
