export interface ChipInterface {
  readonly text: string;
  readonly icon: string | null;
}
