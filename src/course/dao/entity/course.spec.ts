/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Course } from './course';
import { Id } from './id/id';
import { Name } from './content/name/name';
import { Price } from './price/price';
import { Content } from './content/content';
import { Icon } from './content/icon/icon';

describe('Course', () => {
  it('should be create a course', () => {
    const id = Id.next();
    const content = new Content(new Name('dawdawd'), new Icon('hello'));
    const price = new Price(100);
    const course: Course = Course.create(id, content, price);

    expect(course).toBeInstanceOf(Course);
    expect(course.getId()).toEqual(id);
    expect(course.getContent()).toEqual(content);
    expect(course.getPrice()).toEqual(price);
    expect(course.isPublished()).toBeFalsy();
  });

  describe('immutable', () => {
    let course: Course;

    beforeEach(() => {
      course = Course.create(
        Id.next(),
        new Content(new Name('dawdawd'), new Icon('hello')),
        new Price(100),
      );
    });

    it('should be change price immutable', () => {
      const newCourse: Course = course.changePrice(new Price(200));

      expect(course).not.toEqual(newCourse);
      expect(course.getPrice().getValue()).toBe(100);
      expect(newCourse.getPrice().getValue()).toBe(200);
    });

    it('should be change content', () => {
      const newContent = new Content(new Name('new'), new Icon('Hello'));
      const newCourse: Course = course.changeContent(newContent);

      expect(course).not.toEqual(newCourse);
      expect(course.getContent()).not.toEqual(newContent);
      expect(newCourse.getContent()).toEqual(newContent);
    });

    it('should be publish', () => {
      const newCourse: Course = course.publish();

      expect(course.isPublished()).toBeFalsy();
      expect(newCourse.isPublished()).toBeTruthy();
    });

    it('should be unpublish', () => {
      const publishedCourse: Course = course.publish();

      expect(course.isPublished()).toBeFalsy();
      expect(publishedCourse.isPublished()).toBeTruthy();

      const unpublishedCourse: Course = publishedCourse.unpublish();

      expect(publishedCourse.isPublished()).toBeTruthy();
      expect(unpublishedCourse.isPublished()).toBeFalsy();
    });
  });
});
