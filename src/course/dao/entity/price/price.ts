/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { InvalidArgumentException } from 'node-exceptions';
import * as Joi from 'joi';

export class Price {
  /**
   * @description in rub value without scaled
   * @private
   */
  private readonly value: number;

  public constructor(value: number) {
    Joi.assert(
      value,
      Joi.number().min(0),
      new InvalidArgumentException('Price should be positive'),
    );
    Joi.assert(
      value,
      Joi.number().integer(),
      new InvalidArgumentException('Price should be integer'),
    );
    this.value = value;
  }

  public getValue(): number {
    return this.value;
  }

  public inKopecks(): number {
    return this.value * 100;
  }

  public inLds(rate = 10): bigint {
    return BigInt(Math.ceil(this.value / rate)) * BigInt(10 ** 18);
  }
}
