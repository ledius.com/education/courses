/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Price } from './price';
import { InvalidArgumentException } from 'node-exceptions';

describe('Name', () => {
  it('should be not construct if price is negative', () => {
    try {
      new Price(-1);
      fail('negative price accepted');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof InvalidArgumentException) {
        expect(e).toBeInstanceOf(InvalidArgumentException);
        expect(e.message).toBe('Price should be positive');
      }
    }
  });

  it('should be not construct if price is float', () => {
    try {
      new Price(1.1);
      fail('float price accepted');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof InvalidArgumentException) {
        expect(e).toBeInstanceOf(InvalidArgumentException);
        expect(e.message).toBe('Price should be integer');
      }
    }
  });

  it('should be create with zero', () => {
    const name = new Price(0);
    expect(name.getValue()).toBe(0);
  });

  it('should be create with positive', () => {
    const name = new Price(1);
    expect(name.getValue()).toBe(1);
  });
});
