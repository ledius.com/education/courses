/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Name } from './name/name';
import { Icon } from './icon/icon';
import { Column } from 'typeorm';
import { NameTransformer } from './name/name-transformer';
import { IconTransformer } from './icon/icon-transformer';

export class Content {
  @Column({
    type: 'varchar',
    nullable: false,
    transformer: new NameTransformer(),
  })
  private readonly name: Name;

  @Column({
    type: 'varchar',
    nullable: false,
    transformer: new IconTransformer(),
  })
  private readonly icon: Icon;

  public constructor(name: Name, icon: Icon) {
    this.name = name;
    this.icon = icon;
  }

  public getIcon(): Icon {
    return this.icon;
  }

  public getName(): Name {
    return this.name;
  }
}
