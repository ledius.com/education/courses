import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTagColumn1645702670699 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "public"."courses" ADD "tag_id" uuid`);
    await queryRunner.query(
      `ALTER TABLE "public"."courses" ADD CONSTRAINT "FK_68f45bdd57295ce0c7ebef2d76b" FOREIGN KEY ("tag_id") REFERENCES "tags"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."courses" DROP CONSTRAINT "FK_68f45bdd57295ce0c7ebef2d76b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "public"."courses" DROP COLUMN "tag_id"`,
    );
  }
}
