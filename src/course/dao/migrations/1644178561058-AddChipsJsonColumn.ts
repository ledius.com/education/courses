import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddChipsJsonColumn1644178561058 implements MigrationInterface {
  public readonly name = 'AddChipsJsonColumn1644178561058';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."courses" ADD "chips" jsonb NOT NULL DEFAULT '[]'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."courses" DROP COLUMN "chips"`,
    );
  }
}
