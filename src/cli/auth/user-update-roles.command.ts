import { Injectable } from '@nestjs/common';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { CheckboxQuestion, ListQuestion, prompt } from 'inquirer';
import { Role } from '@/permission/role.enum';
import { Id } from '@/auth/entity/id/id';
import { Command } from 'nestjs-command';

@Injectable()
export class UserUpdateRolesCommand {
  public constructor(
    private readonly userRepository: UserRepositoryInterface,
  ) {}

  @Command({
    command: 'auth:update-roles',
    describe: 'user update roles',
    aliases: ['auth:ur'],
  })
  public async execute(): Promise<void> {
    const users = await this.userRepository.findAll();
    const questions = [];

    if (!users.length) {
      throw new Error('Users not found');
    }

    questions.push({
      type: 'list',
      name: 'userId',
      message: 'Choose user',
      choices: users.map((user) => ({
        value: user.getId().getValue(),
        name: user.getEmail().getValue(),
      })),
    } as ListQuestion);

    questions.push({
      type: 'checkbox',
      name: 'roles',
      message: 'Choose roles',
      choices: [
        { name: 'Root', value: Role.ROOT },
        { name: 'Admin', value: Role.ADMIN },
        { name: 'Customer', value: Role.CUSTOMER },
      ],
      default: [],
    } as CheckboxQuestion);

    const answers = await prompt<{ userId: string; roles: Role[] }>(questions);

    const user = await this.userRepository.getById(new Id(answers.userId));
    const updatedUser = user.setRoles(...answers.roles);
    await this.userRepository.update(updatedUser);

    console.log(`User has roles`, updatedUser.getRoles());
  }
}
