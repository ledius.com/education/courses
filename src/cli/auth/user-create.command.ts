/**
 *  Ledius LLC
 *  Copyright (C) 3 Oct 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { Command, Option } from 'nestjs-command';
import { Role } from '@/permission/role.enum';
import { prompt, CheckboxQuestion, InputQuestion } from 'inquirer';
import { JoinHandler } from '@/auth/handler/join/join-handler';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';

@Injectable()
export class UserCreateCommand {
  public constructor(
    private readonly joinHandler: JoinHandler,
    private readonly userRepository: UserRepositoryInterface,
    @Inject(Logger) private readonly logger: LoggerService,
  ) {}

  @Command({
    command: 'auth:user-create',
    describe: 'auth user create',
    aliases: ['auth:uc'],
  })
  public async exec(
    @Option({
      name: 'phone',
      describe: 'phone',
      string: true,
      default: null,
    })
    phone?: string,
    @Option({
      name: 'email',
      describe: 'email',
      string: true,
      default: null,
    })
    email?: string,
    @Option({
      name: 'roles',
      describe: 'roles',
      array: true,
      default: [],
      choices: Object.values(Role),
    })
    roles?: string[],
  ): Promise<void> {
    const questions = [];
    if (!roles?.length) {
      questions.push({
        name: 'roles',
        type: 'checkbox',
        choices: Object.values(Role),
      } as CheckboxQuestion);
    }
    if (!email) {
      questions.push({
        name: 'email',
        message: 'Please specify email:',
        type: 'input',
      } as InputQuestion);
    }
    if (!phone) {
      questions.push({
        name: 'phone',
        message: 'please specify phone (like 79609889797)',
        type: 'input',
      } as InputQuestion);
    }

    const data: Record<string, string | string[] | undefined> = {
      email,
      phone,
      roles,
    };

    if (questions.length) {
      const answers = await prompt<{
        email: string;
        phone: string;
        roles: string[];
      }>(questions);
      Object.entries(answers).forEach(
        ([key, value]: [string, string | string[]]) => {
          data[String(key)] = value;
        },
      );
    }

    const user = await this.joinHandler.handle({
      email: data.email as string,
      phone: data.phone as string,
    });

    const newUser = (data.roles as string[]).reduce(
      (accum, role) => accum.addRole(role as Role),
      user,
    );

    await this.userRepository.update(newUser);

    this.logger.log(
      `User created: ${newUser.getEmail().getValue()} (${newUser
        .getPhone()
        ?.getValue()}) ${JSON.stringify(newUser.getRoles())}`,
    );
    this.logger.log(`UID: ${newUser.getId().getValue()}`);
  }
}
