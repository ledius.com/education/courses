/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { OAuthAccessTokenModel } from './oauth-access-token-model';
import { OAuthAccessTokenType } from './oauth-access-token-type';
import { LogicalException } from 'node-exceptions';
import * as moment from 'moment';

export class OAuthAccessTokenBuilder {
  private refreshToken?: { (): string };
  private expiresInDate?: { (): Date };

  public constructor(private readonly accessToken: { (): string }) {}

  public refresh(param: { (): string }): OAuthAccessTokenBuilder {
    this.refreshToken = param;
    return this;
  }

  public expiresIn(param: { (): Date }): OAuthAccessTokenBuilder {
    this.expiresInDate = param;
    return this;
  }

  public create(type: () => OAuthAccessTokenType): OAuthAccessTokenModel {
    if (!this.refreshToken) {
      throw new LogicalException('Refresh token not passed');
    }
    if (!this.expiresInDate) {
      throw new LogicalException('Expires in not passed');
    }
    return new OAuthAccessTokenModel(
      this.accessToken(),
      this.refreshToken(),
      this.expiresInDate(),
      type(),
    );
  }
}

export const bearer = (): 'bearer' => 'bearer';
export const hour = (now: Date = new Date()): Date =>
  moment(now).add(1, 'h').toDate();
export const dummy = (type: 'access' | 'refresh' = 'access'): string =>
  type === 'access' ? dummyAccess() : dummyRefresh();
export const dummyAccess = (): string => 'access.access.access';
export const dummyRefresh = (): string => 'refresh.refresh.refresh';
