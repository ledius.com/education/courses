/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
  dummy,
  dummyAccess,
  dummyRefresh,
  OAuthAccessTokenBuilder,
} from './oauth-access-token-builder';
import * as moment from 'moment';
import { OAuthAccessTokenModel } from './oauth-access-token-model';
import { LogicalException } from 'node-exceptions';

describe('OauthAccessTokenBuilder', () => {
  it('should be build access token model', () => {
    const accessToken = 'access.token.value';
    const expiresIn = moment().add(1, 'h').toDate();

    const accessTokenModel: OAuthAccessTokenModel = new OAuthAccessTokenBuilder(
      () => accessToken,
    )
      .expiresIn(() => expiresIn)
      .refresh(() => 'refresh.token.value')
      .create(() => 'unexpected' as never);

    expect(accessTokenModel).toBeDefined();
    expect(accessTokenModel).toBeInstanceOf(OAuthAccessTokenModel);
    expect(accessTokenModel.getAccess()).toBe(accessToken);
    expect(accessTokenModel.getRefresh()).toBe('refresh.token.value');
    expect(accessTokenModel.getExpiresIn()).toEqual(expiresIn);
    expect(accessTokenModel.getType()).toBe('unexpected');
  });

  it('should be throw error if refresh token not passed', () => {
    const accessToken = 'access.token.value';
    const expiresIn = moment().add(1, 'h').toDate();

    try {
      new OAuthAccessTokenBuilder(() => accessToken)
        .expiresIn(() => expiresIn)
        .create(() => 'unexpected' as never);
      fail('build token without refresh token');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof LogicalException) {
        expect(e).toBeInstanceOf(LogicalException);
        expect(e.message).toBe('Refresh token not passed');
      }
    }
  });

  it('should be throw error if expires not passed', () => {
    const accessToken = 'access.token.value';

    try {
      new OAuthAccessTokenBuilder(() => accessToken)
        .refresh(() => 'refresh.token.value')
        .create(() => 'bearer');
      fail('build token without expires in');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof LogicalException) {
        expect(e).toBeInstanceOf(LogicalException);
        expect(e.message).toBe('Expires in not passed');
      }
    }
  });

  describe('factories', () => {
    it('dummy', () => {
      expect(dummy()).toBe(dummyAccess());
      expect(dummy('access')).toBe(dummyAccess());
      expect(dummy('refresh')).toBe(dummyRefresh());
    });

    it('dummyRefresh', () => {
      const refresh = dummyRefresh();
      expect(refresh).toBe('refresh.refresh.refresh');
    });
  });
});
