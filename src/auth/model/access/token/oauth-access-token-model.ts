/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { OAuthAccessTokenBuilder } from './oauth-access-token-builder';
import { OAuthAccessTokenType } from './oauth-access-token-type';

export class OAuthAccessTokenModel {
  public constructor(
    private readonly access: string,
    private readonly refresh: string,
    private readonly expiresIn: Date,
    private readonly type: OAuthAccessTokenType,
  ) {}

  public static access(accessToken: () => string): OAuthAccessTokenBuilder {
    return new OAuthAccessTokenBuilder(accessToken);
  }

  public getAccess(): string {
    return this.access;
  }

  public getRefresh(): string {
    return this.refresh;
  }

  public getExpiresIn(): Date {
    return this.expiresIn;
  }

  public getType(): OAuthAccessTokenType {
    return this.type;
  }

  public toString(): string {
    return `${this.type} ${this.access}`;
  }
}
