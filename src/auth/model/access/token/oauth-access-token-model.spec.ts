/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { OAuthAccessTokenModel } from './oauth-access-token-model';
import * as moment from 'moment';

describe('OauthAccessTokenModel', () => {
  it('should be create', () => {
    const access = 'dwauawd.diawwwwwwwwd.dawd';
    const refresh = 'dawdadw.dawdawd.ddwaadaw';
    const expiresIn = moment().add(1, 'h').toDate();
    const type = 'bearer';

    const accessToken: OAuthAccessTokenModel = new OAuthAccessTokenModel(
      access,
      refresh,
      expiresIn,
      type,
    );

    expect(accessToken.getAccess()).toBe(access);
    expect(accessToken.getRefresh()).toBe(refresh);
    expect(accessToken.getExpiresIn()).toEqual(expiresIn);
    expect(accessToken.getType()).toBe(type);
  });

  it('should be create with builder', () => {
    const access = 'dwauawd.diawwwwwwwwd.dawd';
    const refresh = 'dawdadw.dawdawd.ddwaadaw';
    const expiresIn = moment().add(1, 'h').toDate();
    const type = 'bearer';

    const accessToken: OAuthAccessTokenModel = OAuthAccessTokenModel.access(
      () => access,
    )
      .refresh(() => refresh)
      .expiresIn(() => expiresIn)
      .create(() => type);

    expect(accessToken.getAccess()).toBe(access);
    expect(accessToken.getRefresh()).toBe(refresh);
    expect(accessToken.getExpiresIn()).toEqual(expiresIn);
    expect(accessToken.getType()).toBe(type);
  });
});
