/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { User } from '../entity/user';
import { Id } from '../entity/id/id';
import { Email } from '../entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { UserId } from '@/purchase/course/entity/user/id/user-id';

export class UserBuilder {
  private email: Email = new Email('test@gmail.com');
  private phone: Phone = new Phone('79609889797');
  private id: Id = new Id('9f1be5e6-4b64-4330-9106-c0ae4231950f');

  public withEmail(email: Email): this {
    this.email = email;
    return this;
  }

  public withPhone(phone: Phone): this {
    this.phone = phone;
    return this;
  }

  public withId(id: Id | UserId): this {
    this.id = id instanceof UserId ? new Id(id.getValue()) : id;
    return this;
  }

  public build(): User {
    return User.join(this.id, this.email, this.phone);
  }
}
