/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { UserRepositoryInterface } from './user-repository-interface';
import { Email } from '../entity/email/email';
import { User } from '../entity/user';
import { Phone } from '@/common/dao/phone/phone';
import { Id } from '../entity/id/id';
import { FindConditions, In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DomainException } from 'node-exceptions';

export class PostgresUserRepository implements UserRepositoryInterface {
  public constructor(
    @InjectRepository(User) private readonly repository: Repository<User>,
  ) {}

  public async add(user: User): Promise<User> {
    await this.repository.save(user);
    return user;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async getByEmail(email: Email): Promise<User> {
    const user = await this.repository.findOne({
      where: {
        email: email.getValue(),
      },
    });
    if (!user) {
      throw new DomainException('User not found');
    }
    return user;
  }

  public async getById(id: Id): Promise<User> {
    return this.repository.findOneOrFail(id.getValue());
  }

  public async getByPhone(phone: Phone): Promise<User> {
    return await this.repository.findOneOrFail({
      where: { phone },
    });
  }

  public async hasByEmail(email: Email): Promise<boolean> {
    const count = await this.repository.count({
      where: {
        email: email.getValue(),
      },
    });
    return count >= 1;
  }

  public async hasByPhone(phone: Phone): Promise<boolean> {
    const count = await this.repository.count({
      where: {
        phone: phone.getValue(),
      },
    });
    return count >= 1;
  }

  public async update(user: User): Promise<User> {
    await this.repository.save(user);
    return user;
  }

  public async findByIds(ids: string[] = []): Promise<User[]> {
    let where = {} as FindConditions<User>;

    if (ids && ids.length > 0) {
      where = { ...where, id: In(ids) };
    }

    return this.repository.find({
      where,
    });
  }

  public async findAll(): Promise<User[]> {
    return await this.repository.find();
  }
}
