/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Email } from '../entity/email/email';
import { User } from '../entity/user';
import { Id } from '../entity/id/id';
import { Phone } from '@/common/dao/phone/phone';

export abstract class UserRepositoryInterface {
  public abstract hasByEmail(email: Email): Promise<boolean>;
  public abstract hasByPhone(phone: Phone): Promise<boolean>;
  public abstract getById(id: Id): Promise<User>;
  public abstract getByEmail(email: Email): Promise<User>;
  public abstract getByPhone(phone: Phone): Promise<User>;
  public abstract findByIds(ids: string[]): Promise<User[]>;
  public abstract add(user: User): Promise<User>;
  public abstract update(user: User): Promise<User>;
  public abstract findAll(): Promise<User[]>;
}
