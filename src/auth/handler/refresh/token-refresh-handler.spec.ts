/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { TokenRefreshHandler } from './token-refresh-handler';
import { TokenRefreshCommand } from './token-refresh-command';
import { OAuthAccessTokenModel } from '../../model/access/token/oauth-access-token-model';
import { mock, MockProxy } from 'jest-mock-extended';
import {
  bearer,
  dummyAccess,
  dummyRefresh,
  hour,
} from '../../model/access/token/oauth-access-token-builder';
import { AccessTokenServiceInterface } from '../../service/access/token/access-token-service-interface';

describe('TokenRefreshHandler', () => {
  let handler: TokenRefreshHandler;
  let accessTokenService: MockProxy<AccessTokenServiceInterface> &
    AccessTokenServiceInterface;

  beforeEach(() => {
    accessTokenService = mock<AccessTokenServiceInterface>();
    handler = new TokenRefreshHandler(accessTokenService);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should refresh token', async () => {
    const command = new TokenRefreshCommand();
    command.refresh = 'hello.world';

    const expectedToken = OAuthAccessTokenModel.access(dummyAccess)
      .expiresIn(hour)
      .refresh(dummyRefresh)
      .create(bearer);

    accessTokenService.refresh.mockReturnValue(Promise.resolve(expectedToken));
    const token: OAuthAccessTokenModel = await handler.handle(command);
    expect(token).toEqual(expectedToken);
    expect(accessTokenService.refresh).toBeCalledWith(command.refresh);
  });
});
