/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { PhoneConfirmHandler } from './phone-confirm-handler';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { Token } from '@/auth/entity/token';
import * as moment from 'moment';
import { PhoneConfirmCommand } from './phone-confirm-command';
import { mock, MockProxy } from 'jest-mock-extended';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';

describe('PhoneConfirmHandler', () => {
  let handler: PhoneConfirmHandler;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;

  beforeEach(() => {
    repository = mock<UserRepositoryInterface>();
    handler = new PhoneConfirmHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be confirm phone', async () => {
    const expectedUser = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const now = moment();
    const token = Token.create('2000', now.clone().add(1, 'h').toDate());
    expectedUser.requestPhoneConfirmation(token, now.toDate());

    const command = new PhoneConfirmCommand();
    command.id = expectedUser.getId().getValue();
    command.code = token.getValue();

    repository.getById.mockReturnValue(Promise.resolve(expectedUser));

    repository.update.mockImplementationOnce(
      async (user: User): Promise<User> => {
        expect(expectedUser.getId().getValue()).toBe(user.getId().getValue());
        expect(user.isPhoneConfirmed()).toBeTruthy();
        return user;
      },
    );

    await handler.handle(command);
    expect(repository.update).toBeCalled();
  });
});
