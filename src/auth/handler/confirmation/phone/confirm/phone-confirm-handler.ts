/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { PhoneConfirmCommand } from './phone-confirm-command';
import { UserRepositoryInterface } from '../../../../repository/user-repository-interface';
import { Id } from '../../../../entity/id/id';

export class PhoneConfirmHandler {
  public constructor(private readonly repository: UserRepositoryInterface) {}

  public async handle(command: PhoneConfirmCommand): Promise<void> {
    const user = await this.repository.getById(new Id(command.id));
    user.phoneConfirm(command.code, new Date());
    await this.repository.update(user);
  }
}
