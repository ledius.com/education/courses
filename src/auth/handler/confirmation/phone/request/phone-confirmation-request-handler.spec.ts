/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { PhoneConfirmationRequestHandler } from './phone-confirmation-request-handler';
import { PhoneConfirmationRequestCommand } from './phone-confirmation-request-command';
import { mock, MockProxy } from 'jest-mock-extended';
import { TokenizerInterface } from '@/auth/service/tokenizer/tokenizer-interface';
import { Token } from '@/auth/entity/token';
import { Moment } from 'moment';
import * as moment from 'moment';
import { Email } from '@/auth/entity/email/email';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Phone } from '@/common/dao/phone/phone';
import { PhoneConfirmationSenderInterface } from '@/auth/service/phone-confirmation-sender-interface';

describe('PhoneConfirmationRequestHandler', () => {
  let handler: PhoneConfirmationRequestHandler;
  let sender: MockProxy<PhoneConfirmationSenderInterface> &
    PhoneConfirmationSenderInterface;
  let tokenizer: MockProxy<TokenizerInterface> & TokenizerInterface;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;

  beforeEach(() => {
    sender = mock<PhoneConfirmationSenderInterface>();
    repository = mock<UserRepositoryInterface>();
    tokenizer = mock<TokenizerInterface>();
    handler = new PhoneConfirmationRequestHandler(
      repository,
      sender,
      tokenizer,
    );
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  describe('confirmation', () => {
    let now: Moment;
    let expectedToken: Token;
    let command: PhoneConfirmationRequestCommand;
    let expectedUser: User;

    beforeEach(() => {
      now = moment();
      expectedToken = Token.create('1234', now.clone().add('1', 'h').toDate());
      tokenizer.generate.mockReturnValue(expectedToken);
      command = new PhoneConfirmationRequestCommand();
      command.phone = '79609889797';
      expectedUser = User.join(
        Id.next(),
        new Email('test@gmail.com'),
        new Phone('79609889797'),
      );
      repository.getByPhone.mockReturnValue(Promise.resolve(expectedUser));
    });

    it('should be send sms', async () => {
      await handler.handle(command);
      expect(sender.send).toBeCalledWith(
        new Phone(command.phone),
        expectedToken,
      );
    });

    it('should be confirm user phone with sender token', async () => {
      await handler.handle(command);
      expect(expectedUser.isPhoneConfirmed()).toBeFalsy();

      expectedUser.phoneConfirm(expectedToken.getValue(), now.toDate());

      expect(expectedUser.isPhoneConfirmed()).toBeTruthy();
      expect(repository.getByPhone).toBeCalledWith(new Phone(command.phone));
    });

    it('should be update user in repository with actual confirmation request', async () => {
      repository.update.mockImplementationOnce(
        async (user: User): Promise<User> => {
          expect(expectedUser.isPhoneConfirmed()).toBeFalsy();
          expectedUser.phoneConfirm(expectedToken.getValue(), now.toDate());
          expect(expectedUser.isPhoneConfirmed()).toBeTruthy();
          return user;
        },
      );
      await handler.handle(command);

      expect(repository.update).toBeCalled();
    });
  });
});
