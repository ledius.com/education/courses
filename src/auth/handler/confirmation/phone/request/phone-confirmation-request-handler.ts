/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { PhoneConfirmationRequestCommand } from './phone-confirmation-request-command';
import { TokenizerInterface } from '@/auth/service/tokenizer/tokenizer-interface';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { PhoneConfirmationSenderInterface } from '@/auth/service/phone-confirmation-sender-interface';
import { Phone } from '@/common/dao/phone/phone';

export class PhoneConfirmationRequestHandler {
  public constructor(
    private readonly repository: UserRepositoryInterface,
    private readonly sender: PhoneConfirmationSenderInterface,
    private readonly tokenizer: TokenizerInterface,
  ) {}

  public async handle(command: PhoneConfirmationRequestCommand): Promise<void> {
    const user = await this.repository.getByPhone(new Phone(command.phone));
    const phone = user.getPhone() as Phone;
    const token = this.tokenizer.generate();

    user.requestPhoneConfirmation(token, new Date());

    await this.repository.update(user);
    await this.sender.send(phone, token);
  }
}
