/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { EmailConfirmationRequestHandler } from './email-confirmation-request-handler';
import { EmailConfirmationRequestCommand } from './email-confirmation-request-command';
import { mock, MockProxy } from 'jest-mock-extended';
import { EmailConfirmationSenderInterface } from '@/auth/service/email-confirmation-sender-interface';
import { TokenizerInterface } from '@/auth/service/tokenizer/tokenizer-interface';
import { Token } from '@/auth/entity/token';
import { Moment } from 'moment';
import * as moment from 'moment';
import { Email } from '@/auth/entity/email/email';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Phone } from '@/common/dao/phone/phone';

describe('EmailConfirmationRequestHandler', () => {
  let handler: EmailConfirmationRequestHandler;
  let emailConfirmationSender: MockProxy<EmailConfirmationSenderInterface> &
    EmailConfirmationSenderInterface;
  let tokenizer: MockProxy<TokenizerInterface> & TokenizerInterface;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;

  beforeEach(() => {
    emailConfirmationSender = mock<EmailConfirmationSenderInterface>();
    repository = mock<UserRepositoryInterface>();
    tokenizer = mock<TokenizerInterface>();
    handler = new EmailConfirmationRequestHandler(
      repository,
      emailConfirmationSender,
      tokenizer,
    );
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  describe('confirmation', () => {
    let now: Moment;
    let expectedToken: Token;
    let command: EmailConfirmationRequestCommand;
    let expectedUser: User;

    beforeEach(() => {
      now = moment();
      expectedToken = Token.create('1234', now.clone().add('1', 'h').toDate());
      tokenizer.generate.mockReturnValue(expectedToken);
      command = new EmailConfirmationRequestCommand();
      command.email = 'test@gmail.com';
      expectedUser = User.join(
        Id.next(),
        new Email('test@gmail.com'),
        new Phone('79609889797'),
      );
      repository.getByEmail.mockReturnValue(Promise.resolve(expectedUser));
    });

    it('should be send email', async () => {
      await handler.handle(command);
      expect(emailConfirmationSender.send).toBeCalledWith(
        new Email(command.email),
        expectedToken,
      );
    });

    it('should be confirm user email with sender token', async () => {
      await handler.handle(command);
      expect(expectedUser.isEmailConfirmed()).toBeFalsy();

      expectedUser.emailConfirm(expectedToken.getValue(), now.toDate());

      expect(expectedUser.isEmailConfirmed()).toBeTruthy();
      expect(repository.getByEmail).toBeCalledWith(new Email(command.email));
    });

    it('should be update user in repository with actual confirmation request', async () => {
      repository.update.mockImplementationOnce(
        async (user: User): Promise<User> => {
          expect(expectedUser.isEmailConfirmed()).toBeFalsy();
          expectedUser.emailConfirm(expectedToken.getValue(), now.toDate());
          expect(expectedUser.isEmailConfirmed()).toBeTruthy();
          return user;
        },
      );
      await handler.handle(command);

      expect(repository.update).toBeCalled();
    });
  });
});
