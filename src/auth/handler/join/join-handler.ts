/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { JoinCommand } from './join-command';
import { User } from '../../entity/user';
import { Id } from '../../entity/id/id';
import { Email } from '../../entity/email/email';
import { UserRepositoryInterface } from '../../repository/user-repository-interface';
import { DomainException } from 'node-exceptions';
import { Injectable } from '@nestjs/common';
import { Phone } from '@/common/dao/phone/phone';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserJoinEvent } from '@/auth/handler/event/user-join.event';

@Injectable()
export class JoinHandler {
  public constructor(
    private readonly repository: UserRepositoryInterface,
    private readonly emitter: EventEmitter2,
  ) {}

  public async handle(command: JoinCommand): Promise<User> {
    const email = new Email(command.email);
    const phone = command.phone ? new Phone(command.phone) : undefined;
    if (await this.repository.hasByEmail(email)) {
      throw new DomainException('Email already used');
    }
    if (phone && (await this.repository.hasByPhone(phone))) {
      throw new DomainException('Phone already used');
    }
    const user = User.join(Id.next(), email, phone);
    await this.repository.add(user);
    this.emitter.emit(UserJoinEvent.name, new UserJoinEvent(user));
    return user;
  }
}
