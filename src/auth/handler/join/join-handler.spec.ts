/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { JoinHandler } from './join-handler';
import { User } from '../../entity/user';
import { JoinCommand } from './join-command';
import { UserRepositoryInterface } from '../../repository/user-repository-interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { DomainException } from 'node-exceptions';
import { Email } from '../../entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserJoinEvent } from '@/auth/handler/event/user-join.event';

describe('JoinHandler', () => {
  let handler: JoinHandler;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;
  let emitter: MockProxy<EventEmitter2> & EventEmitter2;

  beforeEach(() => {
    repository = mock<UserRepositoryInterface>();
    emitter = mock();
    handler = new JoinHandler(repository, emitter);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be join user', async () => {
    const command = new JoinCommand();
    command.email = 'test@gmail.com';
    command.phone = '79609889393';
    const user: User = await handler.handle(command);
    expect(user.getId()).toBeDefined();
    expect(user.getEmail().getValue()).toEqual(command.email);
    expect(user.getPhone()?.getValue()).toEqual(command.phone);
    expect(emitter.emit).toBeCalledWith(
      UserJoinEvent.name,
      new UserJoinEvent(user),
    );
  });

  it('should be error if user with given email already joined', async () => {
    repository.hasByEmail.mockReturnValue(Promise.resolve(true));
    const command = new JoinCommand();
    command.email = 'test@gmail.com';
    command.phone = '79609889393';
    try {
      await handler.handle(command);
      fail('not error if email already exists');
    } catch (e) {
      expect.assertions(3);
      if (e instanceof DomainException) {
        expect(e).toBeInstanceOf(DomainException);
        expect(e.message).toBe('Email already used');
      }
    } finally {
      expect(repository.hasByEmail).toBeCalledWith(new Email('test@gmail.com'));
    }
  });

  it('should be error if user with given phone already joined', async () => {
    repository.hasByPhone.mockReturnValue(Promise.resolve(true));
    const command = new JoinCommand();
    command.email = 'test@gmail.com';
    command.phone = '79609889393';
    try {
      await handler.handle(command);
      fail('not error if phone already exists');
    } catch (e) {
      expect.assertions(3);
      if (e instanceof DomainException) {
        expect(e).toBeInstanceOf(DomainException);
        expect(e.message).toBe('Phone already used');
      }
    } finally {
      expect(repository.hasByPhone).toBeCalledWith(new Phone('79609889393'));
    }
  });

  it('should be add user', async () => {
    const command = new JoinCommand();
    command.email = 'test@gmail.com';
    command.phone = '79609889393';
    const user: User = await handler.handle(command);
    expect(repository.add).toBeCalled();
    expect(repository.add).toBeCalledWith(user);
  });
});
