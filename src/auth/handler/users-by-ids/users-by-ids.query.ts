import { IsOptional, IsUUID } from 'class-validator';
import { Transform } from 'class-transformer';

export class UsersByIdsQuery {
  @IsOptional()
  @IsUUID('4', {
    each: true,
  })
  @Transform((v) => (v.value || '').split(','))
  public ids!: string[];
}
