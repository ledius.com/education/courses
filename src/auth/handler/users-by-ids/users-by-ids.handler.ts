import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { UsersByIdsQuery } from '@/auth/handler/users-by-ids/users-by-ids.query';
import { User } from '@/auth/entity/user';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';

@QueryHandler(UsersByIdsQuery)
export class UsersByIdsHandler
  implements IQueryHandler<UsersByIdsQuery, User[]>
{
  public constructor(
    private readonly userRepository: UserRepositoryInterface,
  ) {}

  public async execute(query: UsersByIdsQuery): Promise<User[]> {
    return this.userRepository.findByIds(query.ids);
  }
}
