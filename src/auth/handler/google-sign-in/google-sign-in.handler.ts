import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { GoogleSignInCommand } from '@/auth/handler/google-sign-in/google-sign-in.command';
import { OAuthAccessTokenModel } from '@/auth/model/access/token/oauth-access-token-model';
import { Injectable } from '@nestjs/common';
import { OAuth2Client } from 'google-auth-library';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { BaseException } from '@/common/base-exception';
import { ErrorCodeEnum } from '@/common/error-code.enum';
import { Email } from '@/auth/entity/email/email';
import { JoinHandler } from '@/auth/handler/join/join-handler';
import { AccessTokenServiceInterface } from '@/auth/service/access/token/access-token-service-interface';

@Injectable()
@CommandHandler(GoogleSignInCommand)
export class GoogleSignInHandler
  implements ICommandHandler<GoogleSignInCommand, OAuthAccessTokenModel>
{
  public constructor(
    private readonly oAuth2Client: OAuth2Client,
    private readonly userRepository: UserRepositoryInterface,
    private readonly joinHandler: JoinHandler,
    private readonly accessTokenService: AccessTokenServiceInterface,
  ) {}

  public async execute(
    command: GoogleSignInCommand,
  ): Promise<OAuthAccessTokenModel> {
    const verified = await this.oAuth2Client.verifyIdToken(command);
    const { email, picture = '', name = '' } = verified.getPayload() || {};

    if (!email) {
      throw new BaseException({
        message: 'Cannot verify token. Payload not found',
        statusCode: ErrorCodeEnum.AUTH_VERIFIED_INVALID,
      });
    }

    const userExists = await this.userRepository.hasByEmail(new Email(email));

    if (!userExists) {
      await this.joinHandler.handle({
        email,
        phone: '',
      });
    }

    const user = await this.userRepository.getByEmail(new Email(email));

    await this.userRepository.update(user.changePicture(picture).rename(name));

    return this.accessTokenService.releaseFor(user);
  }
}
