import { ICommand } from '@nestjs/cqrs';
import { IsDefined, IsString } from 'class-validator';
import { VerifyIdTokenOptions } from 'google-auth-library';
import { ApiProperty } from '@nestjs/swagger';

export class GoogleSignInCommand implements ICommand, VerifyIdTokenOptions {
  @IsDefined()
  @IsString()
  @ApiProperty()
  public readonly idToken!: string;
}
