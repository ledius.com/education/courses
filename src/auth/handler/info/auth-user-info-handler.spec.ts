/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AuthUserInfoHandler } from './auth-user-info-handler';
import { User } from '../../entity/user';
import { AuthUserInfoQuery } from './auth-user-info-query';
import { Email } from '../../entity/email/email';
import { Id } from '../../entity/id/id';
import { mock } from 'jest-mock-extended';
import { AccessTokenServiceInterface } from '../../service/access/token/access-token-service-interface';
import { Phone } from '@/common/dao/phone/phone';

describe('AuthUserInfoHandler', () => {
  it('should be defined', async () => {
    const expectedUser = User.join(
      Id.next(),
      new Email('test'),
      new Phone('79609889730'),
    );
    const accessTokenService = mock<AccessTokenServiceInterface>();
    accessTokenService.getUser.mockReturnValue(Promise.resolve(expectedUser));
    const handler = new AuthUserInfoHandler(accessTokenService);

    const query = new AuthUserInfoQuery();
    query.accessToken = 'hello.world.test';
    const user: User = await handler.handle(query);

    expect(handler).toBeDefined();
    expect(expectedUser).toEqual(user);
    expect(accessTokenService.getUser).toBeCalledWith('hello.world.test');
  });
});
