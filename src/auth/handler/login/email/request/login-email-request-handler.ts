/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailRequestCommand } from './login-email-request-command';
import { LoginEmailCodeSenderInterface } from '../../../../service/login-email-code-sender-interface';
import { Email } from '../../../../entity/email/email';
import { TokenizerInterface } from '../../../../service/tokenizer/tokenizer-interface';
import { UserRepositoryInterface } from '../../../../repository/user-repository-interface';
import { LoginRequestsInterface } from '../../../../service/login/requests/login-requests-interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class LoginEmailRequestHandler {
  public constructor(
    private readonly codeSender: LoginEmailCodeSenderInterface,
    private readonly tokenizer: TokenizerInterface,
    private readonly repository: UserRepositoryInterface,
    private readonly requests: LoginRequestsInterface,
  ) {}

  public async handle(command: LoginEmailRequestCommand): Promise<void> {
    const email = new Email(command.email);
    const user = await this.repository.getByEmail(email);
    const token = this.tokenizer.generate();
    await this.requests.add(user, token);
    await this.codeSender.send(email, token);
  }
}
