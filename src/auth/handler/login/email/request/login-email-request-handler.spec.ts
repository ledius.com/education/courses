/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailRequestHandler } from './login-email-request-handler';
import { LoginEmailRequestCommand } from './login-email-request-command';
import { LoginEmailCodeSenderInterface } from '@/auth/service/login-email-code-sender-interface';
import { TokenizerInterface } from '@/auth/service/tokenizer/tokenizer-interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { Email } from '@/auth/entity/email/email';
import { Token } from '@/auth/entity/token';
import * as moment from 'moment';
import { DomainException } from 'node-exceptions';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { LoginRequestsInterface } from '@/auth/service/login/requests/login-requests-interface';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Phone } from '@/common/dao/phone/phone';

describe('LoginEmailRequestHandler', () => {
  let handler: LoginEmailRequestHandler;
  let codeSender: MockProxy<LoginEmailCodeSenderInterface> &
    LoginEmailCodeSenderInterface;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;
  let tokenizer: MockProxy<TokenizerInterface> & TokenizerInterface;
  let requests: MockProxy<LoginRequestsInterface> & LoginRequestsInterface;

  beforeEach(() => {
    codeSender = mock<LoginEmailCodeSenderInterface>();
    tokenizer = mock<TokenizerInterface>();
    requests = mock<LoginRequestsInterface>();
    repository = mock<UserRepositoryInterface>();
    handler = new LoginEmailRequestHandler(
      codeSender,
      tokenizer,
      repository,
      requests,
    );
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  describe('send request', () => {
    it('should be send code on email', async () => {
      const command = new LoginEmailRequestCommand();
      command.email = 'test@gmail.com';

      const now = moment();
      const token = Token.create('12345', now.clone().add('1', 'h').toDate());
      tokenizer.generate.mockReturnValue(token);

      const expectedUser = User.join(
        Id.next(),
        new Email(command.email),
        new Phone('79609889797'),
      );

      repository.getByEmail.mockReturnValue(Promise.resolve(expectedUser));

      await handler.handle(command);

      expect(codeSender.send).toBeCalledWith(new Email(command.email), token);
      expect(repository.getByEmail).toBeCalledWith(new Email(command.email));
      expect(requests.add).toBeCalledWith(expectedUser, token);
    });

    it('should be failed if user does not exists', async () => {
      const command = new LoginEmailRequestCommand();
      command.email = 'test@gmail.com';

      repository.getByEmail.mockImplementation(async () => {
        throw new DomainException('User not found');
      });

      try {
        await handler.handle(command);
        fail('Code will be send to unknown user');
      } catch (e) {
        expect.assertions(2);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('User not found');
        }
      }
    });
  });
});
