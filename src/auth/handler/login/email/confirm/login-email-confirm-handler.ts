/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailConfirmCommand } from './login-email-confirm-command';
import { OAuthAccessTokenModel } from '../../../../model/access/token/oauth-access-token-model';
import { AccessTokenServiceInterface } from '../../../../service/access/token/access-token-service-interface';
import { UserRepositoryInterface } from '../../../../repository/user-repository-interface';
import { Email } from '../../../../entity/email/email';
import { DomainException } from 'node-exceptions';
import { LoginRequestsInterface } from '../../../../service/login/requests/login-requests-interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class LoginEmailConfirmHandler {
  public constructor(
    private readonly tokens: AccessTokenServiceInterface,
    private readonly repository: UserRepositoryInterface,
    private readonly requests: LoginRequestsInterface,
  ) {}

  public async handle(
    command: LoginEmailConfirmCommand,
  ): Promise<OAuthAccessTokenModel> {
    const email = new Email(command.email);
    const user = await this.repository.getByEmail(email);
    const hasRequest = await this.requests.has(user, command.code);
    if (!hasRequest) {
      throw new DomainException('Login did not requested');
    }
    await this.requests.revoke(user);
    return this.tokens.releaseFor(user);
  }
}
