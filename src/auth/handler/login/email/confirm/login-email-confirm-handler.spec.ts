/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailConfirmHandler } from './login-email-confirm-handler';
import { LoginEmailConfirmCommand } from './login-email-confirm-command';
import { mock, MockProxy } from 'jest-mock-extended';
import { AccessTokenServiceInterface } from '@/auth/service/access/token/access-token-service-interface';
import { OAuthAccessTokenModel } from '@/auth/model/access/token/oauth-access-token-model';
import {
  bearer,
  dummy,
  hour,
} from '@/auth/model/access/token/oauth-access-token-builder';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { DomainException } from 'node-exceptions';
import { LoginRequestsInterface } from '@/auth/service/login/requests/login-requests-interface';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Phone } from '@/common/dao/phone/phone';
import { Email } from '@/auth/entity/email/email';

describe('LoginEmailConfirmHandler', () => {
  let accessTokenService: MockProxy<AccessTokenServiceInterface> &
    AccessTokenServiceInterface;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;
  let requests: MockProxy<LoginRequestsInterface> & LoginRequestsInterface;
  let handler: LoginEmailConfirmHandler;

  beforeEach(() => {
    accessTokenService = mock<AccessTokenServiceInterface>();
    repository = mock<UserRepositoryInterface>();
    requests = mock<LoginRequestsInterface>();
    handler = new LoginEmailConfirmHandler(
      accessTokenService,
      repository,
      requests,
    );
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  describe('confirm login', () => {
    let expectedAccessToken: OAuthAccessTokenModel;
    let expectedUser: User;
    let command: LoginEmailConfirmCommand;

    beforeEach(() => {
      command = new LoginEmailConfirmCommand();
      command.email = 'test@gmail.com';
      command.code = '12345';
      expectedAccessToken = OAuthAccessTokenModel.access(dummy)
        .expiresIn(hour)
        .refresh(dummy)
        .create(bearer);
      accessTokenService.releaseFor.mockReturnValue(
        Promise.resolve(expectedAccessToken),
      );
      requests.has.mockReturnValue(Promise.resolve(true));
      expectedUser = User.join(
        Id.next(),
        new Email(command.email),
        new Phone('79609889797'),
      );
      repository.getByEmail.mockReturnValue(Promise.resolve(expectedUser));
    });

    it('should be confirmed login if requested', async () => {
      const token: OAuthAccessTokenModel = await handler.handle(command);
      expect(token.getAccess()).toBe(expectedAccessToken.getAccess());
      expect(token.getRefresh()).toBe(expectedAccessToken.getRefresh());
      expect(token.getType()).toBe(expectedAccessToken.getType());
      expect(token.getExpiresIn()).toBe(expectedAccessToken.getExpiresIn());
    });

    it('should be throw exception if login did not requested', async () => {
      requests.has.mockReturnValue(Promise.resolve(false));
      try {
        await handler.handle(command);
        fail('login does not requested');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Login did not requested');
          expect(requests.has).toBeCalledWith(expectedUser, command.code);
        }
      }
    });
  });
});
