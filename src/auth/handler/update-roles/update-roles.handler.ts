import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { UpdateRolesCommand } from '@/auth/handler/update-roles/update-roles.command';
import { User } from '@/auth/entity/user';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { Id } from '@/auth/entity/id/id';
import { Role } from '@/permission/role.enum';

@CommandHandler(UpdateRolesCommand)
export class UpdateRolesHandler
  implements ICommandHandler<UpdateRolesCommand, User>
{
  public constructor(
    private readonly userRepository: UserRepositoryInterface,
  ) {}

  public async execute(command: UpdateRolesCommand): Promise<User> {
    const user = await this.userRepository.getById(new Id(command.userId));

    if (user.isAdmin() && user.getId().getValue() === command.emitterUserId) {
      const foundAdminRole = command.roles.includes(Role.ADMIN);
      if (!foundAdminRole) {
        command.roles.push(Role.ADMIN);
      }
    }

    if (user.isRoot() && user.getId().getValue() === command.emitterUserId) {
      const foundRootRole = command.roles.includes(Role.ROOT);
      if (!foundRootRole) {
        command.roles.push(Role.ROOT);
      }
    }

    const updatedUser = user.setRoles(...command.roles);

    await this.userRepository.update(updatedUser);

    return updatedUser;
  }
}
