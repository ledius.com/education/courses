import { ICommand } from '@nestjs/cqrs';
import { Role } from '@/permission/role.enum';
import { IsDefined, IsEnum, IsOptional, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateRolesCommand implements ICommand {
  @IsDefined()
  @IsEnum(Role, {
    each: true,
  })
  @ApiProperty({
    isArray: true,
    type: 'string',
  })
  public roles!: Role[];

  @IsDefined()
  @IsUUID('4')
  @ApiProperty()
  public userId!: string;

  @IsOptional()
  @IsUUID('4')
  public emitterUserId?: string;
}
