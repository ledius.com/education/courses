/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { JoinHandler } from './handler/join/join-handler';
import { PostgresUserRepository } from './repository/postgres-user-repository';
import { UserRepositoryInterface } from './repository/user-repository-interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entity/user';
import { LoginEmailRequestHandler } from './handler/login/email/request/login-email-request-handler';
import { LoginEmailCodeSenderInterface } from './service/login-email-code-sender-interface';
import { LoginEmailCodeSender } from './service/login-email-code-sender';
import { TokenizerInterface } from './service/tokenizer/tokenizer-interface';
import { Tokenizer } from './service/tokenizer/tokenizer';
import { LoginRequestsInterface } from './service/login/requests/login-requests-interface';
import { MemoryLoginRequests } from './service/login/requests/memory-login-requests';
import * as moment from 'moment';
import { MailerModule } from '@/mailer/mailer-module';
import { LoginEmailConfirmHandler } from './handler/login/email/confirm/login-email-confirm-handler';
import { AccessTokenServiceInterface } from './service/access/token/access-token-service-interface';
import { AccessTokenService } from './service/access/token/access-token-service';
import { AuthUserInfoHandler } from './handler/info/auth-user-info-handler';
import { TokenRefreshHandler } from './handler/refresh/token-refresh-handler';
import { CqrsModule } from '@nestjs/cqrs';
import { GoogleSignInHandler } from '@/auth/handler/google-sign-in/google-sign-in.handler';
import { OAuth2Client } from 'google-auth-library';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthController } from '@/auth/controller/auth.controller';
import { TemplateEngineModule } from '@/template/template-engine.module';
import { JsonWebTokenModule } from '@/jwt/json-web-token.module';
import { UsersByIdsHandler } from '@/auth/handler/users-by-ids/users-by-ids.handler';
import { UpdateRolesHandler } from '@/auth/handler/update-roles/update-roles.handler';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    MailerModule,
    JsonWebTokenModule.forRoot({}),
    CqrsModule,
    ConfigModule,
    TemplateEngineModule,
  ],
  controllers: [AuthController],
  providers: [
    PostgresUserRepository,
    {
      provide: UserRepositoryInterface,
      useExisting: PostgresUserRepository,
    },
    JoinHandler,
    LoginEmailRequestHandler,
    LoginEmailConfirmHandler,
    AuthUserInfoHandler,
    TokenRefreshHandler,
    GoogleSignInHandler,
    UsersByIdsHandler,
    UpdateRolesHandler,
    {
      provide: AccessTokenServiceInterface,
      useClass: AccessTokenService,
    },
    {
      provide: LoginEmailCodeSenderInterface,
      useClass: LoginEmailCodeSender,
    },
    {
      provide: TokenizerInterface,
      useFactory: (): TokenizerInterface =>
        new Tokenizer((now: Date) => moment(now).add(10, 'm').toDate()),
    },
    {
      provide: LoginRequestsInterface,
      useClass: MemoryLoginRequests,
    },
    {
      provide: OAuth2Client,
      useFactory: (config: ConfigService): OAuth2Client => {
        return new OAuth2Client({
          clientId: config.get('GOOGLE_AUTH_CLIENT_ID'),
          clientSecret: config.get('GOOGLE_AUTH_CLIENT_SECRET'),
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: [
    JoinHandler,
    LoginEmailRequestHandler,
    LoginEmailCodeSenderInterface,
    LoginEmailConfirmHandler,
    AuthUserInfoHandler,
    UserRepositoryInterface,
    TokenRefreshHandler,
    UsersByIdsHandler,
    UpdateRolesHandler,
  ],
})
export class AuthModule {}
