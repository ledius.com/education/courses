import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPicture1647849234766 implements MigrationInterface {
  public readonly name = 'AddPicture1647849234766';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" ADD "picture" character varying DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" DROP COLUMN "picture"`,
    );
  }
}
