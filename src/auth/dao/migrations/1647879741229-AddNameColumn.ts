import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNameColumn1647879741229 implements MigrationInterface {
  public readonly name = 'AddNameColumn1647879741229';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" ADD "name" character varying NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" DROP COLUMN "name"`,
    );
  }
}
