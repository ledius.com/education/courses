/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { User } from './user';
import { Email } from './email/email';
import { Phone } from '@/common/dao/phone/phone';
import * as moment from 'moment';
import { Token } from './token';
import { DomainException } from 'node-exceptions';
import { Moment } from 'moment';
import { Id } from './id/id';
import { Role } from '@/permission/role.enum';

describe('User', () => {
  it('should be join user', () => {
    const email = new Email('testy@gmail.com');
    const phone = new Phone('79609889393');
    const id: Id = Id.next();
    const user = User.join(id, email, phone);

    expect(user.getId()).toEqual(id);
    expect(user).toBeDefined();
    expect(user).toBeInstanceOf(User);
    expect(user.getEmail()).toEqual(email);
    expect(user.getPhone()).toEqual(phone);
    expect(user.isEmailConfirmed()).toBeFalsy();
    expect(user.isPhoneConfirmed()).toBeFalsy();
  });

  describe('email confirmation', () => {
    let user: User;
    let now: Moment;
    let expires: Moment;
    beforeEach(() => {
      now = moment();
      expires = now.clone().add('10', 'minutes');
      user = User.join(
        Id.next(),
        new Email('test@gmail.com'),
        new Phone('79609887979'),
      );
    });

    it('should be confirm email', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailConfirmation(confirmationToken, now.toDate());
      expect(user.isEmailConfirmed()).toBeFalsy();

      try {
        user.emailConfirm('1234', now.clone().toDate());
        expect(user.isEmailConfirmed()).toBeTruthy();
      } catch (e) {
        fail('throw exception on correct confirmation');
      }
    });

    it('should be throw error if email already confirmed', () => {
      expect.assertions(2);

      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailConfirmation(confirmationToken, now.toDate());
      user.emailConfirm('1234', now.clone().toDate());

      try {
        user.emailConfirm('1234', now.clone().toDate());
        fail('Email already confirmed');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Email already confirmed');
        }
      }
    });

    it('should be not request if previous request not expired', () => {
      expect.assertions(2);
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailConfirmation(confirmationToken, now.toDate());
      try {
        user.requestEmailConfirmation(confirmationToken, now.toDate());
        fail('confirmation request then previous is not expired');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Email confirmation request already sent');
        }
      }
    });

    it('should not confirm user after request confirmation', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailConfirmation(confirmationToken, now.toDate());
      expect(user.isEmailConfirmed()).toBeFalsy();
    });

    it('should be error if email confirmation is not requested', () => {
      expect.assertions(3);

      try {
        user.emailConfirm('1234', now.clone().toDate());
        fail('confirmation not requested');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Email confirmation not requested');
          expect(user.isEmailConfirmed()).toBeFalsy();
        }
      }
    });

    it('should not confirm if token is invalid', () => {
      expect.assertions(3);

      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailConfirmation(confirmationToken, now.toDate());

      try {
        user.emailConfirm('1234555', now.clone().toDate());
        fail('confirm email with wrong token');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is invalid');
          expect(user.isEmailConfirmed()).toBeFalsy();
        }
      }
    });

    it('should not confirm if token is expired', () => {
      expect.assertions(3);

      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailConfirmation(confirmationToken, now.toDate());

      try {
        user.emailConfirm('1234', expires.clone().add('1', 'seconds').toDate());
        fail('confirm email with wrong token');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is expired');
          expect(user.isEmailConfirmed()).toBeFalsy();
        }
      }
    });
  });

  describe('phone confirmation', () => {
    let user: User;
    let now: Moment;
    let expires: Moment;

    beforeEach(() => {
      now = moment();
      expires = now.clone().add('10', 'minutes');
      user = User.join(
        Id.next(),
        new Email('test@gmail.com'),
        new Phone('79609887979'),
      );
    });

    it('should be confirm phone', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneConfirmation(confirmationToken, now.toDate());
      expect(user.isPhoneConfirmed()).toBeFalsy();

      try {
        user.phoneConfirm('1234', now.clone().toDate());
      } catch (e) {
        fail('throw exception on correct confirmation');
      } finally {
        expect(user.isPhoneConfirmed()).toBeTruthy();
      }
    });

    it('should be throw error if phone already confirmed', () => {
      expect.assertions(2);

      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneConfirmation(confirmationToken, now.toDate());
      user.phoneConfirm('1234', now.clone().toDate());

      try {
        user.phoneConfirm('1234', now.clone().toDate());
        fail('Phone already confirmed');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Phone already confirmed');
        }
      }
    });

    it('should be not request if previous request not expired', () => {
      expect.assertions(2);

      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneConfirmation(confirmationToken, now.toDate());
      try {
        user.requestPhoneConfirmation(confirmationToken, now.toDate());
        fail('confirmation request then previous is not expired');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Phone confirmation request already sent');
        }
      }
    });

    it('should not confirm phone after request confirmation', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneConfirmation(confirmationToken, now.toDate());
      expect(user.isPhoneConfirmed()).toBeFalsy();
    });

    it('should be error if phone confirmation is not requested', () => {
      expect.assertions(3);

      try {
        user.phoneConfirm('1234', now.clone().toDate());
        fail('confirmation not requested');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Phone confirmation not requested');
          expect(user.isPhoneConfirmed()).toBeFalsy();
        }
      }
    });

    it('should not confirm if token is invalid', () => {
      expect.assertions(3);

      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneConfirmation(confirmationToken, now.toDate());

      try {
        user.phoneConfirm('1234555', now.clone().toDate());
        fail('confirm email with wrong token');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is invalid');
          expect(user.isPhoneConfirmed()).toBeFalsy();
        }
      }
    });

    it('should not confirm if token is expired', () => {
      expect.assertions(3);
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneConfirmation(confirmationToken, now.toDate());

      try {
        user.phoneConfirm('1234', expires.clone().add('1', 'seconds').toDate());
        fail('confirm email with wrong token');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is expired');
          expect(user.isPhoneConfirmed()).toBeFalsy();
        }
      }
    });
  });

  describe('email changing', () => {
    let user: User;
    let now: Moment;
    let expires: Moment;
    let email: Email;
    let replacedEmail: Email;

    beforeEach(() => {
      now = moment();
      expires = now.clone().add('10', 'minutes');
      email = new Email('test@gmail.com');
      user = User.join(Id.next(), email, new Phone('79609887979'));
      replacedEmail = new Email('test1@gmail.com');
    });

    it('should be change email', () => {
      expect(user.getEmail()).toEqual(email);
      const token = Token.create('1234', expires.toDate());
      user.requestEmailChanging(token, replacedEmail, now.toDate());
      expect(user.getEmail()).toEqual(email);

      user.confirmEmailChanging('1234', now.toDate());
      expect(user.getEmail()).toEqual(replacedEmail);
    });

    it('should be not request changing then previous request not expired', () => {
      expect.assertions(2);
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailChanging(confirmationToken, replacedEmail, now.toDate());
      try {
        user.requestEmailChanging(
          confirmationToken,
          replacedEmail,
          now.toDate(),
        );
        fail('confirmation request then previous is not expired');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Email changing request already sent');
        }
      }
    });

    it('should not change email then email changing not requested', async () => {
      expect.assertions(3);
      try {
        user.confirmEmailChanging('1234', now.clone().toDate());
        fail('email changing not requested');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Email changing not requested');
        }
      } finally {
        expect(user.getEmail()).toEqual(email);
      }
    });

    it('should not confirm if token is invalid', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailChanging(confirmationToken, replacedEmail, now.toDate());

      try {
        user.confirmEmailChanging('1234555', now.clone().toDate());
        fail('email changed with wrong token');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is invalid');
        }
      } finally {
        expect(user.getEmail()).toEqual(email);
      }
    });

    it('should not confirm if token is expired', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestEmailChanging(confirmationToken, replacedEmail, now.toDate());

      try {
        user.confirmEmailChanging(
          '1234',
          expires.clone().add('1', 'seconds').toDate(),
        );
        fail('email changed with expired token');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is expired');
        }
      } finally {
        expect(user.getEmail()).toEqual(email);
      }
    });
  });

  describe('phone changing', () => {
    let user: User;
    let now: Moment;
    let expires: Moment;
    let phone: Phone;
    let replacedPhone: Phone;

    beforeEach(() => {
      now = moment();
      expires = now.clone().add('10', 'minutes');
      phone = new Phone('79609887979');
      user = User.join(Id.next(), new Email('test@gmail.com'), phone);
      replacedPhone = new Phone('79609882222');
    });

    it('should be change phone', () => {
      expect(user.getPhone()).toEqual(phone);
      const token = Token.create('1234', expires.toDate());
      user.requestPhoneChanging(token, replacedPhone, now.toDate());
      expect(user.getPhone()).toEqual(phone);

      user.confirmPhoneChanging('1234', now.toDate());
      expect(user.getPhone()).toEqual(replacedPhone);
    });

    it('should be not request changing then previous request not expired', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneChanging(confirmationToken, replacedPhone, now.toDate());
      try {
        user.requestPhoneChanging(
          confirmationToken,
          replacedPhone,
          now.toDate(),
        );
        fail('confirmation request then previous is not expired');
      } catch (e) {
        expect.assertions(2);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Email changing request already sent');
        }
      }
    });

    it('should not change phone then phone changing not requested', async () => {
      try {
        user.confirmPhoneChanging('1234', now.clone().toDate());
        fail('email changing not requested');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Phone changing not requested');
        }
      } finally {
        expect(user.getPhone()).toEqual(phone);
      }
    });

    it('should not confirm if token is invalid', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneChanging(confirmationToken, replacedPhone, now.toDate());

      try {
        user.confirmPhoneChanging('1234555', now.clone().toDate());
        fail('email changed with wrong token');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is invalid');
        }
      } finally {
        expect(user.getPhone()).toEqual(phone);
      }
    });

    it('should not confirm if token is expired', () => {
      const confirmationToken = Token.create('1234', expires.toDate());
      user.requestPhoneChanging(confirmationToken, replacedPhone, now.toDate());

      try {
        user.confirmPhoneChanging(
          '1234',
          expires.clone().add('1', 'seconds').toDate(),
        );
        fail('email changed with expired token');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is expired');
        }
      } finally {
        expect(user.getPhone()).toEqual(phone);
      }
    });
  });

  describe('roles', () => {
    let user: User;

    beforeEach(() => {
      user = User.join(
        Id.next(),
        new Email('test@gmail.com'),
        new Phone('79609889090'),
      );
    });

    it('should be empty roles after join only', () => {
      expect(user.getRoles()).toHaveLength(1);
    });

    describe('add role', () => {
      let customerUser: User;

      beforeEach(() => {
        customerUser = user.addRole(Role.ADMIN);
      });

      it('should be immutable', () => {
        expect(customerUser).not.toEqual(user);
      });

      it('should be have roles with customer', () => {
        expect(customerUser.getRoles()).toMatchObject([
          Role.CUSTOMER,
          Role.ADMIN,
        ]);
      });

      describe('remove role', () => {
        let emptyUser: User;

        beforeEach(() => {
          emptyUser = customerUser.removeRole(Role.CUSTOMER);
        });

        it('should be immutable', () => {
          expect(customerUser).not.toEqual(emptyUser);
        });

        it('should be have empty roles', () => {
          expect(emptyUser.getRoles()).toEqual([Role.ADMIN]);
        });
      });
    });
  });
});
