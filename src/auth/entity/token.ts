/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DomainException, InvalidArgumentException } from 'node-exceptions';
import * as Joi from 'joi';
import { Column } from 'typeorm';

export class Token {
  @Column({
    name: 'value',
    type: 'varchar',
    nullable: true,
    length: 100,
  })
  private readonly value: string | null;

  @Column({
    name: 'expires',
    type: 'timestamptz',
    nullable: true,
  })
  private readonly expires: Date | null;

  private constructor(token: string | null, expires: Date | null) {
    this.value = token;
    this.expires = expires;
  }

  public static create(token: string, expires: Date): Token {
    const tokenValue = token.trim();
    Joi.assert(
      tokenValue,
      Joi.string().min(1),
      new InvalidArgumentException('Token cannot be empty'),
    );
    return new Token(tokenValue, expires);
  }

  public getValue(): string {
    return this.value || '';
  }

  public verify(value: string): boolean {
    return this.value === value;
  }

  public isExpiredTo(date: Date): boolean {
    if (!this.expires) {
      return true;
    }
    return this.expires <= date;
  }

  public validate(token: string, date: Date): void {
    if (token !== this.value) {
      throw new DomainException('Token is invalid');
    }
    if (this.isExpiredTo(date)) {
      throw new DomainException('Token is expired');
    }
  }
}
