/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Email } from './email/email';
import { Phone } from '@/common/dao/phone/phone';
import { Token } from './token';
import { DomainException } from 'node-exceptions';
import { Id } from './id/id';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { IdTransformer } from './id/id-transformer';
import { EmailTransformer } from './email/email-transformer';
import { PhoneColumn } from '@/common/dao/phone/phone-column';
import { Role } from '@/permission/role.enum';

@Entity('auth_users')
export class User {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: new IdTransformer(),
  })
  public readonly id!: Id | string;

  @Column({
    name: 'email',
    unique: true,
    nullable: false,
    type: 'varchar',
    length: 100,
    transformer: new EmailTransformer(),
  })
  private email!: Email;

  @Column({
    name: 'email_confirmed',
    type: 'boolean',
    default: false,
    nullable: false,
  })
  private emailConfirmed: boolean;

  @Column(() => Token, { prefix: 'email_confirmation_token' })
  private emailConfirmationToken!: Token | null;

  @Column(() => Token, { prefix: 'email_changing_token' })
  private emailChangingToken!: Token | null;

  @Column({
    name: 'new_email',
    unique: true,
    nullable: true,
    type: 'varchar',
    length: 100,
    transformer: new EmailTransformer(),
  })
  private newEmail!: Email | null;

  @PhoneColumn({
    nullable: true,
  })
  public phone?: Phone;

  @Column({
    name: 'phone_confirmed',
    type: 'boolean',
    default: false,
    nullable: false,
  })
  private phoneConfirmed: boolean;

  @Column(() => Token, { prefix: 'phone_confirmation_token' })
  private phoneConfirmationToken!: Token | null;

  @Column(() => Token, { prefix: 'phone_changing_token' })
  private phoneChangingToken!: Token | null;

  @PhoneColumn({
    nullable: true,
    name: 'new_phone',
  })
  private newPhone!: Phone | null;

  @Column({
    type: 'varchar',
    default: '',
  })
  public readonly name!: string;

  @Column({
    nullable: true,
    type: 'varchar',
    default: '',
  })
  public readonly picture?: string;

  @Column({
    type: 'varchar',
    array: true,
    default: '{}',
  })
  public readonly roles!: Role[];

  private constructor(
    id: Id,
    email: Email,
    phone?: Phone,
    roles?: Role[],
    picture?: string,
    name?: string,
  ) {
    this.id = id;
    this.email = email;
    this.phone = phone;
    this.emailConfirmed = false;
    this.phoneConfirmed = false;
    if (roles && roles.length) {
      this.roles = roles;
    }
    this.picture = picture;
    this.name = name || '';
  }

  public static join(id: Id, email: Email, phone?: Phone): User {
    return new User(id, email, phone, [Role.CUSTOMER]);
  }

  public getId(): Id {
    return this.id as Id;
  }

  public getPhone(): Phone | undefined {
    return this.phone || undefined;
  }
  public getEmail(): Email {
    return this.email;
  }

  public getRoles(): Role[] {
    return this.roles || [];
  }

  public isEmailConfirmed(): boolean {
    return this.emailConfirmed;
  }

  public isPhoneConfirmed(): boolean {
    return this.phoneConfirmed;
  }

  public requestEmailConfirmation(confirmationToken: Token, date: Date): void {
    if (
      this.emailConfirmationToken &&
      !this.emailConfirmationToken.isExpiredTo(date)
    ) {
      throw new DomainException('Email confirmation request already sent');
    }
    this.emailConfirmationToken = confirmationToken;
  }

  public emailConfirm(token: string, now: Date): void {
    if (!this.emailConfirmationToken) {
      throw new DomainException('Email confirmation not requested');
    }
    if (this.emailConfirmed) {
      throw new DomainException('Email already confirmed');
    }
    this.emailConfirmationToken.validate(token, now);
    this.emailConfirmed = true;
  }

  public requestPhoneConfirmation(confirmationToken: Token, date: Date): void {
    if (
      this.phoneConfirmationToken &&
      !this.phoneConfirmationToken.isExpiredTo(date)
    ) {
      throw new DomainException('Phone confirmation request already sent');
    }
    this.phoneConfirmationToken = confirmationToken;
  }

  public phoneConfirm(token: string, now: Date): void {
    if (!this.phoneConfirmationToken) {
      throw new DomainException('Phone confirmation not requested');
    }
    if (this.phoneConfirmed) {
      throw new DomainException('Phone already confirmed');
    }
    this.phoneConfirmationToken.validate(token, now);
    this.phoneConfirmed = true;
  }

  public requestEmailChanging(
    token: Token,
    replacedEmail: Email,
    date: Date,
  ): void {
    if (this.emailChangingToken && !this.emailChangingToken.isExpiredTo(date)) {
      throw new DomainException('Email changing request already sent');
    }
    this.emailChangingToken = token;
    this.newEmail = replacedEmail;
  }

  public confirmEmailChanging(token: string, now: Date): void {
    if (!this.emailChangingToken || !this.newEmail) {
      throw new DomainException('Email changing not requested');
    }
    this.emailChangingToken.validate(token, now);
    this.email = this.newEmail;
  }

  public requestPhoneChanging(
    token: Token,
    replacedPhone: Phone,
    date: Date,
  ): void {
    if (this.phoneChangingToken && !this.phoneChangingToken.isExpiredTo(date)) {
      throw new DomainException('Email changing request already sent');
    }
    this.phoneChangingToken = token;
    this.newPhone = replacedPhone;
  }

  public confirmPhoneChanging(token: string, now: Date): void {
    if (!this.phoneChangingToken) {
      throw new DomainException('Phone changing not requested');
    }
    if (!this.newPhone) {
      throw new DomainException('Phone changing not requested');
    }
    this.phoneChangingToken.validate(token, now);
    this.phone = this.newPhone;
  }

  public changePicture(picture = ''): User {
    return new User(this.id as Id, this.email, this.phone, this.roles, picture);
  }

  public rename(name: string): User {
    return new User(
      this.id as Id,
      this.email,
      this.phone,
      this.roles,
      this.picture,
      name,
    );
  }

  public isAdmin(): boolean {
    return Boolean(this.getRoles().find((role) => role === Role.ADMIN));
  }

  public isRoot(): boolean {
    return this.getRoles().includes(Role.ROOT);
  }

  public setRoles(...roles: Role[]): User {
    return new User(
      this.id as Id,
      this.email,
      this.phone,
      roles,
      this.picture,
      this.name,
    );
  }

  public addRole(role: Role): User {
    if (this.roles.includes(role)) {
      return this;
    }
    const roles = [...this.getRoles(), role];
    return new User(this.id as Id, this.email, this.phone, roles);
  }

  public removeRole(role: Role): User {
    const roles = this.getRoles().filter((item) => item !== role);
    return new User(this.id as Id, this.email, this.phone, roles);
  }
}
