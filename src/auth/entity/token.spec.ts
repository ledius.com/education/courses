/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Token } from './token';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DomainException, InvalidArgumentException } from 'node-exceptions';

describe('Token', () => {
  it('should be defined', () => {
    const now = moment();
    const token = Token.create(
      '1234',
      now.clone().add('10', 'minutes').toDate(),
    );

    expect(token.verify('1234')).toBeTruthy();
    expect(token.verify('12345')).toBeFalsy();
    expect(token.getValue()).toBe('1234');
    expect(token.isExpiredTo(now.toDate())).toBeFalsy();
    expect(
      token.isExpiredTo(now.clone().add('11', 'minutes').toDate()),
    ).toBeTruthy();
    expect(
      token.isExpiredTo(now.clone().add('10', 'minutes').toDate()),
    ).toBeTruthy();
  });

  describe('validate', () => {
    let token: Token;
    let now: Moment;

    beforeEach(() => {
      now = moment();
      token = Token.create('1234', now.clone().add('10', 'minutes').toDate());
    });

    it('should be passed', () => {
      try {
        token.validate('1234', now.toDate());
      } catch (e) {
        fail('validate not passed');
      }
    });

    it('should be not pass with invalid token', () => {
      try {
        token.validate('12345', now.toDate());
        fail('validate passed');
      } catch (e) {
        expect.assertions(2);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is invalid');
        }
      }
    });

    it('should be not pass expires', () => {
      try {
        token.validate('1234', now.clone().add('11', 'minutes').toDate());
        fail('validate passed');
      } catch (e) {
        expect.assertions(2);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Token is expired');
        }
      }
    });
  });

  it('should not be empty', () => {
    try {
      Token.create('  ', new Date());
      fail('Token can be empty');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof InvalidArgumentException) {
        expect(e).toBeInstanceOf(InvalidArgumentException);
        expect(e.message).toBe('Token cannot be empty');
      }
    }
  });

  it('should not be have whitespaces', () => {
    const now = moment();
    const expires = now.clone().add('1', 'minutes');
    const token = Token.create(' 123  ', expires.toDate());
    expect(token.verify('123')).toBeTruthy();
  });
});
