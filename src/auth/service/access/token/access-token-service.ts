/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AccessTokenServiceInterface } from './access-token-service-interface';
import { User } from '@/auth/entity/user';
import { OAuthAccessTokenModel } from '@/auth/model/access/token/oauth-access-token-model';
import { bearer } from '@/auth/model/access/token/oauth-access-token-builder';
import { JwtService } from '@nestjs/jwt';
import * as moment from 'moment';
import { Injectable } from '@nestjs/common';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { Id } from '@/auth/entity/id/id';
import { LogicalException } from 'node-exceptions';
import { TokenPayload } from '@/jwt/interfaces/token-payload.interface';
import { Audience } from '@/jwt/audience';

@Injectable()
export class AccessTokenService implements AccessTokenServiceInterface {
  public constructor(
    private readonly jwtService: JwtService,
    private readonly repository: UserRepositoryInterface,
  ) {}

  public async refresh(
    refreshToken: string,
    now: Date = new Date(),
  ): Promise<OAuthAccessTokenModel> {
    const user = await this.getUser(refreshToken);
    return await this.releaseFor(user, now);
  }

  public async releaseFor(
    user: User,
    now: Date = new Date(),
  ): Promise<OAuthAccessTokenModel> {
    const time = moment(now);

    const payload: TokenPayload = {
      userId: user.getId().getValue(),
      email: user.getEmail().getValue(),
      audience: Audience.USER,
      roles: user.getRoles(),
      token: '',
      iat: time.clone().unix(),
    };

    const access = this.jwtService.sign(payload, { expiresIn: '1h' });
    const refresh = this.jwtService.sign(payload, { expiresIn: '30d' });

    return OAuthAccessTokenModel.access(() => access)
      .expiresIn(() => time.add(1, 'h').toDate())
      .refresh(() => refresh)
      .create(bearer);
  }

  public async getUser(accessToken: string): Promise<User> {
    try {
      const decoded: TokenPayload = this.jwtService.verify(accessToken);
      return await this.repository.getById(new Id(decoded.userId));
    } catch (e) {
      throw new LogicalException('Authorization is expired');
    }
  }
}
