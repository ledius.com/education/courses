/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AccessTokenService } from './access-token-service';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { JwtService } from '@nestjs/jwt';
import { bearer } from '@/auth/model/access/token/oauth-access-token-builder';
import { Moment } from 'moment';
import * as moment from 'moment';
import { mock, MockProxy } from 'jest-mock-extended';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { LogicalException } from 'node-exceptions';
import { Phone } from '@/common/dao/phone/phone';
import { TokenPayload } from '@/jwt/interfaces/token-payload.interface';
import { Audience } from '@/jwt/audience';
import { Role } from '@/permission/role.enum';

describe('AccessTokenService', () => {
  let jwt: JwtService;
  let service: AccessTokenService;
  let repository: MockProxy<UserRepositoryInterface> & UserRepositoryInterface;
  let user: User;

  beforeEach(() => {
    repository = mock<UserRepositoryInterface>();
    jwt = new JwtService({ secret: 'hello.world' });
    service = new AccessTokenService(jwt, repository);
    user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
  });

  it('should be create correct token', async () => {
    const now: Moment = moment().add(1, 'd');
    const token = await service.releaseFor(user, now.clone().toDate());
    const decodedAccess = jwt.verify<TokenPayload>(token.getAccess());
    const decodedRefresh = jwt.verify<TokenPayload>(token.getRefresh());

    expect(token.getType()).toBe(bearer());
    expect(decodedAccess).toMatchObject({
      userId: user.getId().getValue(),
      token: '',
      email: user.getEmail().getValue(),
      audience: Audience.USER,
      iat: now.clone().unix(),
      exp: now.clone().add(1, 'h').unix(),
    });

    expect(decodedRefresh).toMatchObject({
      userId: user.getId().getValue(),
      iat: now.clone().unix(),
      exp: now.clone().add(30, 'd').unix(),
    });
  });

  it('should be use default now date arg', async () => {
    const now: Moment = moment();
    const token = await service.releaseFor(user);
    const decodedAccess = jwt.verify<TokenPayload>(token.getAccess());
    const decodedRefresh = jwt.verify<TokenPayload>(token.getRefresh());

    expect(token.getType()).toBe(bearer());
    expect(decodedAccess).toMatchObject({
      userId: user.getId().getValue(),
      token: '',
      email: user.getEmail().getValue(),
      audience: Audience.USER,
      iat: now.clone().unix(),
      exp: now.clone().add(1, 'h').unix(),
    });

    expect(decodedRefresh).toMatchObject({
      userId: user.getId().getValue(),
      token: '',
      email: user.getEmail().getValue(),
      audience: Audience.USER,
      iat: now.clone().unix(),
      exp: now.clone().add(30, 'd').unix(),
    });
  });

  it('should be return encoded user', async () => {
    repository.getById.mockReturnValue(Promise.resolve(user));
    const token = await service.releaseFor(user);
    const result: User = await service.getUser(token.getAccess());

    expect(result).toEqual(user);
    expect(repository.getById).toBeCalledWith(user.getId());
  });

  it('should be throw exception if token expired', async () => {
    const token = await service.releaseFor(
      user,
      moment().subtract(1, 'd').toDate(),
    );

    try {
      await service.getUser(token.getAccess());
      fail('success decoded with expired token');
    } catch (e) {
      if (e instanceof LogicalException) {
        expect(e).toBeInstanceOf(LogicalException);
        expect(e.message).toBe('Authorization is expired');
      }
    }
  });

  describe('refresh', () => {
    it('should be refresh released token', async () => {
      repository.getById.mockReturnValue(Promise.resolve(user));
      const now: Moment = moment().add(1, 'd');

      const token = await service.releaseFor(user, now.toDate());
      const refreshed = await service.refresh(token.getRefresh(), now.toDate());

      const decodedAccess = jwt.verify<TokenPayload>(refreshed.getAccess());
      const decodedRefresh = jwt.verify<TokenPayload>(refreshed.getRefresh());

      expect(decodedAccess).toStrictEqual({
        userId: user.getId().getValue(),
        roles: [Role.CUSTOMER],
        token: '',
        email: user.getEmail().getValue(),
        audience: Audience.USER,
        iat: now.clone().unix(),
        exp: now.clone().add(1, 'h').unix(),
      });

      expect(decodedRefresh).toStrictEqual({
        userId: user.getId().getValue(),
        roles: [Role.CUSTOMER],
        token: '',
        email: user.getEmail().getValue(),
        audience: Audience.USER,
        iat: now.clone().unix(),
        exp: now.clone().add(30, 'd').unix(),
      });
    });
  });
});
