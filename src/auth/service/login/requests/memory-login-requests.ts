/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginRequestsInterface } from './login-requests-interface';
import { User } from '../../../entity/user';
import { Token } from '../../../entity/token';

export class MemoryLoginRequests implements LoginRequestsInterface {
  private readonly requests: Map<string, [User, Token]> = new Map();

  public async add(user: User, token: Token): Promise<void> {
    this.requests.set(user.getId().getValue(), [user, token]);
  }

  public async has(user: User, code: string): Promise<boolean> {
    const [, token] = this.requests.get(user.getId().getValue()) || [];
    if (!token) {
      return false;
    }
    return token.verify(code) && !token.isExpiredTo(new Date());
  }

  public async revoke(user: User): Promise<void> {
    this.requests.delete(user.getId().getValue());
  }

  public async revokeAll(): Promise<void> {
    this.requests.clear();
  }
}
