/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { MemoryLoginRequests } from './memory-login-requests';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { Token } from '@/auth/entity/token';
import * as moment from 'moment';

describe('MemoryLoginRequests', () => {
  let requests: MemoryLoginRequests;

  beforeEach(() => {
    requests = new MemoryLoginRequests();
  });

  it('should be has request', async () => {
    const user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const token = Token.create('1234', moment().add('1', 'h').toDate());

    await requests.add(user, token);
    expect(await requests.has(user, '1234')).toBeTruthy();
  });

  it('should be not has request with wrong code', async () => {
    const user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const token = Token.create('1234', moment().add('1', 'h').toDate());

    await requests.add(user, token);
    expect(await requests.has(user, '1211')).toBeFalsy();
  });

  it('should be not has if token is expired', async () => {
    const user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const token = Token.create(
      '1234',
      moment().subtract('1', 'minutes').toDate(),
    );

    await requests.add(user, token);
    expect(await requests.has(user, '1234')).toBeFalsy();
  });

  it('should be revoke request of user', async () => {
    const user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const token = Token.create('1234', moment().add('1', 'minutes').toDate());

    await requests.add(user, token);
    expect(await requests.has(user, '1234')).toBeTruthy();
    await requests.revoke(user);
    expect(await requests.has(user, '1234')).toBeFalsy();
  });

  it('should be revoke all requests', async () => {
    const user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const user1 = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const user2 = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const token = Token.create('1234', moment().add('1', 'minutes').toDate());

    await requests.add(user, token);
    await requests.add(user1, token);
    await requests.add(user2, token);

    expect(await requests.has(user, '1234')).toBeTruthy();
    expect(await requests.has(user1, '1234')).toBeTruthy();
    expect(await requests.has(user2, '1234')).toBeTruthy();

    await requests.revokeAll();

    expect(await requests.has(user, '1234')).toBeFalsy();
    expect(await requests.has(user1, '1234')).toBeFalsy();
    expect(await requests.has(user2, '1234')).toBeFalsy();
  });
});
