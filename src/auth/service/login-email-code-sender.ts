/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailCodeSenderInterface } from './login-email-code-sender-interface';
import { Email } from '../entity/email/email';
import { Email as MailerEmail } from '../../mailer/email';
import { Token } from '../entity/token';
import { MailerInterface } from '../../mailer/mailer-interface';
import { template } from '../../mailer/template';
import { Injectable } from '@nestjs/common';

@Injectable()
export class LoginEmailCodeSender implements LoginEmailCodeSenderInterface {
  public constructor(private readonly mailer: MailerInterface) {}

  public async send(email: Email, token: Token): Promise<void> {
    await this.mailer.send(
      new MailerEmail(email.getValue()),
      template('mail/auth/login/code/ru/index'),
      { code: token.getValue() },
    );
  }
}
