/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Tokenizer } from './tokenizer';
import * as moment from 'moment';

describe('Tokenizer', () => {
  it('should be generate', () => {
    const expired = moment().add(60, 'minutes').toDate();
    const expiration = jest.fn().mockImplementation((): Date => expired);
    const tokenizer = new Tokenizer(expiration);

    const token = tokenizer.generate();

    const notExpired = moment().add(59, 'minutes').toDate();

    expect(Number(token.getValue())).not.toBeNaN();
    expect(Number(token.getValue())).toBeLessThanOrEqual(9999);
    expect(Number(token.getValue())).toBeGreaterThanOrEqual(1000);
    expect(token.isExpiredTo(notExpired)).toBeFalsy();
    expect(token.isExpiredTo(expired)).toBeTruthy();
    expect(expiration).toBeCalled();
  });
});
