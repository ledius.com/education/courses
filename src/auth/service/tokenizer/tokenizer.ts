/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { TokenizerInterface } from './tokenizer-interface';
import { Token } from '../../entity/token';
import randomNumber = require('random-number');

export class Tokenizer implements TokenizerInterface {
  public constructor(private readonly expiration: { (now: Date): Date }) {}

  public generate(): Token {
    const code = randomNumber({
      min: 1000,
      max: 9999,
      integer: true,
    }).toString();
    return Token.create(code, this.expiration(new Date()));
  }
}
