/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailCodeSender } from './login-email-code-sender';
import { Token } from '../entity/token';
import { Email } from '../entity/email/email';
import { Email as MailerEmail } from '../../mailer/email';
import { MailerInterface } from '../../mailer/mailer-interface';
import { mock } from 'jest-mock-extended';
import { template } from '../../mailer/template';

describe('LoginEmailCodeSender', () => {
  it('should be defined', async () => {
    const mailer = mock<MailerInterface>();
    const sender = new LoginEmailCodeSender(mailer);
    await sender.send(
      new Email('test@gmail.com'),
      Token.create('1234', new Date('2030')),
    );

    expect(mailer.send).toBeCalledWith(
      new MailerEmail('test@gmail.com'),
      template('mail/auth/login/code/ru/index'),
      { code: '1234' },
    );
  });
});
