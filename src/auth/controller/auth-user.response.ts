import { ApiProperty } from '@nestjs/swagger';
import { User } from '@/auth/entity/user';
import { Role } from '@/permission/role.enum';

export class AuthUserResponse {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  public readonly id: string;

  @ApiProperty({
    type: 'string',
    format: 'email',
  })
  public readonly email: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly picture: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly name: string = '';

  @ApiProperty({})
  public readonly roles: Role[];

  public constructor(user: User) {
    this.id = user.getId().getValue();
    this.email = user.getEmail().getValue();
    this.picture = user.picture || '';
    this.name = user.name;
    this.roles = user.getRoles();
  }
}
