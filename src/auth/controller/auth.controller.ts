import { Body, Controller, Get, Post, Put, UseGuards } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { GoogleSignInCommand } from '@/auth/handler/google-sign-in/google-sign-in.command';
import { AuthTokenResponse } from '@/auth/controller/auth-token.response';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { TemplateEngineInterface } from '@/template/template-engine.interface';
import { template } from '@/mailer/template';
import { OAuth2Client } from 'google-auth-library';
import { UpdateRolesCommand } from '@/auth/handler/update-roles/update-roles.command';
import { AuthUserResponse } from '@/auth/controller/auth-user.response';
import { UseRoles } from '@/permission/decorators/use-roles';
import { Role } from '@/permission/role.enum';
import { AuthGuard } from '@/jwt/guard/auth-guard';
import { RoleGuard } from '@/permission/guard/role.guard';
import { AuthPayload } from '@/jwt/decorator/auth-payload';
import { TokenPayload } from '@/jwt/interfaces/token-payload.interface';

@ApiTags('Authentication')
@Controller({
  version: '1',
})
export class AuthController {
  public constructor(
    private readonly commandBus: CommandBus,
    private readonly templateEngine: TemplateEngineInterface,
    private readonly oAuth2Client: OAuth2Client,
  ) {}

  @Get('/auth/google-sign-in')
  public async getGoogleSignInForm(): Promise<string> {
    return this.templateEngine.render(template('auth/google-sign-in.html'), {
      GOOGLE_AUTH_CLIENT_ID: this.oAuth2Client._clientId as string,
    });
  }

  @Post('/auth/google-sign-in')
  public async googleSignIn(
    @Body() command: GoogleSignInCommand,
  ): Promise<AuthTokenResponse> {
    return new AuthTokenResponse(await this.commandBus.execute(command));
  }

  @Put('/auth/update-roles')
  @ApiOkResponse({
    type: AuthUserResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard, RoleGuard)
  @UseRoles(Role.ROOT, Role.ADMIN)
  public async updateRoles(
    @Body() command: UpdateRolesCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<AuthUserResponse> {
    command.emitterUserId = tokenPayload.userId;
    return new AuthUserResponse(await this.commandBus.execute(command));
  }
}
