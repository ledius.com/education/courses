/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AuthTokenResponse } from './auth-token.response';
import { OAuthAccessTokenModel } from '@/auth/model/access/token/oauth-access-token-model';
import {
  bearer,
  dummy,
  hour,
} from '@/auth/model/access/token/oauth-access-token-builder';

describe('LoginConfirmResponse', () => {
  it('should be defined', () => {
    const token = OAuthAccessTokenModel.access(dummy)
      .refresh(dummy)
      .expiresIn(hour)
      .create(bearer);
    const response = new AuthTokenResponse(token);
    expect(response.type).toBe('bearer');
    expect(response.expiresIn).toEqual(token.getExpiresIn());
    expect(response.accessToken).toBe(dummy());
    expect(response.refreshToken).toBe(dummy());
  });
});
