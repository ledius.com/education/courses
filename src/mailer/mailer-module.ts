/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { NodeMailer } from './driver/nodemailer/node-mailer';
import { createTransport } from 'nodemailer';
import { TemplateEngineInterface } from '@/template/template-engine.interface';
import { HandlebarsTemplateEngine } from '@/template/handlebars-template-engine';
import { FileReader } from '@/fs/file/reader/file-reader';
import { MailerInterface } from './mailer-interface';
import * as path from 'path';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MailerController } from '@/mailer/controller/mailer.controller';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [MailerController],
  providers: [
    {
      provide: TemplateEngineInterface,
      useFactory: (): TemplateEngineInterface => {
        const templateDir = path.join(__dirname, '..', '..', 'template');
        return new HandlebarsTemplateEngine(new FileReader(), templateDir);
      },
    },
    {
      provide: NodeMailer,
      useFactory: (
        templateEngine: TemplateEngineInterface,
        config: ConfigService,
      ): NodeMailer => {
        const transporter = createTransport(
          {
            host: config.get('SMTP_HOST'),
            port: Number(config.get('SMTP_PORT')),
            secure: false,
            auth: {
              user: config.get('SMTP_USER'),
              pass: config.get('SMTP_PASS'),
            },
          },
          {
            from: config.get('SMTP_DEFAULT_FROM'),
          },
        );
        return new NodeMailer(transporter, templateEngine);
      },
      inject: [TemplateEngineInterface, ConfigService],
    },
    {
      provide: MailerInterface,
      useExisting: NodeMailer,
    },
  ],
  exports: [MailerInterface],
})
export class MailerModule {}
