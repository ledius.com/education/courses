/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { MailerInterface } from '../../mailer-interface';
import { Template } from '../../template';
import { Email } from '../../email';
import { Transporter } from 'nodemailer';
import { TemplateEngineInterface } from '../../../template/template-engine.interface';

export class NodeMailer implements MailerInterface {
  public constructor(
    private readonly transporter: Transporter,
    private readonly templateEngine: TemplateEngineInterface,
  ) {}

  public async send(
    email: Email,
    template: Template,
    props: Record<string, string> = {},
  ): Promise<void> {
    const [html, text] = [
      await this.templateEngine.render(template.append('.html'), props),
      await this.templateEngine.render(template.append('.txt'), props),
    ];
    await this.transporter.sendMail({
      to: email.getValue(),
      subject: props.subject || 'no-reply',
      text,
      html,
    });
  }
}
