/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { NodeMailer } from './node-mailer';
import { mock } from 'jest-mock-extended';
import { Transporter } from 'nodemailer';
import { email } from '../../email';
import { template } from '../../template';
import { TemplateEngineInterface } from '../../../template/template-engine.interface';

describe('NodeMailer', () => {
  it('should be sent mail via nodemailer', async () => {
    const transporter = mock<Transporter>();
    const templateEngine = mock<TemplateEngineInterface>();
    templateEngine.render.mockReturnValue(Promise.resolve('Welcome John'));
    const mailer = new NodeMailer(transporter, templateEngine);
    await mailer.send(email('test@gmail.com'), template('welcome/index'), {
      name: 'John',
    });

    expect(transporter.sendMail).toBeCalledWith({
      to: 'test@gmail.com',
      subject: 'no-reply',
      text: 'Welcome John',
      html: 'Welcome John',
    });
    expect(templateEngine.render).toHaveBeenNthCalledWith(
      1,
      template('welcome/index.html'),
      { name: 'John' },
    );
    expect(templateEngine.render).toHaveBeenNthCalledWith(
      2,
      template('welcome/index.txt'),
      { name: 'John' },
    );
  });
});
