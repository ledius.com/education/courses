import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class SendMailDto {
  @ApiProperty()
  @IsString()
  public recipient!: string;

  @ApiProperty()
  @IsString()
  public subject!: string;

  @ApiProperty()
  @IsString()
  public content!: string;
}
