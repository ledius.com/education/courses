import { MailerInterface } from '@/mailer/mailer-interface';
import {
  Body,
  Controller,
  Header,
  Headers,
  HttpCode,
  Options,
  Post,
} from '@nestjs/common';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { SendMailDto } from '@/mailer/dto/send-mail.dto';
import { template } from '@/mailer/template';
import { email } from '@/mailer/email';
import { createHash } from 'crypto';

@Controller()
@ApiTags('Mailer')
export class MailerController {
  private static readonly secretCode: string = 'cross-mail';

  public constructor(private readonly mailer: MailerInterface) {}

  @Post('send')
  @Options('send')
  @HttpCode(200)
  @Header('Access-Control-Allow-Origin', '*')
  @Header('Access-Control-Allow-Headers', '*')
  @Header('Access-Control-Allow-Methods', 'POST')
  @ApiHeader({
    name: 'X-Secret-Code',
  })
  public async send(
    @Body() dto: SendMailDto,
    @Headers('X-Secret-Code') secretCode: string,
  ): Promise<void> {
    if (
      secretCode ===
      createHash('sha3-256').update(MailerController.secretCode).digest('hex')
    ) {
      await this.mailer.send(
        email(dto.recipient),
        template('mail/mailer/default'),
        {
          content: dto.content,
          subject: dto.subject,
        },
      );
    }
  }
}
