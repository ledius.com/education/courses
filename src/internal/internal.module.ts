import { Module } from '@nestjs/common';
import { AuthModule } from '@/auth/auth.module';
import { AuthInternalController } from '@/internal/auth/controller/auth.internal.controller';

@Module({
  imports: [AuthModule],
  controllers: [AuthInternalController],
})
export class InternalModule {}
