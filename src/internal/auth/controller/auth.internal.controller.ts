import { Controller, Get, Query } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { AuthUserResponse } from '@/auth/controller/auth-user.response';
import { UsersByIdsQuery } from '@/auth/handler/users-by-ids/users-by-ids.query';
import { UsersByIdsHandler } from '@/auth/handler/users-by-ids/users-by-ids.handler';

@Controller('auth')
@ApiTags('Authentication')
export class AuthInternalController {
  public constructor(private readonly usersByIdsHandler: UsersByIdsHandler) {}

  @Get('users')
  @ApiQuery({
    type: 'string',
    name: 'ids',
    required: false,
  })
  public async getUsersByIds(
    @Query() query: UsersByIdsQuery,
  ): Promise<AuthUserResponse[]> {
    const users = await this.usersByIdsHandler.execute(query);
    return users.map((user) => new AuthUserResponse(user));
  }
}
