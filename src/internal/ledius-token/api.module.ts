import {
  DynamicModule,
  HttpService,
  HttpModule,
  Module,
  Global,
} from '@nestjs/common';
import { Configuration } from './configuration';

import { TransferTokenInternalService } from './api/transfer.service';

@Global()
@Module({
  imports: [HttpModule],
  exports: [TransferTokenInternalService],
  providers: [TransferTokenInternalService],
})
export class ApiModule {
  public static forRoot(
    configurationFactory: () => Configuration,
  ): DynamicModule {
    return {
      module: ApiModule,
      providers: [{ provide: Configuration, useFactory: configurationFactory }],
    };
  }

  constructor(_httpService: HttpService) {}
}
