/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Lesson } from '../entity/lesson';
import { Id } from '../entity/id/id';
import { v4 } from 'uuid';
import { CourseId } from '../entity/course/course-id';
import { Content } from '../entity/content/content';
import { Name } from '../entity/content/name/name';
import { Icon } from '../entity/content/icon/icon';
import { Text } from '../entity/content/text/text';

export class LessonBuilder {
  private courseId: CourseId = new CourseId(v4());

  public withCourseId(courseId: CourseId): LessonBuilder {
    this.courseId = courseId;
    return this;
  }

  public build(): Lesson {
    return Lesson.create(
      Id.next(),
      this.courseId,
      new Content(
        new Name('Lesson name'),
        new Icon('lesson-icon'),
        new Text('Lesson text'),
      ),
    );
  }
}
