/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { CreateLessonHandler } from './handler/create/create-lesson-handler';
import { PostgresLessonRepository } from './repository/postgres-lesson-repository';
import { LessonRepositoryInterface } from './repository/lesson-repository-interface.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lesson } from './entity/lesson';
import { EditLessonHandler } from './handler/edit/edit-lesson-handler';
import { ListLessonHandler } from './handler/list/list-lesson-handler';
import { GetByIdLessonHandler } from './handler/get/get-by-id-lesson-handler';

@Module({
  imports: [TypeOrmModule.forFeature([Lesson])],
  providers: [
    CreateLessonHandler,
    EditLessonHandler,
    PostgresLessonRepository,
    ListLessonHandler,
    GetByIdLessonHandler,
    {
      provide: LessonRepositoryInterface,
      useExisting: PostgresLessonRepository,
    },
  ],
  exports: [
    CreateLessonHandler,
    EditLessonHandler,
    ListLessonHandler,
    GetByIdLessonHandler,
    LessonRepositoryInterface,
  ],
})
export class LessonModule {}
