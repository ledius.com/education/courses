/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Id } from './id/id';
import { CourseId } from './course/course-id';
import { Content } from './content/content';
import { Status } from './status/status';
import { DomainException } from 'node-exceptions';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { IdTransformer } from './id/id-transformer';
import { CourseIdTransformer } from './course/course-id-transformer';
import { StatusTransformer } from './status/status-transformer';
import { Order } from '@/lesson/entity/order/order';
import { OrderTransformer } from '@/lesson/entity/order/order-transformer';

@Entity('lessons')
export class Lesson {
  @PrimaryColumn({
    type: 'uuid',
    transformer: new IdTransformer(),
  })
  private readonly id: Id;

  @Column({
    type: 'uuid',
    transformer: new CourseIdTransformer(),
  })
  public readonly courseId: CourseId;

  @Column(() => Content)
  private readonly content: Content;

  @Column({
    type: 'varchar',
    transformer: new StatusTransformer(),
  })
  private readonly status: Status;

  @Column({
    type: 'integer',
    default: 0,
    transformer: new OrderTransformer(),
  })
  public readonly order: Order;

  private constructor(
    id: Id,
    courseId: CourseId,
    content: Content,
    status: Status,
    order: Order = new Order(0),
  ) {
    this.id = id;
    this.courseId = courseId;
    this.content = content;
    this.status = status;
    this.order = order;
  }

  public static create(id: Id, courseId: CourseId, content: Content): Lesson {
    return new Lesson(id, courseId, content, Status.draft());
  }

  public getId(): Id {
    return this.id;
  }

  public getCourseId(): CourseId {
    return this.courseId;
  }

  public getContent(): Content {
    return this.content;
  }

  public isDraft(): boolean {
    return this.status.isDraft();
  }

  public isPublished(): boolean {
    return this.status.isPublished();
  }

  public changeContent(content: Content): Lesson {
    return new Lesson(this.id, this.courseId, content, this.status);
  }

  public changeOrder(order: Order): Lesson {
    return new Lesson(this.id, this.courseId, this.content, this.status, order);
  }

  public publish(): Lesson {
    if (this.getContent().getText().isEmpty()) {
      throw new DomainException('Lesson cannot be published with empty text');
    }
    if (this.isPublished()) {
      throw new DomainException('Lesson already published');
    }
    return new Lesson(this.id, this.courseId, this.content, Status.published());
  }

  public makeDraft(): Lesson {
    if (this.isDraft()) {
      throw new DomainException('Lesson already draft');
    }
    return new Lesson(this.id, this.courseId, this.content, Status.draft());
  }

  public changeCourseId(newCourseId: CourseId): Lesson {
    return new Lesson(this.id, newCourseId, this.content, this.status);
  }
}
