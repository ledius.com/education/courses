/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Lesson } from './lesson';
import { Id } from './id/id';
import { CourseId } from './course/course-id';
import { v4 } from 'uuid';
import { Content } from './content/content';
import { Name } from './content/name/name';
import { Icon } from './content/icon/icon';
import { DomainException } from 'node-exceptions';
import { Text } from './content/text/text';
import { Order } from '@/lesson/entity/order/order';

describe('Lesson', () => {
  it('should be defined', () => {
    const id = Id.next();
    const courseId = new CourseId(v4());
    const content = new Content(new Name('Hello'), new Icon('test'));
    const lesson = Lesson.create(id, courseId, content);

    expect(lesson.getId()).toEqual(id);
    expect(lesson.getCourseId()).toEqual(courseId);
    expect(lesson.getContent()).toEqual(content);
    expect(lesson.isDraft()).toBeTruthy();
    expect(lesson.isPublished()).toBeFalsy();
    expect(lesson.order.getValue()).toBe(0);
  });

  it('should be change content', () => {
    const id = Id.next();
    const courseId = new CourseId(v4());
    const content = new Content(new Name('Hello'), new Icon('test'));
    const lesson = Lesson.create(id, courseId, content);
    const newContent = content.changeText(new Text('Hello world'));
    const newLesson: Lesson = lesson.changeContent(newContent);

    expect(lesson).not.toEqual(newLesson);
    expect(newLesson.getContent()).toEqual(newContent);
  });

  it('should be change order', () => {
    const id = Id.next();
    const courseId = new CourseId(v4());
    const content = new Content(new Name('Hello'), new Icon('test'));
    const lesson = Lesson.create(id, courseId, content);
    const newLesson: Lesson = lesson.changeOrder(new Order(10));

    expect(lesson.order.getValue()).toEqual(0);
    expect(newLesson.order.getValue()).toEqual(10);
  });

  describe('publishing flow', () => {
    let id: Id;
    let courseId: CourseId;
    let content: Content;

    beforeEach(() => {
      id = Id.next();
      courseId = new CourseId(v4());
      content = new Content(new Name('Hello'), new Icon('test'));
    });

    it('should be publish', () => {
      const lesson = Lesson.create(id, courseId, content).changeContent(
        content.changeText(new Text('Hello world')),
      );

      const newLesson = lesson.publish();

      expect(lesson.isDraft()).toBeTruthy();
      expect(lesson.isPublished()).toBeFalsy();
      expect(newLesson.isDraft()).toBeFalsy();
      expect(newLesson.isPublished()).toBeTruthy();
    });

    it('should be throw error if lesson already published', () => {
      const lesson = Lesson.create(id, courseId, content)
        .changeContent(content.changeText(new Text('Hello world')))
        .publish();

      try {
        lesson.publish();
        fail('Published already published lesson');
      } catch (e) {
        expect.assertions(2);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Lesson already published');
        }
      }
    });

    it('should be throw error on try publish if text is empty', () => {
      expect.assertions(5);

      const lesson = Lesson.create(id, courseId, content);

      expect(lesson.isDraft()).toBeTruthy();
      expect(lesson.isPublished()).toBeFalsy();

      try {
        lesson.publish();
        fail('Lesson has been published with empty text');
      } catch (e) {
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Lesson cannot be published with empty text');
          expect(lesson.isDraft()).toBeTruthy();
        }
      }
    });
  });

  describe('make draft flow', () => {
    let id: Id;
    let courseId: CourseId;
    let content: Content;
    let lesson: Lesson;

    beforeEach(() => {
      id = Id.next();
      courseId = new CourseId(v4());
      content = new Content(
        new Name('Hello'),
        new Icon('test'),
        new Text('Hello'),
      );
      lesson = Lesson.create(id, courseId, content);
    });

    it('should be draft published', () => {
      const published = lesson.publish();

      expect(published).not.toEqual(lesson);
      expect(published.isDraft()).toBeFalsy();
      expect(published.isPublished()).toBeTruthy();
    });

    it('should be draft after create', () => {
      expect(lesson.isDraft()).toBeTruthy();
      expect(lesson.isPublished()).toBeFalsy();
    });

    it('should be throw error if try to make draft already drafted lesson', () => {
      try {
        lesson.makeDraft();
        fail('Draft already drafted lesson');
      } catch (e) {
        expect.assertions(3);
        if (e instanceof DomainException) {
          expect(e).toBeInstanceOf(DomainException);
          expect(e.message).toBe('Lesson already draft');
          expect(lesson.isDraft()).toBeTruthy();
        }
      }
    });
  });

  it('should be change course id', () => {
    const id = Id.next();
    const courseId = new CourseId(v4());
    const content = new Content(
      new Name('Hello'),
      new Icon('test'),
      new Text('Hello'),
    );
    const lesson = Lesson.create(id, courseId, content);

    const newCourseId = new CourseId(v4());
    const newLesson: Lesson = lesson.changeCourseId(newCourseId);

    expect(lesson).not.toEqual(newLesson);
    expect(newLesson.getCourseId().getValue()).toBe(newCourseId.getValue());
  });
});
