/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Content } from './content';
import { Name } from './name/name';
import { Icon } from './icon/icon';
import { Text } from './text/text';

describe('Content', () => {
  it('should be defined', () => {
    const name = new Name('test');
    const icon = new Icon('save');
    const content = new Content(name, icon);

    expect(content).toBeDefined();
    expect(content.getIcon()).toEqual(icon);
    expect(content.getName()).toEqual(name);
    expect(content.getText()).toBeInstanceOf(Text);
    expect(content.getText().isEmpty()).toBeTruthy();
  });

  it('should be changeText', () => {
    const content = new Content(new Name('test'), new Icon('save'));

    const text = new Text('Hello world test of it method');
    const newContent: Content = content.changeText(text);

    expect(content).not.toEqual(newContent);
    expect(content.getText()).toBeInstanceOf(Text);
    expect(content.getText().isEmpty()).toBeTruthy();
    expect(newContent.getText().isEmpty()).toBeFalsy();
    expect(newContent.getText().getValue()).toBe(
      'Hello world test of it method',
    );
  });
});
