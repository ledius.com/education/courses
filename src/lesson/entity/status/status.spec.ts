/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Status } from './status';
import { InvalidArgumentException } from 'node-exceptions';

describe('Status', () => {
  it('should be not create status with unsupported value', () => {
    try {
      new Status('undefined' as never);
      fail('Invalid status has been created');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof InvalidArgumentException) {
        expect(e).toBeInstanceOf(InvalidArgumentException);
        expect(e.message).toBe(`"undefined" is invalid status value`);
      }
    }
  });

  it('should be create draft status', () => {
    const status: Status = Status.draft();

    expect(status.isDraft()).toBeTruthy();
    expect(status.isPublished()).toBeFalsy();
  });

  it('should be create published status', () => {
    const status: Status = Status.published();

    expect(status.isDraft()).toBeFalsy();
    expect(status.isPublished()).toBeTruthy();
  });
});
