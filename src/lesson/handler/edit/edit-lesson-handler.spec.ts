/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { EditLessonHandler } from './edit-lesson-handler';
import { LessonRepositoryInterface } from '../../repository/lesson-repository-interface.interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { Lesson } from '../../entity/lesson';
import { Id } from '../../entity/id/id';
import { v4 } from 'uuid';
import { CourseId } from '../../entity/course/course-id';
import { Name } from '../../entity/content/name/name';
import { Icon } from '../../entity/content/icon/icon';
import { Content } from '../../entity/content/content';
import { EditLessonCommand } from './edit-lesson-command';

describe('EditLessonHandler', () => {
  let handler: EditLessonHandler;
  let repository: MockProxy<LessonRepositoryInterface> &
    LessonRepositoryInterface;

  beforeEach(() => {
    repository = mock<LessonRepositoryInterface>();
    handler = new EditLessonHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be edit lesson', async () => {
    const lesson = Lesson.create(
      Id.next(),
      new CourseId(v4()),
      new Content(new Name('Same'), new Icon('icon')),
    );
    repository.getById.mockReturnValue(Promise.resolve(lesson));

    const command = new EditLessonCommand();
    command.id = lesson.getId().getValue();
    command.courseId = v4();
    command.icon = 'new-icon';
    command.name = 'hello';
    command.text = 'New text of content';

    const result: Lesson = await handler.handle(command);

    expect(repository.getById).toBeCalledWith(lesson.getId());
    expect(repository.update).toBeCalledWith(result);
    expect(result.isDraft()).toBeTruthy();
    expect(result.getContent().getText().getValue()).toBe(command.text);
    expect(result.getContent().getIcon().getValue()).toBe(command.icon);
    expect(result.getContent().getName().getValue()).toBe(command.name);
    expect(result.getCourseId().getValue()).toBe(command.courseId);
  });
});
