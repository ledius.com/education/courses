/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ListLessonQuery } from './list-lesson-query';
import { Lesson } from '../../entity/lesson';
import { LessonRepositoryInterface } from '../../repository/lesson-repository-interface.interface';
import { CourseId } from '../../entity/course/course-id';
import { FindByOptions } from '../../repository/find-by-options';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ListLessonHandler {
  public constructor(private readonly lessons: LessonRepositoryInterface) {}

  public async handle(query: ListLessonQuery): Promise<Lesson[]> {
    const options: FindByOptions = {};
    if (query.courseId) {
      options.courseId = new CourseId(query.courseId);
    }
    return await this.lessons.findBy(options);
  }
}
