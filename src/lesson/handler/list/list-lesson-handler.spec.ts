/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ListLessonHandler } from './list-lesson-handler';
import { LessonRepositoryInterface } from '../../repository/lesson-repository-interface.interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { CourseId } from '../../entity/course/course-id';
import { v4 } from 'uuid';
import { ListLessonQuery } from './list-lesson-query';
import { LessonBuilder } from '../../builder/lesson-builder';

describe('ListLessonHandler', () => {
  let handler: ListLessonHandler;
  let repository: MockProxy<LessonRepositoryInterface> &
    LessonRepositoryInterface;

  beforeEach(() => {
    repository = mock<LessonRepositoryInterface>();
    handler = new ListLessonHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be return list of lessons', async () => {
    const courseId = new CourseId(v4());
    const query = new ListLessonQuery();
    query.courseId = courseId.getValue();

    const expectedLessons = [
      new LessonBuilder().build(),
      new LessonBuilder().build(),
    ];
    repository.findBy.mockReturnValue(Promise.resolve(expectedLessons));

    const lessons = await handler.handle(query);

    expect(lessons).toEqual(expectedLessons);
    expect(repository.findBy).toBeCalledWith({ courseId });
  });
});
