/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { GetByIdLessonHandler } from './get-by-id-lesson-handler';
import { mock } from 'jest-mock-extended';
import { LessonRepositoryInterface } from '../../repository/lesson-repository-interface.interface';
import { LessonBuilder } from '../../builder/lesson-builder';
import { GetByIdLessonQuery } from './get-by-id-lesson-query';

describe('GetByIdLessonHandler', () => {
  it('should be get by id', async () => {
    const lesson = new LessonBuilder().build();
    const repository = mock<LessonRepositoryInterface>();
    repository.getById.mockReturnValue(Promise.resolve(lesson));

    const handler = new GetByIdLessonHandler(repository);

    const query = new GetByIdLessonQuery();
    query.id = lesson.getId().getValue();

    const result = await handler.handle(query);
    expect(result).toEqual(lesson);
    expect(repository.getById).toBeCalledWith(lesson.getId());
  });
});
