/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CreateLessonHandler } from './create-lesson-handler';
import { LessonRepositoryInterface } from '../../repository/lesson-repository-interface.interface';
import { mock } from 'jest-mock-extended';
import { CreateLessonCommand } from './create-lesson-command';
import { v4 } from 'uuid';
import { Lesson } from '../../entity/lesson';

describe('CreateLessonHandler', () => {
  let handler: CreateLessonHandler;
  let repository: LessonRepositoryInterface;

  beforeEach(() => {
    repository = mock<LessonRepositoryInterface>();
    handler = new CreateLessonHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be create a lesson', async () => {
    const command = new CreateLessonCommand();
    command.name = 'test';
    command.icon = 'same';
    command.courseId = v4();

    const lesson: Lesson = await handler.handle(command);

    expect(lesson.getId().getValue()).toBeDefined();
    expect(lesson.isDraft()).toBeTruthy();
    expect(lesson.getContent().getText().isEmpty()).toBeTruthy();
    expect(lesson.getContent().getName().getValue()).toBe(command.name);
    expect(lesson.getContent().getIcon().getValue()).toBe(command.icon);
    expect(lesson.getCourseId().getValue()).toBe(command.courseId);
    expect(repository.add).toBeCalledWith(lesson);
  });
});
