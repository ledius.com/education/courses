/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LessonRepositoryInterface } from './lesson-repository-interface.interface';
import { Lesson } from '../entity/lesson';
import { FindByOptions } from './find-by-options';
import { Id } from '../entity/id/id';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DomainException } from 'node-exceptions';
import { CourseId } from '@/lesson/entity/course/course-id';

export class PostgresLessonRepository implements LessonRepositoryInterface {
  public constructor(
    @InjectRepository(Lesson) private readonly repository: Repository<Lesson>,
  ) {}

  public async add(lesson: Lesson): Promise<Lesson> {
    await this.repository.save(lesson);
    return lesson;
  }

  public async findBy(options: FindByOptions): Promise<Lesson[]> {
    const params = {} as Record<string, string>;
    if (options.courseId) {
      params.courseId = options.courseId.getValue();
    }
    return await this.repository.find({
      where: params,
      order: {
        order: 'ASC',
      },
    });
  }

  public async getById(id: Id): Promise<Lesson> {
    const found = await this.repository.findOne(id.getValue());
    if (!found) {
      throw new DomainException('Lesson not found');
    }
    return found;
  }

  public async remove(lesson: Lesson): Promise<void> {
    await this.repository.remove(lesson);
  }

  public async update(lesson: Lesson): Promise<Lesson> {
    await this.repository.save(lesson);
    return lesson;
  }

  public async removeByCourseId(courseId: CourseId): Promise<void> {
    await this.repository.remove(
      await this.repository.find({
        where: {
          courseId,
        },
      }),
    );
  }
}
