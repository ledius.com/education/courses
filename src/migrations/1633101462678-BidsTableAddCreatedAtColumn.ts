import { MigrationInterface, QueryRunner } from 'typeorm';

export class BidsTableAddCreatedAtColumn1633101462678
  implements MigrationInterface
{
  public readonly name = 'BidsTableAddCreatedAtColumn1633101462678';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."bids" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."bids" DROP COLUMN "created_at"`,
    );
  }
}
