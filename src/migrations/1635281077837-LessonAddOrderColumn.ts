import { MigrationInterface, QueryRunner } from 'typeorm';

export class LessonAddOrderColumn1635281077837 implements MigrationInterface {
  public readonly name = 'LessonAddOrderColumn1635281077837';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."lessons" ADD "order" integer NOT NULL DEFAULT '0'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."lessons" DROP COLUMN "order"`,
    );
  }
}
