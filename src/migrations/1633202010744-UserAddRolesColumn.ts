import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserAddRolesColumn1633202010744 implements MigrationInterface {
  public readonly name = 'UserAddRolesColumn1633202010744';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" ADD "roles" character varying array NOT NULL DEFAULT '{}'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" DROP COLUMN "roles"`,
    );
  }
}
