import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateConsultationTable1630777224487
  implements MigrationInterface
{
  public readonly name = 'CreateConsultationTable1630777224487';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "consultations" (
        "id" uuid NOT NULL,
        "price" integer NOT NULL,
        "status" character varying NOT NULL,
        "content_name" character varying NOT NULL,
        "content_icon" character varying NOT NULL,
        "content_description" text NOT NULL,
        CONSTRAINT "PK_c5b78e9424d9bc68464f6a12103" PRIMARY KEY ("id")
      )`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS "consultations"`);
  }
}
