/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { MigrationInterface, QueryRunner } from 'typeorm';

/**
 * @class
 * @migration
 */
export class CreateAuthUserTable1620128946187 implements MigrationInterface {
  public readonly name = 'CreateAuthUserTable1620128946187';

  /**
   *
   * @param queryRunner
   */
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "auth_users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "email" character varying(100) NOT NULL, "email_confirmed" boolean NOT NULL DEFAULT false, "new_email" character varying(100), "phone" character varying(100) NOT NULL, "phone_confirmed" boolean NOT NULL DEFAULT false, "new_phone" character varying(100), "email_confirmation_token_value" character varying(100), "email_confirmation_token_expires" TIMESTAMP WITH TIME ZONE, "email_changing_token_value" character varying(100), "email_changing_token_expires" TIMESTAMP WITH TIME ZONE, "phone_confirmation_token_value" character varying(100), "phone_confirmation_token_expires" TIMESTAMP WITH TIME ZONE, "phone_changing_token_value" character varying(100), "phone_changing_token_expires" TIMESTAMP WITH TIME ZONE, CONSTRAINT "UQ_13d8b49e55a8b06bee6bbc828fb" UNIQUE ("email"), CONSTRAINT "UQ_70bb2c150614b8ca1ea1f45e744" UNIQUE ("new_email"), CONSTRAINT "UQ_f1fc077e69102d06f8724358bf2" UNIQUE ("phone"), CONSTRAINT "UQ_793f2f1d9bc5001057b5bedab90" UNIQUE ("new_phone"), CONSTRAINT "PK_c88cc8077366b470dafc2917366" PRIMARY KEY ("id"))`,
    );
  }

  /**
   *
   * @param queryRunner
   */
  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "auth_users"`);
  }
}
