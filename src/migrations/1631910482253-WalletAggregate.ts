import { MigrationInterface, QueryRunner } from 'typeorm';

export class WalletAggregate1631910482253 implements MigrationInterface {
  public readonly name = 'WalletAggregate1631910482253';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "wallets_events" (
        "id" uuid NOT NULL, 
        "aggregate_id" uuid NOT NULL, 
        "data" jsonb NOT NULL, 
        "timestamp" TIMESTAMP NOT NULL, 
        "type" character varying NOT NULL, 
        CONSTRAINT "PK_914b82ad7ae61777b47daec7d3c" PRIMARY KEY ("id")
      )`,
    );
    await queryRunner.query(
      `CREATE INDEX "wallets-events-aggregate-idx" ON "wallets_events" ("aggregate_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b82d41360b165e54afb6e06381" ON "wallets_events" ("type") `,
    );
    await queryRunner.query(
      `CREATE TABLE "wallets_aggregates" (
            "id" uuid NOT NULL,
            "owner_id" uuid NOT NULL,
            CONSTRAINT "PK_863ab87b406e33ce42d8e6341be"
            PRIMARY KEY ("id")
      )`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "wallets_aggregates"`);
    await queryRunner.query(`DROP INDEX "IDX_b82d41360b165e54afb6e06381"`);
    await queryRunner.query(`DROP INDEX "wallets-events-aggregate-idx"`);
    await queryRunner.query(`DROP TABLE "wallets_events"`);
  }
}
