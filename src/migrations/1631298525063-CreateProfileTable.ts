import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateProfileTable1631298525063 implements MigrationInterface {
  public readonly name = 'CreateProfileTable1631298525063';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE IF NOT EXISTS "profiles" (
            "id" uuid NOT NULL, 
            "user_id" uuid NOT NULL, 
            "name_first" character varying NOT NULL, 
            "name_last" character varying NOT NULL, 
            CONSTRAINT "PK_8e520eb4da7dc01d0e190447c8e" PRIMARY KEY ("id"))
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS "profiles"`);
  }
}
