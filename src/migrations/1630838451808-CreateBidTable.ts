import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateBidTable1630838451808 implements MigrationInterface {
  public readonly name = 'CreateBidTable1630838451808';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE IF NOT EXISTS "bids" (
        "id" uuid NOT NULL, 
        "user_id" uuid NOT NULL, 
        "description" text NOT NULL, 
        "price" integer NOT NULL, 
        "status" character varying NOT NULL, 
        "consultation_id" uuid NOT NULL,
         CONSTRAINT "PK_7950d066d322aab3a488ac39fe5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "bids" 
        ADD CONSTRAINT "FK_9f86f14a8da81591d5edc116a42" FOREIGN KEY ("consultation_id") REFERENCES "consultations"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE IF EXISTS "bids" DROP CONSTRAINT "FK_9f86f14a8da81591d5edc116a42"`,
    );
    await queryRunner.query(`DROP TABLE IF EXISTS "bids"`);
  }
}
