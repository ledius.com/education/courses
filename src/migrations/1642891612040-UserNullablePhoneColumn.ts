import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserNullablePhoneColumn1642891612040
  implements MigrationInterface
{
  public readonly name = 'UserNullablePhoneColumn1642891612040';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" ALTER COLUMN "phone" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "public"."auth_users" ALTER COLUMN "phone" SET NOT NULL`,
    );
  }
}
