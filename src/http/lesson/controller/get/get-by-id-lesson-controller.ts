/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { GetByIdLessonHandler } from '@/lesson/handler/get/get-by-id-lesson-handler';
import { GetByIdLessonQuery } from '@/lesson/handler/get/get-by-id-lesson-query';
import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';

@Controller({
  version: '1',
})
@ApiTags('Lessons')
export class GetByIdLessonController {
  public constructor(private readonly handler: GetByIdLessonHandler) {}

  @Get('/lesson/:id')
  @ApiOkResponse({
    type: LessonResponse,
  })
  public async get(@Param('id') id: string): Promise<LessonResponse> {
    const query = new GetByIdLessonQuery();
    query.id = id;
    const lesson = await this.handler.handle(query);
    return new LessonResponse(lesson);
  }
}
