/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { GetByIdLessonController } from './get-by-id-lesson-controller';
import { mock } from 'jest-mock-extended';
import { GetByIdLessonHandler } from '@/lesson/handler/get/get-by-id-lesson-handler';
import { LessonBuilder } from '@/lesson/builder/lesson-builder';
import { GetByIdLessonQuery } from '@/lesson/handler/get/get-by-id-lesson-query';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';

describe('GetByIdLessonController', () => {
  it('should be return result', async () => {
    const lesson = new LessonBuilder().build();
    const handler = mock<GetByIdLessonHandler>();
    handler.handle.mockReturnValue(Promise.resolve(lesson));
    const controller = new GetByIdLessonController(handler);

    const query = new GetByIdLessonQuery();
    query.id = lesson.getId().getValue();

    const response: LessonResponse = await controller.get(query.id);

    expect(response.id).toBe(query.id);
    expect(response.content).toEqual({
      name: lesson.getContent().getName().getValue(),
      icon: lesson.getContent().getIcon().getValue(),
      text: lesson.getContent().getText().getValue(),
    });
    expect(response.courseId).toBe(lesson.getCourseId().getValue());
    expect(response.isDraft).toBe(lesson.isDraft());
    expect(response.isPublished).toBe(lesson.isPublished());
  });
});
