/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ListLessonController } from './list-lesson-controller';
import { ListLessonHandler } from '@/lesson/handler/list/list-lesson-handler';
import { mock, MockProxy } from 'jest-mock-extended';
import { LessonBuilder } from '@/lesson/builder/lesson-builder';
import { ListLessonQuery } from '@/lesson/handler/list/list-lesson-query';
import { ListLessonResponse } from './list-lesson-response';

describe('ListLessonController', () => {
  let controller: ListLessonController;
  let handler: MockProxy<ListLessonHandler> & ListLessonHandler;

  beforeEach(() => {
    handler = mock<ListLessonHandler>();
    controller = new ListLessonController(handler);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be return list', async () => {
    const expectedLessons = [
      new LessonBuilder().build(),
      new LessonBuilder().build(),
    ];
    handler.handle.mockReturnValue(Promise.resolve(expectedLessons));

    const query = new ListLessonQuery();
    query.courseId = '06a864d8-853f-4162-92cf-37bab45afe5b';

    const response: ListLessonResponse = await controller.get(query);

    expect(response).toEqual(new ListLessonResponse(expectedLessons));
    expect(handler.handle).toBeCalledWith(query);
  });
});
