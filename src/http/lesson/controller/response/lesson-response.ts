/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Lesson } from '@/lesson/entity/lesson';
import { ApiProperty } from '@nestjs/swagger';
import { ApiUuid } from '@/swagger/common/api-uuid';

class LessonContentResponse {
  @ApiProperty({
    type: 'string',
  })
  public name: string;

  @ApiProperty({
    type: 'string',
  })
  public icon: string;

  @ApiProperty({
    type: 'string',
  })
  public text: string;

  public constructor(lesson: Lesson) {
    this.name = lesson.getContent().getName().getValue();
    this.icon = lesson.getContent().getIcon().getValue();
    this.text = lesson.getContent().getText().getValue();
  }
}

export class LessonResponse {
  @ApiUuid()
  public readonly id: string;

  @ApiProperty({
    type: LessonContentResponse,
  })
  public readonly content: LessonContentResponse;

  @ApiUuid()
  public readonly courseId: string;

  @ApiProperty({
    type: 'boolean',
  })
  public readonly isDraft: boolean;

  @ApiProperty({
    type: 'boolean',
  })
  public readonly isPublished: boolean;

  public constructor(lesson: Lesson) {
    this.id = lesson.getId().getValue();
    this.content = new LessonContentResponse(lesson);
    this.courseId = lesson.getCourseId().getValue();
    this.isDraft = lesson.isDraft();
    this.isPublished = lesson.isPublished();
  }
}
