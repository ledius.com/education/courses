/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { EditLessonHandler } from '@/lesson/handler/edit/edit-lesson-handler';
import { Body, Controller, Put, UseGuards } from '@nestjs/common';
import { EditLessonCommand } from '@/lesson/handler/edit/edit-lesson-command';
import { ApiBearerAuth, ApiProperty, ApiTags } from '@nestjs/swagger';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';
import { AuthGuard } from '@/jwt/guard/auth-guard';

@Controller({
  version: '1',
})
@ApiTags('Lessons')
export class EditLessonController {
  public constructor(private readonly handler: EditLessonHandler) {}

  @Put('lesson')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiProperty({
    type: LessonResponse,
  })
  public async edit(
    @Body() command: EditLessonCommand,
  ): Promise<LessonResponse> {
    const lesson = await this.handler.handle(command);
    return new LessonResponse(lesson);
  }
}
