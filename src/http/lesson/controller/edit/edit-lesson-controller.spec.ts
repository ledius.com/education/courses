/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { EditLessonController } from './edit-lesson-controller';
import { mock, MockProxy } from 'jest-mock-extended';
import { Lesson } from '@/lesson/entity/lesson';
import { LessonBuilder } from '@/lesson/builder/lesson-builder';
import { v4 } from 'uuid';
import { EditLessonHandler } from '@/lesson/handler/edit/edit-lesson-handler';
import { EditLessonCommand } from '@/lesson/handler/edit/edit-lesson-command';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';

describe('EditLessonController', () => {
  let handler: MockProxy<EditLessonHandler> & EditLessonHandler;
  let controller: EditLessonController;
  let createdLesson: Lesson;

  beforeEach(() => {
    createdLesson = new LessonBuilder().build();
    handler = mock<EditLessonHandler>();
    handler.handle.mockReturnValue(Promise.resolve(createdLesson));
    controller = new EditLessonController(handler);
  });

  it('should be create', async () => {
    const command = new EditLessonCommand();
    command.name = 'dawda';
    command.icon = 'dwaad';
    command.courseId = v4();
    command.text = 'hello';
    command.id = v4();
    const response: LessonResponse = await controller.edit(command);

    expect(response).toBeInstanceOf(LessonResponse);
    expect(response.id).toBeDefined();
    expect(response.content).toEqual({
      name: createdLesson.getContent().getName().getValue(),
      icon: createdLesson.getContent().getIcon().getValue(),
      text: createdLesson.getContent().getText().getValue(),
    });
    expect(response.isDraft).toBeTruthy();
    expect(response.isPublished).toBeFalsy();
    expect(handler.handle).toBeCalledWith(command);
  });
});
