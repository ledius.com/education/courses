/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CreateLessonController } from './create-lesson-controller';
import { mock, MockProxy } from 'jest-mock-extended';
import { CreateLessonHandler } from '@/lesson/handler/create/create-lesson-handler';
import { LessonBuilder } from '@/lesson/builder/lesson-builder';
import { CreateLessonCommand } from '@/lesson/handler/create/create-lesson-command';
import { v4 } from 'uuid';
import { Lesson } from '@/lesson/entity/lesson';
import { LessonResponse } from '@/http/lesson/controller/response/lesson-response';

describe('CreateLessonController', () => {
  let handler: MockProxy<CreateLessonHandler> & CreateLessonHandler;
  let controller: CreateLessonController;
  let createdLesson: Lesson;

  beforeEach(() => {
    createdLesson = new LessonBuilder().build();
    handler = mock<CreateLessonHandler>();
    handler.handle.mockReturnValue(Promise.resolve(createdLesson));
    controller = new CreateLessonController(handler);
  });

  it('should be create', async () => {
    const command = new CreateLessonCommand();
    command.name = 'dawda';
    command.icon = 'dwaad';
    command.courseId = v4();
    const response: LessonResponse = await controller.create(command);

    expect(response).toBeInstanceOf(LessonResponse);
    expect(response.id).toBeDefined();
    expect(response.content).toEqual({
      name: createdLesson.getContent().getName().getValue(),
      icon: createdLesson.getContent().getIcon().getValue(),
      text: createdLesson.getContent().getText().getValue(),
    });
    expect(response.isDraft).toBeTruthy();
    expect(response.isPublished).toBeFalsy();
    expect(handler.handle).toBeCalledWith(command);
  });
});
