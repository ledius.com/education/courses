/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { InitHandler } from '@/tinkoff/handler/init/init-handler';
import { InitResultResponse } from './init-result-response';
import { InitCommand } from '@/tinkoff/handler/init/init-command';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@/jwt/guard/auth-guard';

@Controller({
  version: '1',
})
@ApiTags('Payment')
export class InitController {
  public constructor(private readonly handler: InitHandler) {}

  @Post('/tinkoff/payment/init')
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: InitResultResponse,
  })
  public async init(@Body() command: InitCommand): Promise<InitResultResponse> {
    const result = await this.handler.handle(command);
    return new InitResultResponse(result);
  }
}
