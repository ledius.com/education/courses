/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { InitController } from './init-controller';
import { mock } from 'jest-mock-extended';
import { InitHandler } from '../../../tinkoff/handler/init/init-handler';
import { InitResult } from '../../../tinkoff/handler/init/init-result';
import { InitCommand } from '../../../tinkoff/handler/init/init-command';
import { InitResultResponse } from './init-result-response';

describe('InitController', () => {
  it('should be return response', async () => {
    const result = new InitResult();
    result.PaymentURL = 'hello://world.me';
    const handler = mock<InitHandler>();
    handler.handle.mockReturnValue(Promise.resolve(result));
    const controller = new InitController(handler);
    const command = new InitCommand();
    command.Amount = 1500;
    command.OrderId = 'hello';
    command.CustomerKey = '221';
    const response: InitResultResponse = await controller.init(command);

    expect(response.paymentUrl).toBe('hello://world.me');
    expect(handler.handle).toBeCalledWith(command);
  });
});
