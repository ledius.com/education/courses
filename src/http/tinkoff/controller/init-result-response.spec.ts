/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { InitResultResponse } from './init-result-response';
import { InitResult } from '../../../tinkoff/handler/init/init-result';

describe('InitResultResponse', () => {
  it('should be defined', () => {
    const result = new InitResult();
    result.PaymentURL = 'Hello';
    result.ErrorCode = 1;
    result.Status = '0';
    result.Success = true;
    result.PaymentId = 228;
    result.TerminalKey = 'dawdd';
    const response = new InitResultResponse(result);
    expect(response.paymentUrl).toBe('Hello');
  });
});
