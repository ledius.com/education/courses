/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { JoinController } from './v1/join/controller/join.controller';
import { AuthModule } from '@/auth/auth.module';
import { LoginRequestController } from './v1/login/request/login-request-controller';
import { LoginConfirmController } from './v1/login/confirm/login-confirm-controller';
import { AuthInfoController } from './v1/info/controller/auth-info-controller';
import { RefreshController } from './v1/refresh/refresh-controller';

@Module({
  imports: [AuthModule],
  controllers: [
    JoinController,
    LoginRequestController,
    LoginConfirmController,
    AuthInfoController,
    RefreshController,
  ],
})
export class AuthHttpModule {}
