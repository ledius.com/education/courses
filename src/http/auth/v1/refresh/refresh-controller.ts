/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { TokenRefreshCommand } from '@/auth/handler/refresh/token-refresh-command';
import { RefreshResponse } from './refresh-response';
import { TokenRefreshHandler } from '@/auth/handler/refresh/token-refresh-handler';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiTags } from '@nestjs/swagger';

@Controller({
  version: '1',
})
@ApiTags('Authentication')
export class RefreshController {
  public constructor(private readonly handler: TokenRefreshHandler) {}

  @Post('/auth/refresh')
  @ApiCreatedResponse({
    type: RefreshResponse,
  })
  public async refresh(
    @Body() command: TokenRefreshCommand,
  ): Promise<RefreshResponse> {
    return await RefreshResponse.of(command)(this.handler);
  }
}
