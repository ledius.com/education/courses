/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { RefreshController } from './refresh-controller';
import { TokenRefreshCommand } from '../../../../auth/handler/refresh/token-refresh-command';
import { RefreshResponse } from './refresh-response';
import { OAuthAccessTokenModel } from '../../../../auth/model/access/token/oauth-access-token-model';
import {
  bearer,
  dummyAccess,
  dummyRefresh,
  hour,
} from '../../../../auth/model/access/token/oauth-access-token-builder';
import { TokenRefreshHandler } from '../../../../auth/handler/refresh/token-refresh-handler';
import { mock, MockProxy } from 'jest-mock-extended';

describe('RefreshController', () => {
  let controller: RefreshController;
  let handler: MockProxy<TokenRefreshHandler> & TokenRefreshHandler;

  beforeEach(() => {
    handler = mock<TokenRefreshHandler>();
    controller = new RefreshController(handler);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be call handler', async () => {
    const command = new TokenRefreshCommand();
    command.refresh = 'refresh.token.string';
    const expectedToken = OAuthAccessTokenModel.access(dummyAccess)
      .expiresIn(hour)
      .refresh(dummyRefresh)
      .create(bearer);
    handler.handle.mockReturnValue(Promise.resolve(expectedToken));
    const result: RefreshResponse = await controller.refresh(command);

    expect(result.accessToken).toBe(expectedToken.getAccess());
    expect(result.refreshToken).toBe(expectedToken.getRefresh());
    expect(result.expiresIn).toBe(expectedToken.getExpiresIn());
    expect(result.type).toBe(expectedToken.getType());
  });
});
