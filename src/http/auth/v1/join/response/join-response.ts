/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { User } from '@/auth/entity/user';
import { ApiUuid } from '@/swagger/common/api-uuid';
import { ApiEmail } from '@/swagger/common/api-email';
import { ApiPhone } from '@/swagger/common/api-phone';

export class JoinResponse {
  @ApiUuid()
  public readonly id: string;

  @ApiEmail()
  public readonly email: string;

  @ApiPhone({
    nullable: true,
  })
  public readonly phone: string | null = null;

  public constructor(user: User) {
    this.id = user.getId().getValue();
    this.email = user.getEmail().getValue();
    this.phone = user.getPhone()?.getValue() ?? null;
  }
}
