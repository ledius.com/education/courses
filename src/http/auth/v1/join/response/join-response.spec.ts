/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { JoinResponse } from './join-response';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';

describe('JoinResponse', () => {
  it('should be defined', () => {
    const user = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889797'),
    );
    const response = new JoinResponse(user);
    expect(response).toBeDefined();
    expect(response.id).toBe(user.getId().getValue());
    expect(response.email).toBe(user.getEmail().getValue());
    expect(response.phone).toBe(user.getPhone()?.getValue());
  });
});
