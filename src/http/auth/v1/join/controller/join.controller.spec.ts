/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { JoinController } from './join.controller';
import { JoinHandler } from '@/auth/handler/join/join-handler';
import { JoinCommand } from '@/auth/handler/join/join-command';
import { mock, MockProxy } from 'jest-mock-extended';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';
import { JoinResponse } from '../response/join-response';

describe('JoinControllerController', () => {
  let controller: JoinController;
  let handler: MockProxy<JoinHandler> & JoinHandler;

  beforeEach(async () => {
    handler = mock<JoinHandler>();
    controller = new JoinController(handler);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be call join handler with passed body', async () => {
    const command = new JoinCommand();
    command.email = 'test@gmail.com';
    command.phone = '79609889797';

    const expectedUser = User.join(
      Id.next(),
      new Email(command.email),
      new Phone(command.phone),
    );
    handler.handle.mockReturnValue(Promise.resolve(expectedUser));

    const response = await controller.join(command);

    expect(response).toBeInstanceOf(JoinResponse);
    expect(response).toEqual({
      id: expectedUser.getId().getValue(),
      email: expectedUser.getEmail().getValue(),
      phone: expectedUser.getPhone()?.getValue(),
    });
    expect(handler.handle).toBeCalled();
    expect(handler.handle).toBeCalledWith(command);
  });
});
