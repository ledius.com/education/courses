/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Body, Controller, Post } from '@nestjs/common';
import { JoinCommand } from '@/auth/handler/join/join-command';
import { JoinHandler } from '@/auth/handler/join/join-handler';
import { JoinResponse } from '../response/join-response';
import {
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DomainExceptionResponse } from '@/http/common/response/domain-exception-response';

@Controller({
  version: '1',
})
@ApiTags('Authentication')
export class JoinController {
  public constructor(private readonly handler: JoinHandler) {}

  @Post('auth/join')
  @ApiCreatedResponse({
    type: JoinResponse,
  })
  @ApiConflictResponse({
    type: DomainExceptionResponse,
  })
  public async join(@Body() data: JoinCommand): Promise<JoinResponse> {
    const user = await this.handler.handle(data);
    return new JoinResponse(user);
  }
}
