/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { User } from '@/auth/entity/user';
import { ApiProperty } from '@nestjs/swagger';
import { Role } from '@/permission/role.enum';

export class AuthInfoResponse {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  public readonly id: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly name: string;

  @ApiProperty({
    type: 'string',
    format: 'email',
  })
  public readonly email: string;

  @ApiProperty({
    type: 'string',
    example: '79609889797',
    nullable: true,
  })
  public readonly phone: string | null = null;

  @ApiProperty({
    type: 'string',
  })
  public readonly picture: string;

  @ApiProperty()
  public readonly roles: Role[] = [];

  public constructor(user: User) {
    this.id = user.getId().getValue();
    this.email = user.getEmail().getValue();
    this.name = user.name;
    this.phone = user.getPhone()?.getValue() ?? null;
    this.picture = user.picture || '';
    this.roles = user.getRoles();
  }
}
