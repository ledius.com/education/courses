/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AuthUserInfoHandler } from '@/auth/handler/info/auth-user-info-handler';
import { AuthInfoResponse } from './auth-info-response';
import { AuthUserInfoQuery } from '@/auth/handler/info/auth-user-info-query';
import { Controller, Get, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ExceptionResponse } from '@/http/common/response/exception-response';
import { AuthGuard } from '@/jwt/guard/auth-guard';
import { AuthPayload } from '@/jwt/decorator/auth-payload';
import { TokenPayload } from '@/jwt/interfaces/token-payload.interface';

@Controller({
  version: '1',
})
@ApiTags('Authentication')
export class AuthInfoController {
  public constructor(private readonly handler: AuthUserInfoHandler) {}

  @Get('/auth/info')
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: AuthInfoResponse,
    description: 'Returns authentication information for auth client',
  })
  @ApiUnauthorizedResponse({
    type: ExceptionResponse,
    description: 'Not authorized',
  })
  public async getInfo(
    @AuthPayload() authPayload: TokenPayload,
  ): Promise<AuthInfoResponse> {
    const query = new AuthUserInfoQuery();
    query.accessToken = authPayload.token;
    const user = await this.handler.handle(query);
    return new AuthInfoResponse(user);
  }
}
