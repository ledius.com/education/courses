/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { AuthInfoController } from './auth-info-controller';
import { AuthInfoResponse } from './auth-info-response';
import { mock } from 'jest-mock-extended';
import { AuthUserInfoHandler } from '@/auth/handler/info/auth-user-info-handler';
import { AuthUserInfoQuery } from '@/auth/handler/info/auth-user-info-query';
import { User } from '@/auth/entity/user';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';

describe('AuthInfoController', () => {
  it('should be return info', async () => {
    const accessToken = 'hello.world.test';
    const expectedUser = User.join(
      Id.next(),
      new Email('test@gmail.com'),
      new Phone('79609889031'),
    );
    const handler = mock<AuthUserInfoHandler>();
    handler.handle.mockReturnValue(Promise.resolve(expectedUser));
    const controller = new AuthInfoController(handler);

    const info: AuthInfoResponse = await controller.getInfo({
      token: accessToken,
    } as never);

    const expectedQuery = new AuthUserInfoQuery();
    expectedQuery.accessToken = 'hello.world.test';
    expect(controller).toBeDefined();
    expect(handler.handle).toBeCalledWith(expectedQuery);
    expect(info.id).toBe(expectedUser.getId().getValue());
    expect(info.email).toBe(expectedUser.getEmail().getValue());
    expect(info.phone).toBe(expectedUser.getPhone()?.getValue());
  });
});
