/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginConfirmController } from './login-confirm-controller';
import { mock } from 'jest-mock-extended';
import { LoginEmailConfirmHandler } from '../../../../../auth/handler/login/email/confirm/login-email-confirm-handler';
import { LoginEmailConfirmCommand } from '../../../../../auth/handler/login/email/confirm/login-email-confirm-command';
import { OAuthAccessTokenModel } from '../../../../../auth/model/access/token/oauth-access-token-model';
import { LoginConfirmResponse } from './login-confirm-response';

describe('LoginConfirmController', () => {
  it('should be confirm login', async () => {
    const handler = mock<LoginEmailConfirmHandler>();
    const accessToken = new OAuthAccessTokenModel(
      'access',
      'refresh',
      new Date(),
      'bearer',
    );
    handler.handle.mockReturnValue(Promise.resolve(accessToken));
    const controller = new LoginConfirmController(handler);

    const command = new LoginEmailConfirmCommand();
    command.email = 'test@gmail.com';
    command.code = '1234';
    const response: LoginConfirmResponse = await controller.confirm(command);

    expect(handler.handle).toBeCalledWith(command);
    expect(response.accessToken).toBe(accessToken.getAccess());
    expect(response.refreshToken).toBe(accessToken.getRefresh());
    expect(response.type).toBe(accessToken.getType());
    expect(response.expiresIn).toEqual(accessToken.getExpiresIn());
  });
});
