/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginRequestController } from './login-request-controller';
import { mock } from 'jest-mock-extended';
import { LoginEmailRequestHandler } from '../../../../../auth/handler/login/email/request/login-email-request-handler';
import { LoginEmailRequestCommand } from '../../../../../auth/handler/login/email/request/login-email-request-command';

describe('LoginRequestController', () => {
  it('should be defined', async () => {
    const handler = mock<LoginEmailRequestHandler>();
    const command = new LoginEmailRequestCommand();
    command.email = 'test@gmail.com';
    const controller = new LoginRequestController(handler);
    await controller.requestByEmail(command);
    expect(handler.handle).toBeCalledWith(command);
  });
});
