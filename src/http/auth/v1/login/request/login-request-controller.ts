/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { LoginEmailRequestHandler } from '@/auth/handler/login/email/request/login-email-request-handler';
import { LoginEmailRequestCommand } from '@/auth/handler/login/email/request/login-email-request-command';
import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DomainExceptionResponse } from '@/http/common/response/domain-exception-response';

@Controller({
  version: '1',
})
@ApiTags('Authentication')
export class LoginRequestController {
  public constructor(private readonly handler: LoginEmailRequestHandler) {}

  @Post('/auth/email/request')
  @ApiCreatedResponse()
  @ApiConflictResponse({
    type: DomainExceptionResponse,
  })
  public async requestByEmail(
    @Body() command: LoginEmailRequestCommand,
  ): Promise<void> {
    await this.handler.handle(command);
  }
}
