/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DomainExceptionFilter } from './domain-exception-filter';
import { mock } from 'jest-mock-extended';
import { Response } from 'express';
import { DomainException } from 'node-exceptions';
import { ArgumentsHost } from '@nestjs/common';

describe('DomainExceptionFilter', () => {
  it('should be defined', async () => {
    const filter = new DomainExceptionFilter();
    const response = mock<Response>();
    response.status.mockReturnValue(response);
    const host = mock<ArgumentsHost>();
    host.switchToHttp.mockReturnValue({
      getResponse() {
        return response;
      },
    } as never);
    const exception = new DomainException('Hello world');
    await filter.catch(exception, host);

    expect(response.status).toBeCalledWith(409);
    expect(response.json).toBeCalledWith({
      message: exception.message,
    });
  });
});
