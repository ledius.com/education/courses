/**
 *  Ledius LLC
 *  Copyright (C) 24 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter as ExceptionFilterInterface,
} from '@nestjs/common';
import {
  DomainException,
  InvalidArgumentException,
  RuntimeException,
} from 'node-exceptions';
import { Response } from 'express';
import { classToPlain } from 'class-transformer';
import { ExceptionResponse } from '@/http/common/response/exception-response';
import { I18nService } from 'nestjs-i18n';
import { BaseException } from 'node-exceptions/build/src/BaseException';

@Catch(DomainException, InvalidArgumentException, RuntimeException)
export class ExceptionFilter implements ExceptionFilterInterface {
  public constructor(private readonly i18n: I18nService) {}

  private readonly I18nException = class {
    public constructor(private readonly exception: BaseException) {}

    public get path(): string {
      return `exceptions.${this.exception.message}`;
    }
  };

  public async catch(
    exception: DomainException | InvalidArgumentException | RuntimeException,
    host: ArgumentsHost,
  ): Promise<void> {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    response.status(409).json(
      classToPlain(
        new ExceptionResponse(
          await this.i18n.translate(new this.I18nException(exception).path, {
            lang: ctx.getRequest().i18nLang,
          }),
        ),
      ),
    );
  }
}
