/**
 *  Ledius LLC
 *  Copyright (C) 24 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { mock } from 'jest-mock-extended';
import { Response } from 'express';
import { ArgumentsHost } from '@nestjs/common';
import { DomainException } from 'node-exceptions';
import { ExceptionFilter } from '@/http/common/filter/exception-filter';
import { I18nService } from 'nestjs-i18n';

describe('exception filter', () => {
  it('should be defined', async () => {
    const i18nServiceMock = mock<I18nService>();
    i18nServiceMock.translate.mockReturnValue(
      Promise.resolve('translated message'),
    );
    const filter = new ExceptionFilter(i18nServiceMock);
    const response = mock<Response>();
    response.status.mockReturnValue(response);
    const host = mock<ArgumentsHost>();
    host.switchToHttp.mockReturnValue({
      getResponse() {
        return response;
      },
      getRequest() {
        return {
          i18nLang: 'ru',
        };
      },
    } as never);
    const exception = new DomainException('Hello world');
    await filter.catch(exception, host);

    expect(response.status).toBeCalledWith(409);
    expect(response.json).toBeCalledWith({
      message: 'translated message',
    });
  });
});
