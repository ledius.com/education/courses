/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { NotificationDispatcher } from '@/hook/tinkoff/dispatcher/notification-dispatcher';
import { NotificationCommand } from '@/hook/tinkoff/dispatcher/notification-command';
import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@Controller({
  version: '1',
})
@ApiTags('Tinkoff Notification')
export class TinkoffWebhookController {
  public constructor(private readonly dispatcher: NotificationDispatcher) {}

  @Post('/webhook/tinkoff/notification')
  @ApiOkResponse()
  @HttpCode(200)
  public async handle(@Body() command: NotificationCommand): Promise<'OK'> {
    console.log('INCOMING HOOK', JSON.stringify(command));
    await this.dispatcher.dispatch(command);
    return 'OK';
  }
}
