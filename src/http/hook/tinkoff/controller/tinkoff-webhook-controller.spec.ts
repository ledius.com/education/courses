/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { TinkoffWebhookController } from './tinkoff-webhook-controller';
import { mock } from 'jest-mock-extended';
import { NotificationDispatcher } from '../../../../hook/tinkoff/dispatcher/notification-dispatcher';
import { NotificationCommand } from '../../../../hook/tinkoff/dispatcher/notification-command';
import { Status } from '@/tinkoff/model/status';

describe('TinkoffWebhookController', () => {
  it('should be defined', async () => {
    const dispatcher = mock<NotificationDispatcher>();
    const controller = new TinkoffWebhookController(dispatcher);

    const command = new NotificationCommand();
    command.OrderId = '1234';
    command.Success = true;
    command.Status = Status.CONFIRMED;

    const response: 'OK' = await controller.handle(command);

    expect(response).toBe('OK');
    expect(dispatcher.dispatch).toBeCalledWith(command);
  });
});
