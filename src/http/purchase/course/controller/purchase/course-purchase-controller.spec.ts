/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseController } from './course-purchase-controller';
import { CoursePurchaseHandler } from '@/purchase/course/handler/purchase/course-purchase-handler';
import { mock, MockProxy } from 'jest-mock-extended';
import { CoursePurchaseCommand } from '@/purchase/course/handler/purchase/course-purchase-command';
import { v4 } from 'uuid';
import { CoursePurchaseResponse } from '../response/course-purchase-response';
import { CoursePurchase } from '@/purchase/course/entity/course-purchase';
import { Id } from '@/purchase/course/entity/id/id';
import { CourseId } from '@/purchase/course/entity/course/id/course-id';
import { UserId } from '@/purchase/course/entity/user/id/user-id';

describe('CoursePurchaseController', () => {
  let handler: MockProxy<CoursePurchaseHandler> & CoursePurchaseHandler;
  let controller: CoursePurchaseController;

  beforeEach(() => {
    handler = mock();
    controller = new CoursePurchaseController(handler);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be purchase', async () => {
    const command = new CoursePurchaseCommand();
    command.userId = v4();
    command.courseId = v4();

    handler.handle.mockReturnValue(
      Promise.resolve(
        CoursePurchase.purchase(
          Id.next(),
          new CourseId(command.courseId),
          new UserId(command.userId),
        ),
      ),
    );

    const response: CoursePurchaseResponse = await controller.purchase(command);

    expect(response.id).toBeDefined();
    expect(response.userId).toBe(command.userId);
    expect(response.courseId).toBe(command.courseId);
    expect(handler.handle).toBeCalledWith(command);
  });
});
