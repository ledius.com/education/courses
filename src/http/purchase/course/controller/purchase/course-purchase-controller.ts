/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseResponse } from '../response/course-purchase-response';
import { CoursePurchaseHandler } from '@/purchase/course/handler/purchase/course-purchase-handler';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { CoursePurchaseCommand } from '@/purchase/course/handler/purchase/course-purchase-command';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@/jwt/guard/auth-guard';

@Controller({
  version: '1',
})
@ApiTags('Course Purchases')
export class CoursePurchaseController {
  public constructor(private readonly handler: CoursePurchaseHandler) {}

  @Post('/purchase/course')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiCreatedResponse({
    type: CoursePurchaseResponse,
  })
  public async purchase(
    @Body() command: CoursePurchaseCommand,
  ): Promise<CoursePurchaseResponse> {
    const purchase = await this.handler.handle(command);
    return new CoursePurchaseResponse(purchase);
  }
}
