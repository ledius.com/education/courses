/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseListResponse } from './course-purchase-list-response';
import { CoursePurchaseListHandler } from '@/purchase/course/handler/list/course-purchase-list-handler';
import { Controller, Get, Query } from '@nestjs/common';
import { CoursePurchaseListQuery } from '@/purchase/course/handler/list/course-purchase-list-query';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@Controller({
  version: '1',
})
@ApiTags('Course Purchases')
export class CoursePurchaseListController {
  public constructor(private readonly handler: CoursePurchaseListHandler) {}

  @Get('/purchases/course')
  @ApiOkResponse({
    type: CoursePurchaseListResponse,
  })
  public async list(
    @Query() query: CoursePurchaseListQuery,
  ): Promise<CoursePurchaseListResponse> {
    const purchases = await this.handler.handle(query);
    return new CoursePurchaseListResponse(purchases);
  }
}
