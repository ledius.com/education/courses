/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseListController } from './course-purchase-list-controller';
import { mock, MockProxy } from 'jest-mock-extended';
import { CoursePurchaseListHandler } from '@/purchase/course/handler/list/course-purchase-list-handler';
import { CoursePurchase } from '@/purchase/course/entity/course-purchase';
import { Id } from '@/purchase/course/entity/id/id';
import { v4 } from 'uuid';
import { UserId } from '@/purchase/course/entity/user/id/user-id';
import { CourseId } from '@/purchase/course/entity/course/id/course-id';
import { CoursePurchaseListResponse } from './course-purchase-list-response';
import { CoursePurchaseListQuery } from '@/purchase/course/handler/list/course-purchase-list-query';

describe('CoursePurchaseListController', () => {
  let handler: MockProxy<CoursePurchaseListHandler> & CoursePurchaseListHandler;
  let controller: CoursePurchaseListController;

  beforeEach(() => {
    handler = mock();
    controller = new CoursePurchaseListController(handler);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be return expected', async () => {
    const expected = [
      CoursePurchase.purchase(Id.next(), new CourseId(v4()), new UserId(v4())),
      CoursePurchase.purchase(Id.next(), new CourseId(v4()), new UserId(v4())),
      CoursePurchase.purchase(Id.next(), new CourseId(v4()), new UserId(v4())),
    ];
    handler.handle.mockReturnValue(Promise.resolve(expected));

    const query = new CoursePurchaseListQuery();
    query.courseId = v4();
    query.userId = v4();

    const response: CoursePurchaseListResponse = await controller.list(query);

    expect(new CoursePurchaseListResponse(expected)).toEqual(response);
    expect(handler.handle).toBeCalledWith(query);
  });
});
