/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayController } from './course-purchase-pay-controller';
import { mock } from 'jest-mock-extended';
import { CoursePurchasePayHandler } from '../../../../../purchase/course/handler/pay/course-purchase-pay-handler';
import { CoursePurchasePayResult } from '../../../../../purchase/course/handler/pay/course-purchase-pay-result';
import { CoursePurchaseBuilder } from '../../../../../purchase/course/builder/course-purchase-builder';
import { InitResult } from '../../../../../tinkoff/handler/init/init-result';
import { CoursePurchasePayCommand } from '../../../../../purchase/course/handler/pay/course-purchase-pay-command';
import { CoursePurchasePayResponse } from './course-purchase-pay-response';

describe('CoursePurchasePayController', () => {
  it('should be defined', async () => {
    const handler = mock<CoursePurchasePayHandler>();
    const initResult = new InitResult();
    initResult.PaymentURL = 'https://hello.world';
    const purchaseResult = new CoursePurchasePayResult(
      new CoursePurchaseBuilder().build(),
      initResult,
    );
    handler.handle.mockReturnValue(Promise.resolve(purchaseResult));
    const controller = new CoursePurchasePayController(handler);

    const command = new CoursePurchasePayCommand();
    command.courseId = '6441fca0-9ef7-4148-9b6d-700e0a50df99';
    command.userId = '94cc0e70-ef29-4dce-a915-0e121de39805';

    const response: CoursePurchasePayResponse = await controller.pay(command);

    expect(response.courseId).toBe(
      purchaseResult.getCoursePurchase().getCourseId().getValue(),
    );
    expect(response.id).toBe(
      purchaseResult.getCoursePurchase().getId().getValue(),
    );
    expect(response.userId).toBe(
      purchaseResult.getCoursePurchase().getUserId().getValue(),
    );
    expect(response.paymentUrl).toBe(initResult.PaymentURL);
    expect(handler.handle).toBeCalledWith(command);
  });
});
