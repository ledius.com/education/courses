/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayHandler } from '@/purchase/course/handler/pay/course-purchase-pay-handler';
import { CoursePurchasePayCommand } from '@/purchase/course/handler/pay/course-purchase-pay-command';
import { CoursePurchasePayResponse } from './course-purchase-pay-response';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@/jwt/guard/auth-guard';

@Controller({
  version: '1',
})
@ApiTags('Course Purchases')
export class CoursePurchasePayController {
  public constructor(private readonly handler: CoursePurchasePayHandler) {}

  @Post('/purchase/course/pay')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiCreatedResponse({
    type: CoursePurchasePayResponse,
  })
  public async pay(
    @Body() command: CoursePurchasePayCommand,
  ): Promise<CoursePurchasePayResponse> {
    const result = await this.handler.handle(command);
    return new CoursePurchasePayResponse(result);
  }
}
