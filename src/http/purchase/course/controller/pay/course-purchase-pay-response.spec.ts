/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayResponse } from './course-purchase-pay-response';
import { InitResult } from '../../../../../tinkoff/handler/init/init-result';
import { CoursePurchasePayResult } from '../../../../../purchase/course/handler/pay/course-purchase-pay-result';
import { CoursePurchase } from '../../../../../purchase/course/entity/course-purchase';
import { Id } from '../../../../../purchase/course/entity/id/id';
import { v4 } from 'uuid';
import { CourseId } from '../../../../../purchase/course/entity/course/id/course-id';
import { UserId } from '../../../../../purchase/course/entity/user/id/user-id';

describe('CoursePurchasePayResponse', () => {
  it('should be defined', () => {
    const purchase = CoursePurchase.purchase(
      Id.next(),
      new CourseId(v4()),
      new UserId(v4()),
    );
    const initPaymentResult = new InitResult();
    initPaymentResult.PaymentURL = 'https://hello.world';
    const response = new CoursePurchasePayResponse(
      new CoursePurchasePayResult(purchase, initPaymentResult),
    );

    expect(response.id).toBe(purchase.getId().getValue());
    expect(response.paymentUrl).toBe(initPaymentResult.PaymentURL);
    expect(response.userId).toBe(purchase.getUserId().getValue());
    expect(response.courseId).toBe(purchase.getCourseId().getValue());
  });
});
