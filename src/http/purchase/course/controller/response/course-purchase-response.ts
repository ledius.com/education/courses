/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchase } from '@/purchase/course/entity/course-purchase';
import { ApiProperty } from '@nestjs/swagger';
import { ApiUuid } from '@/swagger/common/api-uuid';

export class CoursePurchaseResponse {
  @ApiUuid()
  public readonly id: string;

  @ApiUuid()
  public readonly courseId: string;

  @ApiUuid()
  public readonly userId: string;

  @ApiProperty({
    type: 'boolean',
  })
  public readonly isPaid: boolean;

  public constructor(purchase: CoursePurchase) {
    this.id = purchase.getId().getValue();
    this.courseId = purchase.getCourseId().getValue();
    this.userId = purchase.getUserId().getValue();
    this.isPaid = purchase.isPaid();
  }
}
