/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchase } from '../entity/course-purchase';
import { Id } from '../entity/id/id';
import { CourseId } from '../entity/course/id/course-id';
import { UserId } from '../entity/user/id/user-id';

export class CoursePurchaseBuilder {
  public build(): CoursePurchase {
    return CoursePurchase.purchase(
      new Id('7b7d6090-3e39-4de5-86d5-8febd607d15a'),
      new CourseId('6441fca0-9ef7-4148-9b6d-700e0a50df99'),
      new UserId('94cc0e70-ef29-4dce-a915-0e121de39805'),
    );
  }
}
