/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoursePurchase } from './entity/course-purchase';
import { CoursePurchaseHandler } from './handler/purchase/course-purchase-handler';
import { PostgresCoursePurchaseRepository } from './repository/postgres-course-purchase-repository';
import { CoursePurchaseRepositoryInterface } from './repository/course-purchase-repository.interface';
import { CoursePurchaseListHandler } from './handler/list/course-purchase-list-handler';
import { TinkoffModule } from '@/tinkoff/tinkoff-module';
import { CourseModule } from '@/course/course-module';
import { CoursePurchasePayHandler } from './handler/pay/course-purchase-pay-handler';
import { CoursePurchasePayConfirmHandler } from './handler/pay/confirm/course-purchase-pay-confirm-handler';
import { AuthModule } from '@/auth/auth.module';
import { LediusTokenModule } from '@/ledius-token/ledius-token.module';
import { CourseBuyLdsHandler } from '@/purchase/course/handler/buy/course-buy-lds.handler';
import { CoursePurchaseController } from '@/purchase/course/controller/course-purchase.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { ConfigModule } from '@nestjs/config';
import { LediusRecipientAccountPubKeyService } from '@/purchase/course/ledius-recipient-account-pub-key.service';

export const LEDIUS_RECIPIENT_ACCOUNT_PUB_KEY =
  'LEDIUS_RECIPIENT_ACCOUNT_PUB_KEY';

@Module({
  imports: [
    TypeOrmModule.forFeature([CoursePurchase]),
    TinkoffModule,
    CourseModule,
    AuthModule,
    LediusTokenModule,
    CqrsModule,
    ConfigModule,
  ],
  controllers: [CoursePurchaseController],
  providers: [
    CoursePurchaseHandler,
    CoursePurchaseListHandler,
    CoursePurchasePayHandler,
    CoursePurchasePayConfirmHandler,
    PostgresCoursePurchaseRepository,
    {
      provide: CoursePurchaseRepositoryInterface,
      useExisting: PostgresCoursePurchaseRepository,
    },
    CourseBuyLdsHandler,
    LediusRecipientAccountPubKeyService,
  ],
  exports: [
    CoursePurchaseHandler,
    CoursePurchaseListHandler,
    CoursePurchasePayHandler,
    CoursePurchasePayConfirmHandler,
    CourseBuyLdsHandler,
  ],
})
export class CoursePurchaseModule {}
