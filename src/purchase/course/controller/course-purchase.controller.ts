import { Body, Controller, HttpCode, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CommandBus } from '@nestjs/cqrs';
import { CoursePurchaseResponse } from '@/http/purchase/course/controller/response/course-purchase-response';
import { CourseBuyLdsCommand } from '@/purchase/course/handler/buy/course-buy-lds.command';
import { AuthPayload } from '@/jwt/decorator/auth-payload';
import { TokenPayload } from '@/jwt/interfaces/token-payload.interface';
import { AuthGuard } from '@/jwt/guard/auth-guard';

@Controller()
@ApiTags('Course Purchases')
export class CoursePurchaseController {
  public constructor(private readonly commandBus: CommandBus) {}

  @Post('buy/lds')
  @HttpCode(200)
  @ApiOkResponse({
    type: CoursePurchaseResponse,
  })
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  public async buyLds(
    @Body() command: CourseBuyLdsCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<CoursePurchaseResponse> {
    return new CoursePurchaseResponse(
      await this.commandBus.execute(
        new CourseBuyLdsCommand(tokenPayload.userId, command.courseId),
      ),
    );
  }
}
