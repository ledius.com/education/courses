import { ConfigService } from '@nestjs/config';
import { LEDIUS_RECIPIENT_ACCOUNT_PUB_KEY } from '@/purchase/course/course-purchase-module';
import { Injectable } from '@nestjs/common';

@Injectable()
export class LediusRecipientAccountPubKeyService {
  public constructor(private readonly config: ConfigService) {}

  public getLediusRecipientAccountPubKey(): string {
    const pubKey = this.config.get<string>(LEDIUS_RECIPIENT_ACCOUNT_PUB_KEY);
    if (!pubKey) {
      throw new Error(`Please specify LEDIUS_RECIPIENT_ACCOUNT_PUB_KEY env`);
    }
    return pubKey;
  }
}
