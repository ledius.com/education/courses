/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchase } from '../entity/course-purchase';
import { Id } from '../entity/id/id';
import { FindByOptions } from './find-by-options.interface';

export abstract class CoursePurchaseRepositoryInterface {
  public abstract add(coursePurchase: CoursePurchase): Promise<CoursePurchase>;
  public abstract remove(coursePurchase: CoursePurchase): Promise<void>;
  public abstract getById(id: Id): Promise<CoursePurchase>;
  public abstract findBy(options: FindByOptions): Promise<CoursePurchase[]>;
  public abstract update(
    coursePurchase: CoursePurchase,
  ): Promise<CoursePurchase>;
}
