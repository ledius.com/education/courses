/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Repository } from 'typeorm';
import { CoursePurchase } from '../entity/course-purchase';
import { InjectRepository } from '@nestjs/typeorm';
import { CoursePurchaseRepositoryInterface } from './course-purchase-repository.interface';
import { FindByOptions } from './find-by-options.interface';
import { Id } from '../entity/id/id';
import { DomainException } from 'node-exceptions';

export class PostgresCoursePurchaseRepository
  implements CoursePurchaseRepositoryInterface
{
  public constructor(
    @InjectRepository(CoursePurchase)
    private readonly repository: Repository<CoursePurchase>,
  ) {}

  public async add(coursePurchase: CoursePurchase): Promise<CoursePurchase> {
    return await this.repository.save(coursePurchase);
  }

  public async findBy(options: FindByOptions): Promise<CoursePurchase[]> {
    const params = {} as Record<string, string>;

    if (options.userId) {
      params.userId = options.userId.getValue();
    }
    if (options.courseId) {
      params.courseId = options.courseId.getValue();
    }

    return await this.repository.find(params);
  }

  public async getById(id: Id): Promise<CoursePurchase> {
    const found = await this.repository.findOne(id.getValue());
    if (!found) {
      throw new DomainException('Course Purchase not found');
    }
    return found;
  }

  public async remove(coursePurchase: CoursePurchase): Promise<void> {
    await this.repository.remove(coursePurchase);
  }

  public async update(coursePurchase: CoursePurchase): Promise<CoursePurchase> {
    return await this.repository.save(coursePurchase);
  }
}
