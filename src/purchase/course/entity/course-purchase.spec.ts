/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchase } from './course-purchase';
import { Id } from './id/id';
import { CourseId } from './course/id/course-id';
import { v4 } from 'uuid';
import { UserId } from './user/id/user-id';
import { Status } from './status';

describe('CoursePurchase', () => {
  it('should be purchase', () => {
    const id = Id.next();
    const courseId = new CourseId(v4());
    const userId = new UserId(v4());
    const purchase: CoursePurchase = CoursePurchase.purchase(
      id,
      courseId,
      userId,
    );

    expect(purchase).toBeDefined();
    expect(purchase.getCourseId()).toEqual(courseId);
    expect(purchase.getUserId()).toEqual(userId);
    expect(purchase.getId()).toEqual(id);
    expect(purchase.isNew()).toBeTruthy();
    expect(purchase.isPaid()).toBeFalsy();
    expect(purchase.getStatus()).toBe(Status.NEW);
  });

  it('should be pay', () => {
    const id = Id.next();
    const courseId = new CourseId(v4());
    const userId = new UserId(v4());
    const purchase: CoursePurchase = CoursePurchase.purchase(
      id,
      courseId,
      userId,
    );

    expect(purchase.isNew()).toBeTruthy();
    expect(purchase.isPaid()).toBeFalsy();
    expect(purchase.getStatus()).toBe(Status.NEW);

    const paidPurchase: CoursePurchase = purchase.pay();

    expect(paidPurchase).not.toEqual(purchase);
    expect(paidPurchase.isPaid()).toBeTruthy();
    expect(paidPurchase.isNew()).toBeFalsy();
    expect(paidPurchase.getStatus()).toBe(Status.PAID);
  });
});
