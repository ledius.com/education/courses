/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { v4 } from 'uuid';
import { UserIdTransformer } from './user-id-transformer';
import { UserId } from './user-id';

describe('CourseIdTransformer', () => {
  it('should be transform', () => {
    const id = new UserId(v4());
    const transformer = new UserIdTransformer();
    expect(transformer.to(id)).toBe(id.getValue());
    expect(transformer.from(id.getValue())).toEqual(id);
  });
});
