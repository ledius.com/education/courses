/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Id } from './id';
import { InvalidArgumentException } from 'node-exceptions';
import * as Joi from 'joi';

describe('Id', () => {
  it('should not be construct if value is not a valid uuidv4', () => {
    const value = '1234';
    try {
      new Id(value);
    } catch (e) {
      expect.assertions(2);
      if (e instanceof InvalidArgumentException) {
        expect(e).toBeInstanceOf(InvalidArgumentException);
        expect(e.message).toBe('Id value is not a valid uuid');
      }
    }
  });

  it('should be generate valid uuidv4', () => {
    const id = Id.next();
    Joi.assert(id.getValue(), Joi.string().uuid({ version: 'uuidv4' }));
  });
});
