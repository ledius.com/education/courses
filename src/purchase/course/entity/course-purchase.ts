/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Id } from './id/id';
import { CourseId } from './course/id/course-id';
import { UserId } from './user/id/user-id';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { IdTransformer } from './id/id-transformer';
import { CourseIdTransformer } from './course/id/course-id-transformer';
import { UserIdTransformer } from './user/id/user-id-transformer';
import { Status } from './status';

@Entity('course_purchases')
export class CoursePurchase {
  @PrimaryColumn({
    type: 'uuid',
    transformer: new IdTransformer(),
  })
  private readonly id: Id;

  @Column({
    type: 'uuid',
    transformer: new CourseIdTransformer(),
  })
  private readonly courseId: CourseId;

  @Column({
    type: 'uuid',
    transformer: new UserIdTransformer(),
  })
  private readonly userId: UserId;

  @Column({
    type: 'varchar',
    default: Status.NEW,
    nullable: false,
  })
  private readonly status: Status;

  private constructor(
    id: Id,
    courseId: CourseId,
    userId: UserId,
    status: Status,
  ) {
    this.id = id;
    this.courseId = courseId;
    this.userId = userId;
    this.status = status;
  }

  public static purchase(
    id: Id,
    courseId: CourseId,
    userId: UserId,
  ): CoursePurchase {
    return new CoursePurchase(id, courseId, userId, Status.NEW);
  }

  public getId(): Id {
    return this.id;
  }

  public getUserId(): UserId {
    return this.userId;
  }

  public getCourseId(): CourseId {
    return this.courseId;
  }

  public isNew(): boolean {
    return this.status === Status.NEW;
  }

  public isPaid(): boolean {
    return this.status === Status.PAID;
  }

  public getStatus(): Status {
    return this.status;
  }

  public pay(): CoursePurchase {
    return new CoursePurchase(this.id, this.courseId, this.userId, Status.PAID);
  }
}
