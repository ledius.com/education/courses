import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsUUID } from 'class-validator';

export class CourseBuyLdsCommand implements ICommand {
  public readonly userId!: string;

  @ApiProperty({
    type: 'string',
  })
  @IsDefined()
  @IsUUID('4')
  public readonly courseId!: string;

  public constructor(userId: string, courseId: string) {
    this.userId = userId;
    this.courseId = courseId;
  }
}
