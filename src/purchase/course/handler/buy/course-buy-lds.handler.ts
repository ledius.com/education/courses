import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CourseBuyLdsCommand } from '@/purchase/course/handler/buy/course-buy-lds.command';
import { CoursePurchase } from '@/purchase/course/entity/course-purchase';
import { Injectable } from '@nestjs/common';
import { CoursePurchaseRepositoryInterface } from '@/purchase/course/repository/course-purchase-repository.interface';
import { CourseRepositoryInterface } from '@/course/repository/course-repository.interface';
import { TransferTokenInternalService } from '@/internal/ledius-token';
import { Id as CourseIdOrigin } from '@/course/dao/entity/id/id';
import { UserId } from '@/purchase/course/entity/user/id/user-id';
import { Id } from '@/purchase/course/entity/id/id';
import { CourseId } from '@/purchase/course/entity/course/id/course-id';
import { lastValueFrom } from 'rxjs';
import { LediusRecipientAccountPubKeyService } from '@/purchase/course/ledius-recipient-account-pub-key.service';

@Injectable()
@CommandHandler(CourseBuyLdsCommand)
export class CourseBuyLdsHandler
  implements ICommandHandler<CourseBuyLdsCommand, CoursePurchase>
{
  public constructor(
    private readonly courseRepository: CourseRepositoryInterface,
    private readonly coursePurchaseRepository: CoursePurchaseRepositoryInterface,
    private readonly transferTokenInternalService: TransferTokenInternalService,
    private readonly lediusRecipientAccountPubKeyService: LediusRecipientAccountPubKeyService,
  ) {}

  public async execute(command: CourseBuyLdsCommand): Promise<CoursePurchase> {
    const course = await this.courseRepository.getById(
      new CourseIdOrigin(command.courseId),
    );
    await lastValueFrom(
      this.transferTokenInternalService.transfer({
        amount: course.getPrice().inLds().toString(),
        recipient:
          this.lediusRecipientAccountPubKeyService.getLediusRecipientAccountPubKey(),
        sender: command.userId,
      }),
    );
    const coursePurchase = CoursePurchase.purchase(
      Id.next(),
      new CourseId(course.getId().getValue()),
      new UserId(command.userId),
    ).pay();
    await this.coursePurchaseRepository.add(coursePurchase);

    return coursePurchase;
  }
}
