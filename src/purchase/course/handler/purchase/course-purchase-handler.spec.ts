/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseHandler } from './course-purchase-handler';
import { CoursePurchaseCommand } from './course-purchase-command';
import { v4 } from 'uuid';
import { CoursePurchase } from '../../entity/course-purchase';
import { CoursePurchaseRepositoryInterface } from '../../repository/course-purchase-repository.interface';
import { mock, MockProxy } from 'jest-mock-extended';

describe('CoursePurchaseHandler', () => {
  let handler: CoursePurchaseHandler;
  let repository: MockProxy<CoursePurchaseRepositoryInterface> &
    CoursePurchaseRepositoryInterface;

  beforeEach(() => {
    repository = mock();
    handler = new CoursePurchaseHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be purchase course for user', async () => {
    const command = new CoursePurchaseCommand();
    command.userId = v4();
    command.courseId = v4();

    const purchase: CoursePurchase = await handler.handle(command);

    expect(purchase.getId().getValue()).toBeDefined();
    expect(purchase.getCourseId().getValue()).toBe(command.courseId);
    expect(purchase.getUserId().getValue()).toBe(command.userId);
    expect(repository.add).toBeCalledWith(purchase);
  });
});
