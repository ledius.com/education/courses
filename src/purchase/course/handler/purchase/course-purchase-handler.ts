/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseCommand } from './course-purchase-command';
import { CoursePurchase } from '../../entity/course-purchase';
import { Id } from '../../entity/id/id';
import { CourseId } from '../../entity/course/id/course-id';
import { UserId } from '../../entity/user/id/user-id';
import { CoursePurchaseRepositoryInterface } from '../../repository/course-purchase-repository.interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CoursePurchaseHandler {
  public constructor(
    private readonly purchases: CoursePurchaseRepositoryInterface,
  ) {}

  public async handle(command: CoursePurchaseCommand): Promise<CoursePurchase> {
    const purchase = CoursePurchase.purchase(
      Id.next(),
      new CourseId(command.courseId),
      new UserId(command.userId),
    );
    await this.purchases.add(purchase);
    return purchase;
  }
}
