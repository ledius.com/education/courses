/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseListQuery } from './course-purchase-list-query';
import { CoursePurchase } from '../../entity/course-purchase';
import { CoursePurchaseRepositoryInterface } from '../../repository/course-purchase-repository.interface';
import { FindByOptions } from '../../repository/find-by-options.interface';
import { UserId } from '../../entity/user/id/user-id';
import { CourseId } from '../../entity/course/id/course-id';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CoursePurchaseListHandler {
  public constructor(
    private readonly purchases: CoursePurchaseRepositoryInterface,
  ) {}

  public async handle(
    query: CoursePurchaseListQuery,
  ): Promise<CoursePurchase[]> {
    const options = {} as FindByOptions;

    if (query.userId) {
      options.userId = new UserId(query.userId);
    }
    if (query.courseId) {
      options.courseId = new CourseId(query.courseId);
    }

    return await this.purchases.findBy(options);
  }
}
