/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseListHandler } from './course-purchase-list-handler';
import { CoursePurchase } from '../../entity/course-purchase';
import { Id } from '../../entity/id/id';
import { v4 } from 'uuid';
import { CourseId } from '../../entity/course/id/course-id';
import { UserId } from '../../entity/user/id/user-id';
import { CoursePurchaseRepositoryInterface } from '../../repository/course-purchase-repository.interface';
import { mock, MockProxy } from 'jest-mock-extended';
import { CoursePurchaseListQuery } from './course-purchase-list-query';

describe('CoursePurchaseListHandler', () => {
  let handler: CoursePurchaseListHandler;
  let repository: MockProxy<CoursePurchaseRepositoryInterface> &
    CoursePurchaseRepositoryInterface;

  beforeEach(() => {
    repository = mock();
    handler = new CoursePurchaseListHandler(repository);
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
  });

  it('should be return expected purchases', async () => {
    const expected = [
      CoursePurchase.purchase(Id.next(), new CourseId(v4()), new UserId(v4())),
      CoursePurchase.purchase(Id.next(), new CourseId(v4()), new UserId(v4())),
      CoursePurchase.purchase(Id.next(), new CourseId(v4()), new UserId(v4())),
    ];

    repository.findBy.mockReturnValue(Promise.resolve(expected));
    const query = new CoursePurchaseListQuery();
    query.courseId = v4();
    query.userId = v4();
    const list: CoursePurchase[] = await handler.handle(query);

    expect(list).toEqual(expected);
    expect(repository.findBy).toBeCalledWith({
      userId: new UserId(query.userId),
      courseId: new CourseId(query.courseId),
    });
  });
});
