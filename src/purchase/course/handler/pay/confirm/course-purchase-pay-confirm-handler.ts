/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchaseRepositoryInterface } from '../../../repository/course-purchase-repository.interface';
import { CoursePurchasePayConfirmCommand } from './course-purchase-pay-confirm-command';
import { CoursePurchase } from '../../../entity/course-purchase';
import { Id } from '../../../entity/id/id';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CoursePurchasePayConfirmHandler {
  public constructor(
    private readonly repository: CoursePurchaseRepositoryInterface,
  ) {}

  public async handle(
    command: CoursePurchasePayConfirmCommand,
  ): Promise<CoursePurchase> {
    const purchase = await this.repository.getById(new Id(command.id));
    const newPurchase = purchase.pay();
    await this.repository.update(newPurchase);
    return newPurchase;
  }
}
