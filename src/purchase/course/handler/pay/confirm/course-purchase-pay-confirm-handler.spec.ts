/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayConfirmHandler } from './course-purchase-pay-confirm-handler';
import { mock } from 'jest-mock-extended';
import { CoursePurchaseRepositoryInterface } from '../../../repository/course-purchase-repository.interface';
import { CoursePurchaseBuilder } from '../../../builder/course-purchase-builder';
import { CoursePurchasePayConfirmCommand } from './course-purchase-pay-confirm-command';
import { CoursePurchase } from '../../../entity/course-purchase';

describe('CoursePurchasePayConfirmHandler', () => {
  it('should be pay', async () => {
    const repository = mock<CoursePurchaseRepositoryInterface>();
    const expectedCoursePurchase = new CoursePurchaseBuilder().build();
    repository.getById.mockReturnValue(Promise.resolve(expectedCoursePurchase));

    const handler = new CoursePurchasePayConfirmHandler(repository);

    const command = new CoursePurchasePayConfirmCommand(
      expectedCoursePurchase.getId().getValue(),
    );
    const coursePurchase: CoursePurchase = await handler.handle(command);

    expect(repository.getById).toBeCalledWith(expectedCoursePurchase.getId());
    expect(coursePurchase.isPaid()).toBeTruthy();
    expect(repository.update).toBeCalledWith(coursePurchase);
  });
});
