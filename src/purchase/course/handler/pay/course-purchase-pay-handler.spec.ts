/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayHandler } from './course-purchase-pay-handler';
import { CoursePurchasePayCommand } from './course-purchase-pay-command';
import { v4 } from 'uuid';
import { CoursePurchasePayResult } from './course-purchase-pay-result';
import { mock } from 'jest-mock-extended';
import { InitHandler } from '@/tinkoff/handler/init/init-handler';
import { InitResult } from '@/tinkoff/handler/init/init-result';
import { CoursePurchaseRepositoryInterface } from '../../repository/course-purchase-repository.interface';
import { InitCommand } from '@/tinkoff/handler/init/init-command';
import { CourseRepositoryInterface } from '@/course/repository/course-repository.interface';
import { CourseBuilder } from '@/course/builder/course-builder';
import { Id } from '@/course/dao/entity/id/id';
import { UserRepositoryInterface } from '@/auth/repository/user-repository-interface';
import { UserBuilder } from '@/auth/builder/user-builder';
import { Id as AuthUserId } from '../../../../auth/entity/id/id';
import { OrderType } from '@/tinkoff/model/order-type';

describe('CoursePurchasePayHandler', () => {
  it('should be defined', async () => {
    const tinkoffPaymentInitHandler = mock<InitHandler>();
    tinkoffPaymentInitHandler.handle.mockReturnValue(
      Promise.resolve({
        PaymentURL: 'https://payment.loc/success',
      } as InitResult),
    );
    const repository = mock<CoursePurchaseRepositoryInterface>();
    const userRepository = mock<UserRepositoryInterface>();
    userRepository.getById.mockReturnValue(
      Promise.resolve(new UserBuilder().build()),
    );
    const courseRepository = mock<CourseRepositoryInterface>();
    courseRepository.getById.mockReturnValue(
      Promise.resolve(new CourseBuilder().build()),
    );
    const command = new CoursePurchasePayCommand();
    command.courseId = v4();
    command.userId = v4();

    const handler = new CoursePurchasePayHandler(
      repository,
      tinkoffPaymentInitHandler,
      courseRepository,
      userRepository,
    );
    const result: CoursePurchasePayResult = await handler.handle(command);

    expect(repository.add).toBeCalledWith(result.getCoursePurchase());
    expect(result.getPaymentResult().PaymentURL).toBe(
      'https://payment.loc/success',
    );
    expect(courseRepository.getById).toBeCalledWith(new Id(command.courseId));
    expect(userRepository.getById).toBeCalledWith(
      new AuthUserId(command.userId),
    );
    expect(tinkoffPaymentInitHandler.handle).toBeCalledWith({
      CustomerKey: result.getCoursePurchase().getUserId().getValue(),
      Amount: 10000,
      OrderId: result.getCoursePurchase().getId().getValue(),
      DATA: { orderType: OrderType.COURSE },
      Receipt: {
        Email: 'test@gmail.com',
        Taxation: 'usn_income_outcome',
        Items: [
          {
            Tax: 'none',
            Amount: 10000,
            Quantity: 1,
            Price: 10000,
            Name: 'name',
          },
        ],
      },
    } as InitCommand);
  });
});
