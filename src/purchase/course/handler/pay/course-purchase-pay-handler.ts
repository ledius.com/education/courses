/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayResult } from './course-purchase-pay-result';
import { CoursePurchasePayCommand } from './course-purchase-pay-command';
import { CoursePurchaseRepositoryInterface } from '../../repository/course-purchase-repository.interface';
import { InitHandler } from '../../../../tinkoff/handler/init/init-handler';
import { CoursePurchase } from '../../entity/course-purchase';
import { Id } from '../../entity/id/id';
import { CourseId } from '../../entity/course/id/course-id';
import { UserId } from '../../entity/user/id/user-id';
import { CourseRepositoryInterface } from '../../../../course/repository/course-repository.interface';
import { InitCommand } from '../../../../tinkoff/handler/init/init-command';
import { Id as ExternalCourseId } from '../../../../course/dao/entity/id/id';
import { Injectable } from '@nestjs/common';
import { UserRepositoryInterface } from '../../../../auth/repository/user-repository-interface';
import { Id as AuthUserId } from '../../../../auth/entity/id/id';
import { OrderType } from '@/tinkoff/model/order-type';

@Injectable()
export class CoursePurchasePayHandler {
  public constructor(
    private readonly repository: CoursePurchaseRepositoryInterface,
    private readonly tinkoffPaymentInitHandler: InitHandler,
    private readonly courseRepository: CourseRepositoryInterface,
    private readonly userRepository: UserRepositoryInterface,
  ) {}

  public async handle(
    command: CoursePurchasePayCommand,
  ): Promise<CoursePurchasePayResult> {
    const course = await this.courseRepository.getById(
      new ExternalCourseId(command.courseId),
    );
    const user = await this.userRepository.getById(
      new AuthUserId(command.userId),
    );
    const purchase = CoursePurchase.purchase(
      Id.next(),
      new CourseId(course.getId().getValue()),
      new UserId(command.userId),
    );
    const initCommand = new InitCommand();
    initCommand.OrderId = purchase.getId().getValue();
    initCommand.Amount = course.getPrice().inKopecks();
    initCommand.CustomerKey = purchase.getUserId().getValue();
    initCommand.DATA = { orderType: OrderType.COURSE };
    initCommand.Receipt = {
      Email: user.getEmail().getValue(),
      Taxation: 'usn_income_outcome',
      Items: [
        {
          Name: course.getContent().getName().getValue(),
          Amount: course.getPrice().inKopecks(),
          Price: course.getPrice().inKopecks(),
          Tax: 'none',
          Quantity: 1,
        },
      ],
    };
    const initResult = await this.tinkoffPaymentInitHandler.handle(initCommand);
    await this.repository.add(purchase);
    return new CoursePurchasePayResult(purchase, initResult);
  }
}
