/**
 *  Ledius LLC
 *  Copyright (C) 19 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { FixtureInterface } from '@/fixture/fixture.interface';
import { User } from '@/auth/entity/user';
import { EntityTarget } from 'typeorm';
import { Id } from '@/auth/entity/id/id';
import { Email } from '@/auth/entity/email/email';
import { Phone } from '@/common/dao/phone/phone';

export class AuthFixture implements FixtureInterface<User> {
  public getEntity(): EntityTarget<User> {
    return User;
  }

  public async getFixtures(): Promise<User[]> {
    return [
      User.join(
        new Id('3e9fba70-c2c5-40b7-bbf1-66d5b13cf4cc'),
        new Email('test@ledius.ru'),
        new Phone('79609889797'),
      ),
    ];
  }
}
