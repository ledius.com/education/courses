/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { FixtureInterface } from '../../fixture/fixture.interface';
import { Lesson } from '../../lesson/entity/lesson';
import { EntityTarget } from 'typeorm';
import { Id } from '../../lesson/entity/id/id';
import { CourseId } from '../../lesson/entity/course/course-id';
import { Content } from '../../lesson/entity/content/content';
import { Name } from '../../lesson/entity/content/name/name';
import { Icon } from '../../lesson/entity/content/icon/icon';
import { Text } from '../../lesson/entity/content/text/text';

export class LessonFixture implements FixtureInterface<Lesson> {
  public getEntity(): EntityTarget<Lesson> {
    return Lesson;
  }

  public async getFixtures(): Promise<Lesson[]> {
    const freedomFinance = [
      Lesson.create(
        new Id('1bb7f2ef-b16c-4e63-898e-ff7ab3bdd513'),
        new CourseId('20e2d382-b550-4fa2-bdb6-6771a3783e59'),
        new Content(
          new Name('Что такое деньги'),
          new Icon('money.svg'),
          new Text(
            '<h6>Что такое деньги</h6><p>Деньги самое великое изобретение человечества</p><p>Это хорошо!</p>',
          ),
        ),
      ).publish(),
      Lesson.create(
        new Id('1b757ebc-32a0-416b-8688-d9c096384890'),
        new CourseId('20e2d382-b550-4fa2-bdb6-6771a3783e59'),
        new Content(
          new Name('Что такое брокеры'),
          new Icon('broker.svg'),
          new Text('<h6>Брокеры</h6><p>Твой лучший друг</p><p>Это брокер!</p>'),
        ),
      ).publish(),
    ];

    const programming = [
      Lesson.create(
        new Id('ae1af3af-dff3-413d-b31a-4b1d30d36c32'),
        new CourseId('25968dcd-979d-4523-90a9-82e158c9054c'),
        new Content(
          new Name('Компьютеры для нуба'),
          new Icon('pc-for-noob.svg'),
          new Text(
            '<h6>Компьютер</h6><p>Вычислительная машина</p><p>С нулями и единицами!</p>',
          ),
        ),
      ).publish(),
    ];

    return [...freedomFinance, ...programming];
  }
}
