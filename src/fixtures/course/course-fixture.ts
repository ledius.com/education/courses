/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { FixtureInterface } from '../../fixture/fixture.interface';
import { Course } from '../../course/dao/entity/course';
import { EntityTarget } from 'typeorm';
import { Id } from '../../course/dao/entity/id/id';
import { Content } from '../../course/dao/entity/content/content';
import { Name } from '../../course/dao/entity/content/name/name';
import { Icon } from '../../course/dao/entity/content/icon/icon';
import { Price } from '../../course/dao/entity/price/price';

export class CourseFixture implements FixtureInterface<Course> {
  public getEntity(): EntityTarget<Course> {
    return Course;
  }

  public async getFixtures(): Promise<Course[]> {
    return [
      Course.create(
        new Id('20e2d382-b550-4fa2-bdb6-6771a3783e59'),
        new Content(new Name('Финансовая свобода'), new Icon('same.svg')),
        new Price(3499),
      ),
      Course.create(
        new Id('25968dcd-979d-4523-90a9-82e158c9054c'),
        new Content(
          new Name('Программирование для начинающих'),
          new Icon('dev.svg'),
        ),
        new Price(7299),
      ),
    ];
  }
}
