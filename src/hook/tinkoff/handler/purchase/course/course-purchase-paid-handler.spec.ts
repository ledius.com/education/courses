/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePaidHandler } from './course-purchase-paid-handler';
import { NotificationHandler } from '../../../notification.handler';
import { NotificationCommand } from '../../../dispatcher/notification-command';
import { v4 } from 'uuid';
import { CoursePurchasePayConfirmHandler } from '@/purchase/course/handler/pay/confirm/course-purchase-pay-confirm-handler';
import { mock, MockProxy } from 'jest-mock-extended';
import { CoursePurchasePayConfirmCommand } from '@/purchase/course/handler/pay/confirm/course-purchase-pay-confirm-command';
import { Status } from '@/tinkoff/model/status';
import { OrderType } from '@/tinkoff/model/order-type';

describe('CoursePurchasePayConfirmHandler', () => {
  let confirmHandler: MockProxy<CoursePurchasePayConfirmHandler>;
  let handler: NotificationHandler;

  beforeEach(() => {
    confirmHandler = mock<CoursePurchasePayConfirmHandler>();
    handler = new CoursePurchasePaidHandler(confirmHandler);
  });

  it('should be confirm', async () => {
    const command = new NotificationCommand();
    command.Status = Status.CONFIRMED;
    command.OrderId = v4();
    command.Success = true;
    command.Data = { orderType: OrderType.COURSE };
    expect(handler).toBeDefined();
    expect(handler.canHandle(command)).toBeTruthy();
    const confirmCommand = new CoursePurchasePayConfirmCommand(command.OrderId);
    await handler.handle(command);
    expect(confirmHandler.handle).toBeCalledWith(confirmCommand);
  });

  describe('can handle', () => {
    describe('success', () => {
      const successCase = {
        Status: Status.CONFIRMED,
        Success: true,
        OrderId: v4(),
        Data: { orderType: OrderType.COURSE },
      } as unknown as NotificationCommand;

      let actual: boolean;

      beforeEach(() => {
        actual = handler.canHandle(successCase);
      });

      it('should be true', () => {
        expect(actual).toBeTruthy();
      });
    });

    describe('failure', () => {
      const failureCases = [
        {
          Status: Status.NEW,
          Success: true,
          Data: { orderType: OrderType.COURSE },
        },
        {
          Status: Status.CONFIRMED,
          Success: false,
          Data: { orderType: OrderType.COURSE },
        },
        {
          Status: Status.CONFIRMED,
          Success: true,
          Data: { orderType: OrderType.BID },
        },
        {
          Status: Status.CONFIRMED,
          Success: true,
        },
      ] as unknown[] as NotificationCommand[];

      let actual: boolean[];

      beforeEach(() => {
        actual = failureCases.map(handler.canHandle);
      });

      it('should be fail all', () => {
        expect(actual).toEqual(failureCases.map(() => false));
      });
    });
  });
});
