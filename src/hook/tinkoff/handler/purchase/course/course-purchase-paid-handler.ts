/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CoursePurchasePayConfirmHandler } from '@/purchase/course/handler/pay/confirm/course-purchase-pay-confirm-handler';
import { CoursePurchasePayConfirmCommand } from '@/purchase/course/handler/pay/confirm/course-purchase-pay-confirm-command';
import { Injectable } from '@nestjs/common';
import { NotificationCommand } from '@/hook/tinkoff/dispatcher/notification-command';
import { NotificationHandler } from '@/hook/tinkoff/notification.handler';
import { Status } from '@/tinkoff/model/status';
import { OrderType } from '@/tinkoff/model/order-type';

@Injectable()
export class CoursePurchasePaidHandler implements NotificationHandler {
  public constructor(
    private readonly confirmHandler: CoursePurchasePayConfirmHandler,
  ) {}

  public canHandle(command: NotificationCommand): boolean {
    return (
      command.Status === Status.CONFIRMED &&
      command.Success &&
      command.Data?.orderType === OrderType.COURSE
    );
  }

  public async handle(command: NotificationCommand): Promise<void> {
    await this.confirmHandler.handle(
      new CoursePurchasePayConfirmCommand(command.OrderId),
    );
  }
}
