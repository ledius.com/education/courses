/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { NotificationDispatcher } from './notification-dispatcher';
import { NotificationHandler } from '../notification.handler';
import { NotificationCommand } from './notification-command';
import { mock } from 'jest-mock-extended';
import { Status } from '@/tinkoff/model/status';

describe('NotificationHandler', () => {
  it('should be call supported handler', async () => {
    const notificationHandler = mock<NotificationHandler>();
    notificationHandler.canHandle.mockReturnValue(true);

    const command = new NotificationCommand();
    command.Status = Status.CONFIRMED;

    const handler = new NotificationDispatcher(notificationHandler);
    await handler.dispatch(command);

    expect(notificationHandler.handle).toBeCalledWith(command);
    expect(notificationHandler.handle).toBeCalled();
  });

  it('should be not call unsupported handler', async () => {
    const notificationHandler = mock<NotificationHandler>();
    notificationHandler.canHandle.mockReturnValue(false);

    const command = new NotificationCommand();
    command.Status = Status.CONFIRMED;

    const handler = new NotificationDispatcher(notificationHandler);
    await handler.dispatch(command);

    expect(notificationHandler.handle).not.toBeCalledWith(command);
    expect(notificationHandler.handle).not.toBeCalled();
  });
});
