/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Status } from '@/tinkoff/model/status';
import { Type } from 'class-transformer';
import { Data } from '@/tinkoff/handler/init/data';

export class NotificationCommand {
  public TerminalKey!: string;
  public OrderId!: string;

  @Type(() => Boolean)
  public Success!: boolean;

  public Status!: Status;

  @Type(() => Number)
  public PaymentId!: number;

  public ErrorCode!: string;

  @Type(() => Number)
  public Amount!: number;

  @Type(() => Number)
  public RebillId!: number;

  @Type(() => Number)
  public CardId!: number;

  public Pan!: string;
  public ExpDate!: string;
  public Token!: string;
  public Data!: Data;
}
