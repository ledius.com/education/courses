import {
  Configuration,
  TransferTokenInternalService,
} from '@/internal/ledius-token';
import {
  HttpStatus,
  Logger,
  LoggerService,
  Module,
  OnModuleInit,
} from '@nestjs/common';
import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BaseException, ExceptionData } from '@/common/base-exception';
import { AxiosError } from 'axios';

export const LEDIUS_TOKEN_BASE_URI = 'LEDIUS_TOKEN_BASE_URI';

@Module({
  imports: [HttpModule, ConfigModule],
  providers: [
    {
      provide: TransferTokenInternalService,
      useFactory: (
        config: ConfigService,
        http: HttpService,
      ): TransferTokenInternalService => {
        return new TransferTokenInternalService(
          http as never,
          new Configuration({
            basePath: config.get<string>(LEDIUS_TOKEN_BASE_URI),
          }),
        );
      },
      inject: [ConfigService, HttpService],
    },
  ],
  exports: [TransferTokenInternalService],
})
export class LediusTokenModule implements OnModuleInit {
  private readonly logger: LoggerService = new Logger('LediusTokenInternal');
  public constructor(private readonly http: HttpService) {}

  public onModuleInit(): void {
    this.http.axiosRef.interceptors.response.use(
      undefined,
      (err: AxiosError<ExceptionData>) => {
        if (err.response?.status === HttpStatus.FORBIDDEN) {
          throw new BaseException(err.response?.data);
        }
        this.logger.error(JSON.stringify(err.response?.data));
        throw err;
      },
    );
  }
}
