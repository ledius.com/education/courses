/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Connection } from 'typeorm';
import { FixtureInterface } from './fixture.interface';
import { Injectable } from '@nestjs/common';

export type FixtureLoadable =
  | FixtureInterface
  | { new (connection: Connection): FixtureInterface };

@Injectable()
export class FixtureLoader {
  public constructor(private readonly connection: Connection) {}

  public async load(clsOrFixture: FixtureLoadable): Promise<unknown[]> {
    const fixture =
      typeof clsOrFixture === 'function'
        ? new clsOrFixture(this.connection)
        : clsOrFixture;
    const entityClass = await fixture.getEntity();
    const repository = this.connection.getRepository(entityClass);
    await repository.delete({});
    const refs = [];
    if (typeof fixture.getRefs === 'function') {
      for (const ref of fixture.getRefs()) {
        refs.push(await this.load(ref));
      }
    }
    const entities = await fixture.getFixtures(this.connection, ...refs);
    return await repository.save(entities);
  }

  public async loadMany(fixtures: FixtureLoadable[]): Promise<void> {
    for (const fixture of fixtures) {
      await this.load(fixture);
    }
  }
}
