/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { FixtureLoader } from './fixture-loader';
import { FixtureInterface } from './fixture.interface';
import { Connection, EntityTarget, Repository } from 'typeorm';
import { mock } from 'jest-mock-extended';

describe('FixtureLoader', () => {
  it('should be defined', async () => {
    class Test {}
    const fixture = new (class implements FixtureInterface<Test> {
      public getEntity(): EntityTarget<Test> {
        return Test;
      }

      public async getFixtures(): Promise<Test[]> {
        return [new Test(), new Test()];
      }
    })();
    const connection = mock<Connection>();
    const repository = mock<Repository<Test>>() as never as Repository<Test>;
    connection.getRepository.mockReturnValue(repository);
    const loader = new FixtureLoader(connection as never);
    await loader.load(fixture);

    expect(loader).toBeDefined();
    expect(connection.getRepository).toBeCalledWith(Test);
    expect(repository.delete).toBeCalled();
    expect(repository.save).toBeCalledWith(await fixture.getFixtures());
  });
});
