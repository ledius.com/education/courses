/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Module } from '@nestjs/common';
import { InitHandler } from './handler/init/init-handler';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HttpModule, HttpService } from '@nestjs/axios';

@Module({
  imports: [
    ConfigModule.forRoot(),
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => {
        return {
          baseURL: config.get<string | boolean>(
            'TINKOFF_OVERRIDE_BASE_URL',
            false,
          )
            ? config.get<string>('TINKOFF_OVERRIDE_BASE_URL')
            : 'https://securepay.tinkoff.ru',
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [
    {
      provide: InitHandler,
      useFactory: (http: HttpService, config: ConfigService): InitHandler => {
        return new InitHandler(
          http,
          config.get('TINKOFF_DEFAULT_TERMINAL_KEY') as string,
        );
      },
      inject: [HttpService, ConfigService],
    },
  ],
  exports: [InitHandler],
})
export class TinkoffModule {}
