/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { HttpService } from '@nestjs/axios';
import { InitCommand } from './init-command';
import { InitResult } from './init-result';
import { plainToClass } from 'class-transformer';
import { lastValueFrom } from 'rxjs';
import { BaseException } from 'node-exceptions/build/src/BaseException';

export class InitHandler {
  public constructor(
    private readonly http: HttpService,
    private readonly defaultTerminalKey: string,
  ) {}

  public async handle(command: InitCommand): Promise<InitResult> {
    if (!command.TerminalKey) {
      command.TerminalKey = this.defaultTerminalKey;
    }

    if (command.CustomerKey) {
      command.DATA = {
        ...command.DATA,
        userId: command.CustomerKey,
      };
    }

    const { data } = await lastValueFrom(
      this.http.post<InitResult>('/v2/Init', command),
    );
    // TODO: Refactor to axios interceptor in future
    if (!data.Success) {
      throw new BaseException(data.Message || 'Message is empty');
    }
    return plainToClass(InitResult, data);
  }
}
