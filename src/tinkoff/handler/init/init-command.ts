/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ApiProperty } from '@nestjs/swagger';
import { v4 } from 'uuid';
import { Receipt } from '../../model/receipt';
import { IsDefined, IsOptional, IsUrl, ValidateNested } from 'class-validator';
import { Data } from '@/tinkoff/handler/init/data';

export class InitCommand {
  public TerminalKey?: string;

  @ApiProperty({
    type: 'number',
    default: 350000,
    description: 'amount in kopeck',
  })
  public Amount!: number;

  @ApiProperty({
    type: 'string',
    default: v4(),
  })
  public OrderId!: string;

  @ApiProperty({
    type: 'string',
    default: v4(),
  })
  public CustomerKey!: string;

  public Receipt?: Receipt;

  @ApiProperty({
    type: Data,
  })
  @IsDefined()
  @ValidateNested()
  public DATA!: Data;

  @ApiProperty({
    nullable: true,
  })
  @IsOptional()
  @IsUrl()
  public readonly NotificationURL?: string;

  @ApiProperty({
    nullable: true,
  })
  @IsOptional()
  @IsUrl()
  public readonly SuccessURL?: string;

  @ApiProperty({
    nullable: true,
  })
  @IsOptional()
  @IsUrl()
  public readonly FailURL?: string;
}
