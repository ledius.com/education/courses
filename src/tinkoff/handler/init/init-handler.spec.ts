/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { InitHandler } from './init-handler';
import { mock, MockProxy } from 'jest-mock-extended';
import { HttpService } from '@nestjs/axios';
import { InitCommand } from './init-command';
import { InitResult } from './init-result';
import { of } from 'rxjs';
import { AxiosResponse } from 'axios';

describe('InitHandler', () => {
  describe('Success', () => {
    let http: MockProxy<HttpService> & HttpService;
    let handler: InitHandler;
    let defaultTerminalKey: string;

    beforeEach(() => {
      http = mock<HttpService>();
      http.post.mockReturnValue(
        of({
          status: 200,
          data: {
            Success: true,
            ErrorCode: '0',
            TerminalKey: 'somethingStrongHash1837',
            Status: 'NEW',
            PaymentId: '13660',
            OrderId: 'd833afc6-db10-45f5-9a73-b60209cb076d',
            Amount: 350000,
            PaymentURL: 'https://securepay.tinkoff.ru/rest/Authorize/1B63Y1',
          },
        } as AxiosResponse),
      );
      defaultTerminalKey = 'defaultTerminalKey1';
      handler = new InitHandler(http, defaultTerminalKey);
    });

    it('should be success pay', async () => {
      const command = new InitCommand();
      command.Amount = 350000;
      command.OrderId = 'd833afc6-db10-45f5-9a73-b60209cb076d';
      command.CustomerKey = 'de9356ce-c511-4e49-9378-aa33e790bac1';
      command.TerminalKey = 'somethingStrongHash1837';

      const result: InitResult = await handler.handle(command);

      expect(http.post).toBeCalledWith('/v2/Init', command);
      expect(handler).toBeDefined();
      expect(result.TerminalKey).toBe('somethingStrongHash1837');
      expect(result.Amount).toBe(350000);
      expect(result.PaymentId).toBe(13660);
      expect(result.OrderId).toBe('d833afc6-db10-45f5-9a73-b60209cb076d');
      expect(result.Success).toBeTruthy();
      expect(result.Status).toBe('NEW');
      expect(result.ErrorCode).toBe(0);
      expect(result.PaymentURL).toBeDefined();
      expect(result.PaymentURL).toBe(
        'https://securepay.tinkoff.ru/rest/Authorize/1B63Y1',
      );
    });

    it('should be use config terminal key if it not passed', async () => {
      const command = new InitCommand();
      command.Amount = 350000;
      command.OrderId = 'd833afc6-db10-45f5-9a73-b60209cb076d';
      command.CustomerKey = 'de9356ce-c511-4e49-9378-aa33e790bac1';

      await handler.handle(command);

      expect(http.post).toBeCalledWith('/v2/Init', {
        ...command,
        TerminalKey: defaultTerminalKey,
      });
    });
  });
});
