/**
 *  Ledius LLC
 *  Copyright (C) 7 Oct 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
export const BlogStruct = [
  {
    url: 'crypto',
    name: 'Почему криптовалюта растёт, а рубль падает?',
    template: 'blog/crypto.html',
    image: 'https://ledius.fra1.digitaloceanspaces.com/blog/crypto.png',
  },
  {
    url: 'ledius-fund',
    name: 'Куда стоит вкладывать?',
    template: 'blog/fund.html',
    image: 'https://ledius.fra1.digitaloceanspaces.com/blog/invest.svg',
  },
  {
    url: 'poor-vs-rich',
    name: 'Различие богатых и бедных',
    template: 'blog/finance.html',
    image:
      'https://ledius.fra1.digitaloceanspaces.com/blog/think-different.png',
  },
];
