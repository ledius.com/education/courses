/**
 *  Ledius LLC
 *  Copyright (C) 7 Oct 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Controller, Get } from '@nestjs/common';
import { FileReaderInterface } from '@/fs/file/reader/file-reader.interface';
import { BlogStruct } from '@/blog/blog';
import { join } from 'path';
import { ApiTags } from '@nestjs/swagger';

export interface Post {
  url: string;
  name: string;
  content: string;
  image: string;
}

@Controller({
  version: '1',
})
@ApiTags('Blog')
export class BlogController {
  public constructor(private readonly fileReader: FileReaderInterface) {}

  @Get('blog')
  public async index(): Promise<Post[]> {
    return await Promise.all(
      BlogStruct.map(async (post) => {
        return {
          ...post,
          content: (
            await this.fileReader.read(
              join(process.cwd(), 'template', post.template),
            )
          ).getContent(),
        } as Post;
      }),
    );
  }
}
