/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { MatchingInterface } from '../../matching.interface';
import * as Joi from 'joi';
import { InvalidArgumentException } from 'node-exceptions';

export class UuidMatch implements MatchingInterface {
  public match(value: unknown): void {
    Joi.assert(
      value,
      Joi.string().uuid({ version: 'uuidv4' }),
      new InvalidArgumentException(`${value} not a uuid`),
    );
  }
}
