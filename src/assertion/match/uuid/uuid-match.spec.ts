/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { UuidMatch } from './uuid-match';
import { MatchingInterface } from '../../matching.interface';
import { InvalidArgumentException } from 'node-exceptions';

describe('UuidMatch', () => {
  let match: MatchingInterface;

  beforeEach(() => {
    match = new UuidMatch();
  });

  it('should be defined', () => {
    expect(match).toBeDefined();
  });

  it('should be match uuid', () => {
    match.match('b714ee39-8adb-4c05-be34-259df35af961');
  });

  it('should be throw invalid string error', () => {
    try {
      match.match(1234);
      fail('Accept not a uuid value');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof InvalidArgumentException) {
        expect(e).toBeInstanceOf(InvalidArgumentException);
        expect(e.message).toBe('1234 not a uuid');
      }
    }
  });
});
