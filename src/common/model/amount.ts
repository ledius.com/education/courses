/**
 *  Ledius LLC
 *  Copyright (C) 17 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export class Amount {
  public static readonly KOPECKS = 100;

  public static from(value: number): Amount {
    return new Amount(value * Amount.KOPECKS);
  }
  public static fromKopecks(kopecks: number): Amount {
    return new Amount(kopecks);
  }

  private constructor(private readonly kopecks: number) {}

  public add(amount: Amount): Amount {
    return Amount.from(this.getValue() + amount.getValue());
  }

  public getValue(): number {
    if (this.kopecks === 0) {
      return 0;
    }
    return Math.floor(this.kopecks / Amount.KOPECKS);
  }
  public getKopecks(): number {
    return this.kopecks;
  }
}
