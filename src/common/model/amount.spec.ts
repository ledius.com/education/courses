/**
 *  Ledius LLC
 *  Copyright (C) 17 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Amount } from '@/common/model/amount';

describe('amount', () => {
  describe('fromKopecks', () => {
    let amount: Amount;

    beforeEach(() => {
      amount = Amount.fromKopecks(100);
    });

    it('should be defined', () => {
      expect(amount).toBeInstanceOf(Amount);
    });

    it('should be return right value in kopecks', () => {
      expect(amount.getKopecks()).toBe(100);
    });

    it('should be return right value', () => {
      expect(amount.getValue()).toBe(1);
    });

    describe('negative amount', () => {
      let amount: Amount;

      beforeEach(() => {
        amount = Amount.fromKopecks(-100);
      });

      it('should be return right value in kopecks', () => {
        expect(amount.getKopecks()).toBe(-100);
      });

      it('should be return right value', () => {
        expect(amount.getValue()).toBe(-1);
      });
    });
  });

  describe('from', () => {
    let amount: Amount;

    beforeEach(() => {
      amount = Amount.from(100);
    });

    it('should be defined', () => {
      expect(amount).toBeInstanceOf(Amount);
    });

    it('should be return value in kopecks 10000', () => {
      expect(amount.getKopecks()).toBe(10000);
    });

    it('should be return value', () => {
      expect(amount.getValue()).toBe(100);
    });
  });

  describe('add', () => {
    let amount1: Amount;
    let amount2: Amount;

    let sumAmount: Amount;

    beforeEach(() => {
      amount1 = Amount.from(100);
      amount2 = Amount.from(100);
      sumAmount = amount1.add(amount2);
    });

    it('should be summary of amount', () => {
      expect(sumAmount.getValue()).toBe(200);
    });

    describe('add negative amount', () => {
      let amount: Amount;
      let negativeAmount: Amount;

      let sumAmount: Amount;

      beforeEach(() => {
        amount = Amount.from(100);
        negativeAmount = Amount.from(-50);
        sumAmount = amount.add(negativeAmount);
      });

      it('should be right sum amounts', () => {
        expect(sumAmount.getValue()).toBe(50);
      });
    });
  });
});
