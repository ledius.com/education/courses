export const ErrorCodeEnum = {
  // # internal errors
  INTERNAL_ERROR: 1,

  NOT_FOUND: 2,

  PERMISSION_DENIED: 3,

  AUTH_VERIFIED_INVALID: 4,

  INSUFFICIENT_BALANCE: 7,
};
