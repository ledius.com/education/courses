import { ErrorCodeEnum } from '@/common/error-code.enum';
import { HttpException, HttpStatus } from '@nestjs/common';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export interface ExceptionData {
  statusCode: number;
  message?: string | null;
  extra?: Record<string, unknown>;
}

export class BaseException {
  private readonly data: ExceptionData;
  private readonly extra: Record<string, unknown> = {};

  public constructor(
    data: ExceptionData = { statusCode: ErrorCodeEnum.INTERNAL_ERROR },
    extra: Record<string, unknown> = {},
  ) {
    this.data = data;
    this.extra = extra;
  }

  public get toHttp(): HttpException {
    return new HttpException(
      {
        ...this.data,
        extra: this.extra,
      },
      HttpStatus.FORBIDDEN,
    );
  }

  public get message(): BaseExceptionResponse {
    return new BaseExceptionResponse({ ...this.data, extra: this.extra });
  }
}

export class BaseExceptionResponse implements ExceptionData {
  @ApiPropertyOptional()
  public readonly message: string | null = null;

  @ApiProperty({
    enum: Object.values(ErrorCodeEnum),
    type: 'number',
  })
  public readonly statusCode: number;

  @ApiProperty({})
  public readonly extra: Record<string, unknown> = {};

  public constructor(data: ExceptionData) {
    this.message = data.message || null;
    this.statusCode = data.statusCode;
    this.extra = data?.extra || {};
  }
}
