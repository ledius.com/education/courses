/**
 *  Copyright (C) 21 Jul 2021  Daniyar Yesmagulov darkrangerkz@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Assert } from '@/assertion/assert';
import { UuidMatch } from '@/assertion/match/uuid/uuid-match';
import { v4 } from 'uuid';

export class Id {
  private readonly value: string;

  public constructor(value: string) {
    new Assert(value, new UuidMatch());
    this.value = value;
  }

  public static next(): Id {
    return new Id(v4());
  }

  public getValue(): string {
    return this.value;
  }
}
