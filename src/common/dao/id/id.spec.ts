/**
 *  Copyright (C) 21 Jul 2021  Daniyar Yesmagulov darkrangerkz@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Id } from './id';

describe('Id', () => {
  it('should be return specified value', () => {
    const value = 'b714ee39-8adb-4c05-be34-259df35af961';
    const id = new Id(value);

    expect(id.getValue()).toBe(value);
  });
});
