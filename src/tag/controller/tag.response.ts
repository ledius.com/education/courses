import { Tag } from '@/tag/dao/entity/tag';
import { ApiProperty } from '@nestjs/swagger';

export class TagResponse {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly slug: string;

  public constructor(tag: Tag) {
    this.id = tag.id;
    this.name = tag.name;
    this.slug = tag.slug;
  }

  public static fromList(tags: Tag[]): TagResponse[] {
    return tags.map((tag) => new TagResponse(tag));
  }
}
