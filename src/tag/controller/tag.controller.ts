import { Body, Controller, Get, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Tag } from '@/tag/dao/entity/tag';
import { CreateTagCommand } from '@/tag/command/create-tag.command';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { GetTagsQuery } from '@/tag/command/get-tags.query';

@Controller()
@ApiTags('Tags')
export class TagController {
  public constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  @Post()
  @ApiCreatedResponse({
    type: Tag,
  })
  public async create(@Body() command: CreateTagCommand): Promise<Tag> {
    return this.commandBus.execute(command);
  }

  @Get()
  @ApiOkResponse({
    isArray: true,
    type: Tag,
  })
  public async getTags(): Promise<Tag[]> {
    return this.queryBus.execute(new GetTagsQuery());
  }
}
