import { Repository } from 'typeorm';
import { Tag } from '@/tag/dao/entity/tag';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseException } from '@/common/base-exception';
import { ErrorCodeEnum } from '@/common/error-code.enum';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TagPgRepository {
  public constructor(
    @InjectRepository(Tag)
    private readonly repository: Repository<Tag>,
  ) {}

  public async findBy(): Promise<Tag[]> {
    return this.repository.find();
  }

  public async getById(id: string): Promise<Tag> {
    const found = await this.repository.findOne({ where: { id } });
    if (!found) {
      throw new BaseException({
        statusCode: ErrorCodeEnum.NOT_FOUND,
        message: 'Tag not found',
      });
    }

    return found;
  }

  public async save(tag: Tag): Promise<void> {
    await this.repository.save(tag);
  }

  public async remove(tag: Tag): Promise<void> {
    await this.repository.remove(tag);
  }
}
