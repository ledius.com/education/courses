import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tag } from '@/tag/dao/entity/tag';
import { TagController } from '@/tag/controller/tag.controller';
import { GetTagsHandler } from '@/tag/command/get-tags.handler';
import { CreateTagHandler } from '@/tag/command/create-tag.handler';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Tag]), CqrsModule],
  controllers: [TagController],
  providers: [GetTagsHandler, CreateTagHandler, TagPgRepository],
  exports: [TagPgRepository],
})
export class TagModule {}
