import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';
import { ICommand } from '@nestjs/cqrs';

export class CreateTagCommand implements ICommand {
  @ApiProperty()
  @IsDefined()
  @IsString()
  public readonly name!: string;

  @ApiProperty()
  @IsDefined()
  @IsString()
  public readonly slug!: string;
}
