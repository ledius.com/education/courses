import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetTagsQuery } from '@/tag/command/get-tags.query';
import { Tag } from '@/tag/dao/entity/tag';
import { Injectable } from '@nestjs/common';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';

@Injectable()
@QueryHandler(GetTagsQuery)
export class GetTagsHandler implements IQueryHandler<GetTagsQuery, Tag[]> {
  public constructor(private readonly tagPgRepository: TagPgRepository) {}

  public async execute(): Promise<Tag[]> {
    return this.tagPgRepository.findBy();
  }
}
