import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateTagCommand } from '@/tag/command/create-tag.command';
import { Tag } from '@/tag/dao/entity/tag';
import { Injectable } from '@nestjs/common';
import { TagPgRepository } from '@/tag/repository/tag-pg.repository';
import { v4 } from 'uuid';

@Injectable()
@CommandHandler(CreateTagCommand)
export class CreateTagHandler
  implements ICommandHandler<CreateTagCommand, Tag>
{
  public constructor(private readonly tagPgRepository: TagPgRepository) {}

  public async execute(command: CreateTagCommand): Promise<Tag> {
    return new Tag(v4(), command.name, command.slug).saveTo(
      this.tagPgRepository,
    );
  }
}
