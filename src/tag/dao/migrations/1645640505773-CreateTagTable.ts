import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTagTable1645640505773 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "tags" (
                "id" uuid NOT NULL,
                "name" character varying NOT NULL,
                "slug" character varying NOT NULL,
                CONSTRAINT "PK_e7dc17249a1148a1970748eda99" PRIMARY KEY ("id")
            )`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "tags"`);
  }
}
