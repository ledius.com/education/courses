import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity('tags')
export class Tag {
  @ApiProperty()
  @PrimaryColumn({
    type: 'uuid',
  })
  public readonly id: string;

  @ApiProperty()
  @Column('varchar')
  public readonly name: string;

  @ApiProperty()
  @Column('varchar')
  public readonly slug: string;

  public constructor(id: string, name: string, slug: string) {
    this.id = id;
    this.name = name;
    this.slug = slug;
  }

  public async saveTo(saveable: {
    save: (tag: Tag) => Promise<void>;
  }): Promise<Tag> {
    await saveable.save(this);
    return this;
  }
}
