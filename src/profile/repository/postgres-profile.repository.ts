/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { ProfileRepository } from '@/profile/repository/profile.repository';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Profile } from '@/profile/entity/profile';
import { InjectRepository } from '@nestjs/typeorm';
import { Id } from '@/common/dao/id/id';
import { DomainException } from 'node-exceptions';

@Injectable()
export class PostgresProfileRepository implements ProfileRepository {
  public constructor(
    @InjectRepository(Profile) private readonly repository: Repository<Profile>,
  ) {}

  public async add(profile: Profile): Promise<void> {
    await this.repository.save(profile);
  }

  public async findByUserId(userId: Id): Promise<Profile | undefined> {
    return await this.repository.findOne({
      where: {
        userId,
      },
    });
  }

  public async getById(id: Id): Promise<Profile> {
    const found = await this.repository.findOne({
      where: {
        id,
      },
    });
    if (!found) {
      throw new DomainException('Profile not found');
    }
    return found;
  }

  public async getByUserId(userId: Id): Promise<Profile> {
    const found = await this.repository.findOne({
      where: {
        userId,
      },
    });
    if (!found) {
      throw new DomainException('Profile not found');
    }
    return found;
  }

  public async remove(profile: Profile): Promise<void> {
    await this.repository.remove(profile);
  }

  public async update(profile: Profile): Promise<void> {
    await this.repository.save(profile);
  }
}
