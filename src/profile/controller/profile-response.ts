/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Profile } from '@/profile/entity/profile';
import { ApiProperty } from '@nestjs/swagger';

export class ProfileResponse {
  @ApiProperty({
    type: 'string',
  })
  public readonly id: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly userId: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly firstName: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly lastName: string;

  public constructor(profile: Profile) {
    this.id = profile.id.getValue();
    this.userId = profile.userId.getValue();
    this.firstName = profile.name.first;
    this.lastName = profile.name.last;
  }
}
