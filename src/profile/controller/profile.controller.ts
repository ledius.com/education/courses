/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateProfileCommand } from '@/profile/handler/create/create-profile-command';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ProfileResponse } from '@/profile/controller/profile-response';
import { GetByUserProfileQuery } from '@/profile/handler/get-by-user/get-by-user-profile-query';
import { EditProfileCommand } from '@/profile/handler/edit/edit-profile-command';
import { AuthGuard } from '@/jwt/guard/auth-guard';

@Controller({
  version: '1',
  path: 'profiles',
})
@ApiTags('Profiles')
@UseGuards(AuthGuard)
export class ProfileController {
  public constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post()
  @ApiCreatedResponse({ type: ProfileResponse })
  @ApiBearerAuth()
  public async create(
    @Body() command: CreateProfileCommand,
  ): Promise<ProfileResponse> {
    return new ProfileResponse(await this.commandBus.execute(command));
  }

  @Get('user/:userId')
  @ApiBearerAuth()
  @ApiOkResponse({
    type: ProfileResponse,
  })
  public async findByUserId(
    @Param('userId') userId: string,
  ): Promise<ProfileResponse> {
    return new ProfileResponse(
      await this.queryBus.execute(new GetByUserProfileQuery(userId)),
    );
  }

  @Put()
  @ApiBearerAuth()
  @ApiOkResponse({
    type: ProfileResponse,
  })
  public async edit(
    @Body() command: EditProfileCommand,
  ): Promise<ProfileResponse> {
    return new ProfileResponse(await this.commandBus.execute(command));
  }
}
