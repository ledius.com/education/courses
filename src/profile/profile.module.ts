/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Module } from '@nestjs/common';
import { ProfileController } from '@/profile/controller/profile.controller';
import { CreateProfileHandler } from '@/profile/handler/create/create-profile.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Profile } from '@/profile/entity/profile';
import { EditProfileHandler } from '@/profile/handler/edit/edit-profile.handler';
import { GetByUserProfileHandler } from '@/profile/handler/get-by-user/get-by-user-profile.handler';
import { PostgresProfileRepository } from '@/profile/repository/postgres-profile.repository';
import { ProfileRepository } from '@/profile/repository/profile.repository';

@Module({
  imports: [CqrsModule, TypeOrmModule.forFeature([Profile])],
  controllers: [ProfileController],
  providers: [
    PostgresProfileRepository,
    {
      provide: ProfileRepository,
      useExisting: PostgresProfileRepository,
    },
    CreateProfileHandler,
    EditProfileHandler,
    GetByUserProfileHandler,
  ],
  exports: [ProfileRepository],
})
export class ProfileModule {}
