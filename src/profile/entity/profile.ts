/**
 *  Ledius LLC
 *  Copyright (C) 10 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Id } from '@/common/dao/id/id';
import { Name } from '@/profile/entity/name/name';
import { Column, Entity } from 'typeorm';
import { PrimaryIdColumn } from '@/common/dao/id/primary-id-column';
import { IdColumn } from '@/common/dao/id/id-column';
import { ProfileRepository } from '../repository/profile.repository';
import { DomainException } from 'node-exceptions';

@Entity('profiles')
export class Profile {
  @PrimaryIdColumn()
  public readonly id: Id;

  @IdColumn()
  public readonly userId: Id;

  @Column(() => Name)
  public readonly name: Name;

  private constructor(id: Id, userId: Id, name: Name) {
    this.id = id;
    this.userId = userId;
    this.name = name;
  }

  public static create(id: Id, userId: Id, name: Name): Profile {
    return new Profile(id, userId, name);
  }

  public changeName(name: Name): Profile {
    return new Profile(this.id, this.userId, name);
  }

  public async saveTo(repository: ProfileRepository): Promise<Profile> {
    const found = await repository.findByUserId(this.userId);
    if (found) {
      throw new DomainException('Profile already exists');
    }
    await repository.add(this);
    return this;
  }

  public async update(repository: ProfileRepository): Promise<Profile> {
    await repository.update(this);
    return this;
  }

  public static from(repository: ProfileRepository): {
    withId(id: Id): Promise<Profile>;
  } {
    return {
      async withId(id: Id): Promise<Profile> {
        return repository.getById(id);
      },
    };
  }
}
