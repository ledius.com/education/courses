/**
 *  Ledius LLC
 *  Copyright (C) 10 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Id } from '@/common/dao/id/id';
import { Name } from '@/profile/entity/name/name';
import { Profile } from '@/profile/entity/profile';

describe('profile', () => {
  const expectedId: Id = Id.next();
  const expectedUserId: Id = Id.next();
  const expectedName: Name = new Name('John', 'Doe');
  let profile: Profile;

  beforeEach(() => {
    profile = Profile.create(expectedId, expectedUserId, expectedName);
  });

  it('should be match id', () => {
    expect(profile.id).toEqual(expectedId);
  });

  it('should be match user id', () => {
    expect(profile.userId).toEqual(expectedUserId);
  });

  it('should be match name', () => {
    expect(profile.name).toEqual(expectedName);
  });
});
