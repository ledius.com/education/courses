/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { mock, MockProxy } from 'jest-mock-extended';
import { ProfileRepository } from '@/profile/repository/profile.repository';
import { GetByUserProfileHandler } from '@/profile/handler/get-by-user/get-by-user-profile.handler';
import { Profile } from '@/profile/entity/profile';
import { Name } from '@/profile/entity/name/name';
import { Id } from '@/common/dao/id/id';
import { GetByUserProfileQuery } from '@/profile/handler/get-by-user/get-by-user-profile-query';

describe('find by user profile handler', () => {
  let handler: GetByUserProfileHandler;
  let repository: MockProxy<ProfileRepository> & ProfileRepository;

  beforeEach(() => {
    repository = mock();
    handler = new GetByUserProfileHandler(repository);
  });

  describe('execute', () => {
    const expectedProfile: Profile = Profile.create(
      Id.next(),
      Id.next(),
      new Name('first', 'last'),
    );
    let actual: Profile;

    beforeEach(async () => {
      repository.getByUserId.mockReturnValue(Promise.resolve(expectedProfile));
      actual = await handler.execute(
        new GetByUserProfileQuery(expectedProfile.userId.getValue()),
      );
    });

    it('should be match with expected', () => {
      expect(actual).toEqual(expectedProfile);
    });

    it('should be call with id', () => {
      expect(repository.getByUserId).toBeCalledWith(
        new Id(expectedProfile.userId.getValue()),
      );
    });
  });
});
