/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { EditProfileHandler } from '@/profile/handler/edit/edit-profile.handler';
import { mock, MockProxy } from 'jest-mock-extended';
import { ProfileRepository } from '@/profile/repository/profile.repository';
import { Profile } from '@/profile/entity/profile';
import { Id } from '@/common/dao/id/id';
import { Name } from '@/profile/entity/name/name';
import { EditProfileCommand } from '@/profile/handler/edit/edit-profile-command';

describe('edit profile handler', () => {
  let handler: EditProfileHandler;
  let repository: MockProxy<ProfileRepository> & ProfileRepository;

  beforeEach(() => {
    repository = mock();
    handler = new EditProfileHandler(repository);
  });

  describe('execute', () => {
    const foundProfile: Profile = Profile.create(
      Id.next(),
      Id.next(),
      new Name('a', 'b'),
    );
    let actual: Profile;
    let command: EditProfileCommand;

    beforeEach(async () => {
      repository.getById.mockReturnValue(Promise.resolve(foundProfile));
      command = new EditProfileCommand(
        foundProfile.id.getValue(),
        'hello',
        'world',
      );
      actual = await handler.execute(command);
    });

    it('should be have hello first name', () => {
      expect(actual.name.first).toBe(command.firstName);
    });

    it('should be have world last name', () => {
      expect(actual.name.last).toBe(command.lastName);
    });

    it('should be update with actual', () => {
      expect(repository.update).toBeCalledWith(actual);
    });
  });
});
