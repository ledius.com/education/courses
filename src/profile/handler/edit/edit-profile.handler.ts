/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EditProfileCommand } from '@/profile/handler/edit/edit-profile-command';
import { Profile } from '@/profile/entity/profile';
import { ProfileRepository } from '@/profile/repository/profile.repository';
import { Id } from '@/common/dao/id/id';
import { Name } from '@/profile/entity/name/name';

@CommandHandler(EditProfileCommand)
export class EditProfileHandler
  implements ICommandHandler<EditProfileCommand, Profile>
{
  public constructor(private readonly repository: ProfileRepository) {}

  public async execute(command: EditProfileCommand): Promise<Profile> {
    return Profile.from(this.repository)
      .withId(new Id(command.id))
      .then((profile) =>
        profile.changeName(new Name(command.firstName, command.lastName)),
      )
      .then((profile) => profile.update(this.repository));
  }
}
