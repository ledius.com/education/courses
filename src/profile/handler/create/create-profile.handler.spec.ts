/**
 *  Ledius LLC
 *  Copyright (C) 11 Sep 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CreateProfileHandler } from '@/profile/handler/create/create-profile.handler';
import { ProfileRepository } from '@/profile/repository/profile.repository';
import { mock, MockProxy } from 'jest-mock-extended';
import { CreateProfileCommand } from '@/profile/handler/create/create-profile-command';
import { v4 } from 'uuid';
import { Profile } from '@/profile/entity/profile';
import { DomainException } from 'node-exceptions';

describe('create profile handler', () => {
  let handler: CreateProfileHandler;
  let repository: MockProxy<ProfileRepository> & ProfileRepository;

  beforeEach(() => {
    repository = mock();
    handler = new CreateProfileHandler(repository);
  });

  describe('execute', () => {
    let command: CreateProfileCommand;
    let actual: Profile;

    beforeEach(async () => {
      command = new CreateProfileCommand(v4(), 'Hello', 'World');
      actual = await handler.execute(command);
    });

    it('should be defined', () => {
      expect(actual).toBeDefined();
    });

    it('should be match user id', () => {
      expect(actual.userId.getValue()).toBe(command.userId);
    });

    it('should be match first name', () => {
      expect(actual.name.first).toBe(command.firstName);
    });

    it('should be match last name', () => {
      expect(actual.name.last).toBe(command.lastName);
    });

    it('should be call add to repository', () => {
      expect(repository.add).toBeCalledWith(actual);
    });
  });

  describe('profile already exists', () => {
    let command: CreateProfileCommand;

    beforeEach(async () => {
      command = new CreateProfileCommand(v4(), 'Hello', 'World');
      repository.findByUserId.mockReturnValue(Promise.resolve(true as never));
    });

    it('should be throw domain exception with profile already exists', () => {
      expect(handler.execute(command)).rejects.toThrow(
        new DomainException('Profile already exists'),
      );
    });
  });
});
