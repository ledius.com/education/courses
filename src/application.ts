/**
 *  Ledius LLC
 *  Copyright (C) 11 Oct 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { NestFactory } from '@nestjs/core';
import { AppModule } from '@/app.module';
import { INestApplication } from '@nestjs/common';

export class Application {
  public constructor(
    private readonly port: number,
    private readonly plugins: ApplicationPlugin[] = [],
  ) {}

  public async run(): Promise<void> {
    const app = await NestFactory.create(AppModule, {
      cors: {
        allowedHeaders: ['Content-Type', 'X-Secret-Code'],
        methods: ['POST', 'GET'],
        optionsSuccessStatus: 200,
      },
    });

    for (const plugin of this.plugins) {
      await plugin.apply(app);
    }

    await app.listen(this.port);
  }
}

export interface ApplicationPlugin {
  apply(app: INestApplication): Promise<void>;
}
