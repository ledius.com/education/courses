/**
 *  Ledius LLC
 *  Copyright (C) 11 Oct 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { ApplicationPlugin } from '@/application';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';
import { FileReaderInterface } from '@/fs/file/reader/file-reader.interface';
import * as path from 'path';

export class SwaggerPlugin implements ApplicationPlugin {
  public async getVersion(app: INestApplication): Promise<string> {
    const reader = app.get(FileReaderInterface);
    const { version } = JSON.parse(
      (
        await reader.read(path.join(process.cwd(), 'package.json'))
      ).getContent(),
    );
    return version;
  }

  public async apply(app: INestApplication): Promise<void> {
    const config = new DocumentBuilder()
      .setTitle('Ledius Courses')
      .setDescription('Ledius Courses SaaS API')
      .setVersion(await this.getVersion(app))
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('/api/docs', app, document);
  }
}
