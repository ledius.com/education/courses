/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { TemplateEngineInterface } from './template-engine.interface';
import { Template } from '../mailer/template';
import { FileReaderInterface } from '../fs/file/reader/file-reader.interface';
import { compile } from 'handlebars';
import * as path from 'path';

/**
 * @class
 * @implements TemplateEngineInterface
 */
export class HandlebarsTemplateEngine implements TemplateEngineInterface {
  public constructor(
    private readonly reader: FileReaderInterface,
    private readonly baseDir: string,
  ) {}

  public async render(
    template: Template,
    props: Record<string, string>,
  ): Promise<string> {
    const file = await this.reader.read(
      path.join(this.baseDir, template.getValue()),
    );
    return compile(file.getContent())(props);
  }
}
