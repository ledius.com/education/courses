/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { HandlebarsTemplateEngine } from './handlebars-template-engine';
import { template } from '../mailer/template';
import { FileReaderInterface } from '../fs/file/reader/file-reader.interface';
import { mock } from 'jest-mock-extended';
import { File } from '../fs/file/reader/file';

describe('HandlebarsTemplateEngine', () => {
  it('should be defined', async () => {
    const fileReader = mock<FileReaderInterface>();
    fileReader.read.mockReturnValue(
      Promise.resolve(
        new File('app/test/index.html', '<p>Hello, {{ hello }}</p>'),
      ),
    );

    const templateEngine = new HandlebarsTemplateEngine(fileReader, '/');

    const result = await templateEngine.render(
      template('app/test/index.html'),
      {
        hello: 'world',
      },
    );

    expect(result).toBe('<p>Hello, world</p>');
    expect(fileReader.read).toBeCalledWith('/app/test/index.html');
  });
});
