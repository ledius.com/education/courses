import { Module } from '@nestjs/common';
import { TemplateEngineInterface } from '@/template/template-engine.interface';
import * as path from 'path';
import { HandlebarsTemplateEngine } from '@/template/handlebars-template-engine';
import { FileReader } from '@/fs/file/reader/file-reader';

@Module({
  providers: [
    {
      provide: TemplateEngineInterface,
      useFactory: (): TemplateEngineInterface => {
        const templateDir = path.join(__dirname, '..', '..', 'template');
        return new HandlebarsTemplateEngine(new FileReader(), templateDir);
      },
    },
  ],
  exports: [TemplateEngineInterface],
})
export class TemplateEngineModule {}
