/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { AuthHttpModule } from './http/auth/auth-http.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  APP_FILTER,
  APP_INTERCEPTOR,
  APP_PIPE,
  RouterModule,
} from '@nestjs/core';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CourseModule } from './course/course-module';
import { LessonHttpModule } from './http/lesson/lesson-http-module';
import { LessonModule } from './lesson/lesson-module';
import { CoursePurchaseModule } from './purchase/course/course-purchase-module';
import { CoursePurchaseHttpModule } from './http/purchase/course/course-purchase-http-module';
import { FixtureModule } from './fixture/fixture-module';
import { CliModule } from './cli/cli-module';
import { TinkoffHttpModule } from './http/tinkoff/tinkoff-http-module';
import { TinkoffHookHttpModule } from './http/hook/tinkoff/tinkoff-hook-http-module';
import { ProfileModule } from '@/profile/profile.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { BullModule } from '@nestjs/bull';
import { ExceptionFilter } from '@/http/common/filter/exception-filter';
import {
  AcceptLanguageResolver,
  HeaderResolver,
  I18nJsonParser,
  I18nModule,
} from 'nestjs-i18n';
import * as path from 'path';
import { BlogModule } from '@/blog/blog.module';
import { JsonWebTokenModule } from '@/jwt/json-web-token.module';
import { BaseExceptionFilter } from '@/common/filters/base-exception.filter';
import { MailerModule } from '@/mailer/mailer-module';
import { TagModule } from '@/tag/tag.module';
import { TinkoffModule } from '@/tinkoff/tinkoff-module';
import { InternalModule } from '@/internal/internal.module';
import { LoggerModule } from '@ledius/logger';
import { RequestContextModule } from '@ledius/request-context';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => {
        return {
          type: 'postgres',
          username: config.get('POSTGRES_USER'),
          password: config.get('POSTGRES_PASS'),
          host: config.get('POSTGRES_HOST'),
          port: Number(config.get('POSTGRES_PORT')),
          database: config.get('POSTGRES_DB'),
          autoLoadEntities: true,
          keepConnectionAlive: true,
          namingStrategy: new SnakeNamingStrategy(),
          ssl:
            config.get('POSTGRES_USE_SSL') === '1'
              ? {
                  rejectUnauthorized: false,
                  ca: config.get('POSTGRES_SSL_CA'),
                }
              : false,
        };
      },
      inject: [ConfigService],
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        redis: {
          host: new URL(config.get('REDIS_URI', '')).hostname,
          port: Number(new URL(config.get('REDIS_URI', '')).port),
          db: Number(new URL(config.get('REDIS_URI', '')).pathname) || 1,
          password: config.get('REDIS_PASSWORD'),
        },
      }),
    }),
    I18nModule.forRoot({
      fallbackLanguage: 'ru',
      parserOptions: {
        path: path.join(__dirname, 'i18n'),
      },
      parser: I18nJsonParser,
      resolvers: [AcceptLanguageResolver, new HeaderResolver(['X-Lang'])],
    }),
    EventEmitterModule.forRoot(),
    AuthModule,
    AuthHttpModule,
    BlogModule,
    CliModule,
    CourseModule,
    LessonModule,
    LessonHttpModule,
    CoursePurchaseModule,
    CoursePurchaseHttpModule,
    FixtureModule,
    TinkoffHttpModule,
    TinkoffHookHttpModule,
    ProfileModule,
    TagModule,
    JsonWebTokenModule.forRoot({}),
    InternalModule,
    RequestContextModule,
    LoggerModule,
    RouterModule.register([
      {
        module: AuthModule,
        path: '/api',
      },
      {
        module: CoursePurchaseModule,
        path: '/api/v1/purchase/course',
      },
      {
        module: MailerModule,
        path: '/api/v1/mailer',
      },
      {
        module: CourseModule,
        path: '/api/v1/courses',
      },
      {
        module: TagModule,
        path: '/api/v1/tags',
      },
      {
        module: AuthHttpModule,
        path: '/api/v1',
      },
      {
        module: AuthModule,
        path: '/api/v1',
      },
      {
        module: LessonModule,
        path: '/api/v1',
      },
      {
        module: LessonHttpModule,
        path: '/api/v1',
      },
      {
        module: CoursePurchaseHttpModule,
        path: '/api/v1',
      },
      {
        module: TinkoffHttpModule,
        path: '/api/v1',
      },
      {
        module: TinkoffModule,
        path: '/api/v1',
      },
      {
        module: TinkoffHookHttpModule,
        path: '/api/v1',
      },
      {
        module: InternalModule,
        path: '/internal',
      },
    ]),
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ transform: true }),
    },
    {
      provide: APP_FILTER,
      useClass: ExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: BaseExceptionFilter,
    },
  ],
})
export class AppModule {}
