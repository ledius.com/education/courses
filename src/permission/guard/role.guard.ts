import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ROLES_METADATA_KEY } from '@/permission/constants';
import { Role } from '@/permission/role.enum';
import { TokenPayload } from '@/jwt/interfaces/token-payload.interface';
import { BaseException } from '@/common/base-exception';
import { ErrorCodeEnum } from '@/common/error-code.enum';

@Injectable()
export class RoleGuard implements CanActivate {
  public constructor(private readonly reflector: Reflector) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<Role[]>(
      ROLES_METADATA_KEY,
      context.getHandler(),
    );
    if (!roles) {
      return true;
    }

    this.matchRoles(roles, context.switchToHttp().getRequest().tokenPayload);

    return true;
  }

  private matchRoles(requiredRoles: Role[], user: TokenPayload): void {
    const userRoles = user.roles || [];
    const userHasAccess = requiredRoles.find((role) => {
      return Boolean(userRoles.find((userRole) => userRole === role));
    });

    if (!userHasAccess) {
      throw new BaseException(
        {
          statusCode: ErrorCodeEnum.PERMISSION_DENIED,
          message: `Permission denied`,
        },
        {
          requiredRoles,
          userRoles,
        },
      );
    }
  }
}
