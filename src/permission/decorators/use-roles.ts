import { Role } from '@/permission/role.enum';
import { CustomDecorator, SetMetadata } from '@nestjs/common';
import { ROLES_METADATA_KEY } from '@/permission/constants';

export const UseRoles = (...roles: Role[]): CustomDecorator<string> =>
  SetMetadata(ROLES_METADATA_KEY, roles);
