/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { FileReader } from './file-reader';
import * as fs from 'fs';
import { RuntimeException } from 'node-exceptions';

describe('FileReader', () => {
  let reader: FileReader;

  beforeEach(() => {
    reader = new FileReader();
    fs.writeFileSync('./read.test.txt', 'hello world');
  });

  afterEach(() => {
    fs.unlinkSync('./read.test.txt');
  });

  it('should be read content from file', async () => {
    const file = await reader.read('./read.test.txt');
    expect(file.getContent()).toBe('hello world');
  });

  it('should be throw exception if file doest not exists', async () => {
    try {
      await reader.read('./not-exists.test.txt');
      fail('read not existed file');
    } catch (e) {
      expect.assertions(2);
      if (e instanceof RuntimeException) {
        expect(e).toBeInstanceOf(RuntimeException);
        expect(e.message).toBe('File ./not-exists.test.txt not found');
      }
    }
  });
});
