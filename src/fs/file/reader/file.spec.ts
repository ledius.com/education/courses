/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { File } from './file';

describe('File', () => {
  it('should be defined', () => {
    const path = '/app/src/dir';
    const content = 'hello world';
    const file = new File(path, content);
    expect(file).toBeDefined();
    expect(file.getPath()).toBe(path);
    expect(file.getContent()).toBe(content);
    expect(file.isEmpty()).toBeFalsy();
    expect(typeof file.isEmpty()).toBe('boolean');
  });

  it('should be empty content', () => {
    const file = new File('app', '');
    expect(file.isEmpty()).toBeTruthy();
  });
});
