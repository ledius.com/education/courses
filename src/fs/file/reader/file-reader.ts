/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { FileReaderInterface } from './file-reader.interface';
import * as fs from 'fs';
import { RuntimeException } from 'node-exceptions';
import { File } from './file';

/**
 * @class
 * @implements FileReaderInterface
 */
export class FileReader implements FileReaderInterface {
  public async read(path: string): Promise<File> {
    if (!fs.existsSync(path)) {
      throw new RuntimeException(`File ${path} not found`);
    }
    const content = fs.readFileSync(path).toString('utf8');
    return new File(path, content);
  }
}
